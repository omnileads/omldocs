.. _about_install_postgresql:

********************************
Deploy del componente PostgreSQL
********************************
Como es sabido, **PostgreSQL** implementa un script de *fisrt_boot_installer.tpl*, que puede ser invocado utilizando tecnología de provisioning,
o bien ser ejecutado manualmente con una previa parametrización de las variables implicadas.

Para obtener nuestro script, se debe lanzar el siguiente comando:

.. code-block:: bash

  curl https://gitlab.com/omnileads/omlpgsql/-/raw/master/deploy/first_boot_installer.tpl?inline=false > first_boot_installer.sh && chmod +x first_boot_installer.sh

Entonces, una vez que contamos con el script, pasamos a trabajar las variables, quitando comentarios y configurando sus valores dentro del propio script.
Vamos a listar y explicar cada una de éstas variables que deben ser ajustadas antes de ejecutar el script.

Variables de instalación
************************
* **oml_infras_stage**: Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise* como valor para la variable. **Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

* **oml_nic**: Aquí debemos indicar la interfaz de red sobre la que se levantará el puerto TCP 5432 que va a procesar las solicitudes de conexión provenientes de Asterisk y OMLApp.

* **oml_pgsql_release**: Aquí debemos indicar qué versión del componente se desea desplegar. Para ello, se deberá revisar el archivo `.gitmodules <https://gitlab.com/omnileads/ominicontacto/-/blob/master/.gitmodules>`_ de la versión de OMniLeads que se desee desplegar, y tomar el parámetro *branch* del componente, como valor de ésta variable.

* **oml_db_name, oml_db_user, y oml_db_password**: Aquí citamos 3 parámetros vinculados entre sí, que son el nombre de la base de datos que va a alojar todas las tablas necesarias, el usuario y el password admitido para acceder a dicha base de datos.

* **oml_pgsql_blockdev**: Aquí se indica el *nombre* del disco o partición exclusiva que se desea montar sobre el filesystem, para utilizar como directorio donde se ubicarán las bases de datos PostgreSQL, es decir a donde se va a montar el path */var/lib/pgsql*. El hecho de utilizar un dispositivo/partición aquí, tiene sentido si es que PostgreSQL va a convivir con OMLApp en el mismo Linux host. **Posibles valores**: *NULL, /dev/...*.

* **PRIVATE_IPV4 e IPADDR_MASK**: Estos parámetros por defecto deben quedar comentados. Sólamente se deben habilitar si por algún motivo, el script no logró determinar la dirección IPv4 y máscara de subred a través de inspeccionar *oml_nic*. La idea es que PGSQL abra sus conexiones sobre una IPv4 y subred.

Con respecto a las variables implicadas para un despliegue en Alta Disponibilidad (HA), las mismas vienen
comentadas por defecto. En caso de desplegar en HA, se deberán descomentar y completar:

* **oml_deploy_ha**: En caso de desplegar un cluster de PostgreSQL, se debe indicar **true**.

* **oml_ha_rol**: Se indica si el nodo PostgreSQL a desplegar va a ser **main** o **backup**.

* **oml_ha_vip_nic**: Se indica la interfaz de red sobre la cual se levantará la VIP.

* **oml_ha_vip_main y oml_ha_vip_backup**: Éstas son las direcciones IP virtuales que cada nodo va a tomar, dependiendo de su rol y el estado del otro nodo.

Ejecución de la instalación
***************************
Finalmente, podemos lanzar nuestro script *first-boot-instaler.sh*. Ésto lo podemos hacer como user_data de una instancia cloud, a través de alguna utilidad
de línea de comandos para administrar la infraestructura, o bien directamente copiando el archivo hacia el Linux host destino y lanzando la ejecución del mismo.

Una vez ejecutado el script y finalizado el deploy, debemos comprobar que el componente se encuentra operativo, lanzando el siguiente comando:

.. code-block:: bash

  systemctl status postgresql-11

.. image:: images/install_pgsql_status.png

.. Important::

  ¡Es importante asegurarnos de que PostgreSQL abra su puerto (5432 por defecto) sobre una dirección de red privada! Es decir, NUNCA debemos instalar éste componente sobre una IP pública.
