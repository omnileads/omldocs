.. _about_install_omlapp_variables:

**********************************************
Variables de instalación del componente OMLApp
**********************************************
En éste inciso, vamos a listar y explicar cada una de las variables implicadas en un despliegue de OMniLeads.

oml_infras_stage
****************
Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar
cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise*
como valor para la variable.

**Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

oml_app_release
***************
Aquí se debe indicar qué release se desea desplegar.

**Posibles valores**: *master, develop, release-X.X.X*.

oml_tenant_name
***************
Ésta variable, debe contener un nombre de referencia para asignar al despliegue. Típicamente,
se utiliza el nombre del ambiente/cliente. Está pensado para oferentes de CCaaS (Contact Center as a Service), entornos
en donde se ejecuta una instancia de OMniLeads para cada suscriptor del servicio.

oml_callrec_device
******************
Aquí debemos indicar a la aplicación, la ubicación de las grabaciones de las llamadas.

**Posibles valores**: *local, s3, s3-minio, s3-aws, nfs*.

El valor *local* esta acotado al escenario en donde OMLApp y Asterisk (ACD) comparten el mismo Linux host, e implica que las grabaciones y otra data persistente
queden almacenadas en el disco local donde se ejecutan ambos componentes.

Por otro lado cuando los componentes ACD (Asterisk) y OMLApp (django/uwsgi) se ubican en instancias Linux independientes, tenemos la obligación
de utilizar un recurso compartido para almacenar grabaciones y data persistente.

Aquí podemos usar el concepto de **bucket** en object storage DB o NFS.
Las opciones *s3*, *s3-minio* o *s3-aws*, son para indicar se va a utilizar un bucket de `AWS <https://aws.amazon.com/s3/?nc1=h_ls>`_. También se
puede indicar si se desea operar con un bucket S3 `MinIO <https://min.io/>`_ , o bien si se utiliza el parámetro *s3* la configuracion recae
sobre un formato s3 genérico.

Finalmente, respecto al uso de un NFS para guardar las grabaciones, podemos utilizar cualquier herramienta que proporcione almacenamiento de red NFS, y así
el valor de nuestro parametro será *nfs*.

s3_access_key, s3_secret_key, s3_bucket_name, s3_endpoint_url y s3_region
*************************************************************************
Éstas variables sólamente son necesarias cuando se utiliza el object storage S3.
Allí se debe indicar las claves para autenticarse, junto al URL y nombre del bucket que va a contener las grabaciones.

De no utilizar s3, dichas variables deberan ser inicializadas como NULL.

nfs_host
********
Ésta variable es necesaria asignar sólamente cuando se utiliza NFS como recurso de almacenamiento de grabaciones.
Y debe contener la dirección de red del host que provee NFS.

oml_nic
*******
Aquí debemos indicar la interfaz de red sobre la que se levantará el puerto TCP 443 que va a procesar
las solicitudes HTTPS.

.. Important::

  Idealmente, se debe proporcionar una interfaz ubicada en una subnet privada
  y utilizar algún componente load balancer o proxy/firewall que conecte con la subnet pública, en el caso
  de necesitar habilitar acceso de usuarios desde Internet.

oml_ami_user y oml_ami_password
*******************************
Éstas 2 variables, son para definir el usuario y password que utilizará OMLApp para conectarse a la API de Asterisk AMI.

.. Important::

  Si estamos en un escenario donde Asterisk y OMLApp corren en Linux host independientes, entonces
  aquí debemos poner los valores utilizados para instalar el componente ACD (Asterisk).

oml_acd_host
************
Aquí indicamos la dirección de red del componente ACD (Asterisk), en el caso de que corra en un Linux host diferente a OMLApp.

.. Important::

  En caso de estar compartiendo host OMLApp y ACD (Asterisk), se debe colocar explícitamente NULL.

oml_pgsql_host y oml_pgsql_port
*******************************
Las 2 variables hacen referencia a la dirección de red y puerto en donde el componente PostgreSQL recibe
las peticiones de OMLApp.

.. Important::

  En caso de estar compartiendo host OMLApp y PostgreSQL, se debe colocar explícitamente NULL.

oml_pgsql_db, oml_pgsql_user y oml_pgsql_password
*************************************************
Las 3 variables tienen que ver con el nombre de la base de datos donde se crearán todas las tablas
necesarias, junto al usuario y password que tendrá permitido interactuar con las mismas.

OMLApp utilizará éstas variables para interactuar con el motor PostgreSQL.

oml_pgsql_cloud
***************
Ésta variable sirve para indicarle al instalador de OMLApp que instale la extensión PLPERL, por lo que sólamente hay que ponerla en "true"
cuando el PostgreSQL-11 que vamos a utilizar como motor, es proporcionado *llave en mano*, es decir cuando (ya sea en un ambiente
OnPremise o cloud) el componente es instalado SIN utilizar el RPM que proporciona OMniLeads.

**Posibles valores**: *true, false*.

oml_pgsql_cluster & oml_pgsql_readonly_host
*******************************************
Éste par de variables permiten que OMLApp realice las transacciones SQL de escritura sobre el nodo principal del cluster PostgreSQL (RW), mientras
que las operaciones de sólo lectura serán realizadas sobre el nodo de réplica del cluster HA.

Por ende, sólo se deberán utilizar en caso de que PostgreSQL esté disponible como cluster de alta disponibilidad.

**Posibles valores**: *true, false*.

oml_pgsql_ssl
*************
Éste valor sirve para setear ODBC en modo SSL. Por defecto y para PSQL interno, dejarlo en *NULL*.

**Posibles valores**: *NULL, true*.

api_dialer_user y api_dialer_password
*************************************
Aquí se definen los parámetros que OMLApp utilizará para autenticarse con la API de WombatDialer.

oml_dialer_host
***************
En caso de desplegar WombatDialer sobre un Linux host dedicado, se debe indicar la dirección de red
para que OMLApp pueda alcanzarlo.

**Posibles valores**: *dirección_de_red, NULL*.

oml_rtpengine_host, oml_kamailio_host, oml_redis_host, oml_websocket_host y oml_websocket_port
**********************************************************************************************
Ésta colección de variables hacen alusión al hecho de declarar la dirección de red de dichos componentes.
Cada uno de éstos componentes que se hayan alojado en un Linux host exclusivo y aislado de OMLApp, deberán ser declarados
con su dirección de red, y puerto (en caso de ser necesario).

Si los componentes conviven dentro del mismo host que OMLApp, entonces se deben declarar como **NULL**.

oml_redis_ha
************
En el caso de desplegar el componente Redis en alta disponibilidad, se debe setear éste valor en *true*.

Para mayor información, revisar el siguiente enlace: `Redis sentinel <https://redis.io/docs/manual/sentinel/cluster>`_.

oml_sentinel_host_01, oml_sentinel_host_02 & oml_sentinel_host_03
******************************************************************
Éstos 3 parámetros implican la dirección de red de cada uno de los nodos réplica de Redis. En la sección :ref:`about_install_linux_ha`,
se profundiza al respecto.

oml_extern_ip
*************
En caso de estar en un ambiente en donde los componentes ACD (Asterisk) y RTPEngine conviven con OMLApp,
entonces aquí se debe indicar la dirección de red pública que está afectando a nivel de NAT a nuestro despliegue.

**Posibles valores**: *none, auto, X.X.X.X*.

El valor *none* implica que no vamos a utilizar accesos desde Internet a la aplicación; si se configura como *auto* entonces
la instalación intentará detectar la IP pública con la cual se está realizando NAT a los paquetes que salen a Internet;
y finalmente también es viable declarar la dirección de red pública explícita (X.X.X.X).

oml_tz
******
Aquí se indica la zona horaria donde se ejecuta la instancia de OMniLeads.

oml_app_sca
***********
El tiempo de sesión (en segundos) para aplicar al login de usuario por HTTPS.

oml_app_ecctl
**************
El tiempo de rotación de las credenciales efímeras con las que se autentica un usuario a nivel SIP over websocket contra
Kamailio.

oml_app_login_fail_limit
************************
La cantidad de intentos fallidos de ingresos HTTPS por parte de los usuarios, hasta ser bloqueados por Django-Defender.

oml_high_load
*************
Al ser inicializado en *true*, se disparan una serie de optimizaciones a nivel configuración sobre Redis y Django/uwsgi.
Se utiliza para entornos de carga mayor a 200 usuarios concurrentes.

oml_google_maps_api_key
***********************
Se inicializa con *true*, en caso de utilizar la integración con google maps para visualizar ubicaciones en el formulario de atención.

oml_google_maps_center
**********************
Se debe proporcionar los parámetros de latitud y longitud correspondientes a su ubicación.
Ejemplo: oml_google_maps_center='{ "lat": -31.416668, "lng": -64.183334 }'.

oml_smtp_relay
**************
A partir del release-1.25.0, se puede configurar un SMTP relay para acudir al mismo a la hora de enviar correos,
al momento de crear usuarios o bien a la hora de generar algún reporte que deba ser adjuntado en un correo.

Si se ha habilitado el smtp relay, entonces se deben indicar los parametros sub-siguientes.

oml_email_host
***************
Aquí se indica la dirección de red del servidor smtp.

oml_email_port
**************
Aquí se indica el puerto de red TCP al que se debe apuntar.

oml_email_user
**************
Username para autenticar contra el servidor SMTP.

oml_email_password
*******************
Password para autenticar contra el servidor SMTP.

oml_email_default_from
***********************
El from que va a figurar en los correos enviados.

oml_email_use_tls
*****************
Aquí se indica con *true* si se requiere utilizar TLS.

oml_email_use_ssl
******************
Aquí se indica con *true* si se requiere utilizar SSL.

oml_email_ssl_certfile
***********************

oml_email_ssl_keyfile
*********************
