.. _about_inboundroutes:

**********************************
Enrutamiento de llamadas entrantes
**********************************
Una vez disponible nuestra campaña entrante, se debe proceder con la vinculación de un número telefónico "DID" disponible en alguno de los troncales SIP por los que llegarían las solicitudes de llamadas
con la campaña entrante en cuestión. Es decir, la asignación del número y la campaña entrante hacia donde derivar a todas las llamadas que ingresen a OMniLeads sobre dicho número DID.

.. image:: images/campaigns_in_routes.png

*Figura 1: Rutas entrantes*

Para generar una nueva ruta de llamadas entrantes, debemos acceder al menú *Telefonía -> Rutas entrantes*, en donde se listan las rutas creadas y además se pueden añadir nuevas.

En la siguiente figura, se puede visualizar una *ruta entrante* en su pantalla de configuración:

.. image:: images/campaigns_in_route.png

*Figura 2: Parámetros de ruta entrante*

- **Nombre:** Es el nombre asignado a la ruta (alfanumérico sin espacios).
- **Número DID:** Es el número entrante que se valida como disparador del encaminamiento de la llamada sobre el destino seleccionado por la propia ruta.
- **Prefijo caller id:** El valor que se configure en éste campo, aparecerá como prefijo del CallerID que llega en cada llamada por el troncal correspondiente.
- **Idioma:** El idioma que se utiliza a la hora de reproducir locuciones por defecto del sistema, sobre los canales que ingresan por la ruta.
- **Tipo de destino:** El tipo de destino a donde se enviarán las llamadas ingresadas por dicha ruta. Dentro de los tipos de destinos existen campañas entrantes, IVRs, condicionales de tiempo, identificación de llamante, etc.
- **Destino:** Destino puntual al cual enviar los canales.

Es importante aclarar que está permitido que varias rutas tengan el mismo destino.
