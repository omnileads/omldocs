.. _about_previewcamp:

Creación de campaña preview
***************************
Para crear una nueva campaña preview, se debe ingresar al menú *Campañas -> Campañas preview -> Nueva campaña*. El proceso de creación consta de
un wizard de 2 pantallas.

En la primera etapa, se deben indicar una serie de parámetros de campaña, como lo indica la siguiente figura:

.. image:: images/campaigns_prev_wizard_1.png

*Figura 1: Parámetros de la campaña*

- **Nombre:** Nombre de la campaña.
- **Base de datos de contactos:** La base de contactos que utilzará el discador preview a la hora de entregar contactos bajo demanda a cada agente.
- **Tipo de interacción:** Indica si la campaña va a operar con formularios de OMniLeads o bien va a ejecutar una invocación hacia un CRM por cada llamada conectada.
- **URL externa:** En caso de haber seleccionado la invocación a una URL externa en cada llamada, aquí se indica cuál de los CRMs definidos debe invocar la campaña.
- **Grabar llamados:** Habilita que todas las llamadas de la campaña sean grabadas.
- **Objetivo:** Se define como la cantidad de *gestiones positivas* esperadas en la campaña. En la supervisión de la campaña, se muestra en tiempo real el porcentaje de avance de la campaña respecto a éste objetivo definido.
- **Sistema externo:** Aquí se adjudica el sistema de gestión externo que ejecutaría "click to call" sobre la campaña, en caso de así desearlo.
- **ID en sistema externo:** Éste campo debe contener el ID que posee la campaña dentro del sistema de gestión externo, desde el cual llegarán los "click to call" o "solicitudes de calificación" de contactos.
- **Ruta saliente:** Se le asigna una ruta saliente existente a una campaña.
- **CID ruta saliente:** Éste campo debe contener el CID asignado para una ruta saliente existente a una campaña.
- **Tiempo de desconexión:** Es el tiempo que el *discador preview* reserva un contacto asignado a un agente, tiempo luego del cual el contacto se libera de manera tal que pueda ser demandado por otro agente.
- **Speech:** El speech de campaña para ser desplegado en la consola de agente en las llamadas de la campaña.

En la segunda pantalla, se deben asignar las calificaciones que se requieran como disponibles para los agentes a la hora de clasificar cada llamada a cada contacto.

.. image:: images/campaigns_prev_wizard_2.png

*Figura 2: Calificaciones de llamada*

Luego, resta asignar a los supervisores y agentes que podrán trabajar en la campaña.
En las siguientes 2 figuras, se ejemplifica una asignación de agentes a una campaña:

.. image:: images/campaigns_prev_wizard_3.png

*Figura 3: Asignación de supervisores*

.. image:: images/campaigns_prev_wizard_4.png

*Figura 4: Asignación de agentes*

Por último, en la etapa final del wizard, se permite opcionalmente realizar una pre-asignación de contacto a agentes.
Ésta pre-asignación, puede se realizar en 2 variantes.

La primera variante es mostrada en la siguiente imagen:

.. image:: images/campaigns_prev_wizard_5_1.png

*Figura 5: Asignación de contactos por defecto*

Asignación de contactos
***********************
Si se selecciona la opción de asignar proporcionalmente, el sistema realizará una asignación inicial y equitativa de contactos a cada uno de los agentes
permitiendo que en la consola de agente, cuando el agente pida un contacto de la campaña, se le entreguen sólo contactos libres o de la lista de los
que se le hayan asignados. Podemos ver un ejemplo de distribución usando la interfaz administrativa de Django en la siguiente figura:

.. image:: images/campaigns_prev_admin_proporcional.png

*Figura 6: Distribución proporcional de contactos*

Al momento de seleccionar la opción de pre-asignación proporcional, el sistema muestra una nueva opción para asignar aleatoriamente:

.. image:: images/campaigns_prev_wizard_5_2.png

*Figura 7: Nueva opción para asignar aleatoriamente*

Ésta opción, permite que los contactos sean asignados en orden aleatorio a los agentes, tal como se muestra a continuación:

.. image:: images/campaigns_prev_admin_aleatorio.png

*Figura 8: Distribución proporcional y aleatoria de contacto*

Finalmente, nuestra campaña queda disponible para comenzar a operar. Por lo tanto, cuando los agentes asignados a la misma realicen un login a la plataforma, deberían
disponer de la campaña preview, tal como se expone en la siguiente figura:

.. image:: images/campaigns_prev_agconsole1.png

*Figura 9: Vista del agente de la campaña preview*

Interacción de agente con campaña
*********************************
Si el agente hace click sobre el teléfono, entonces se dispara la llamada, se visualizan los datos (extras al teléfono) del contacto llamado en la vista de agente,
permitiendo a su vez al agente clasificar la llamada con alguna de las calificaciones asignadas a la campaña:

.. image:: images/campaigns_prev_agconsole2.png

*Figura 10: Llamado al contacto*

Campaña con base de datos multinum
**********************************
Como sabemos, OMniLeads admite que cada contacto de una base posea "n" números de teléfono de contacto, de manera tal que si el contacto no es encontrado en su número principal
(el primero de nuestro archivo CSV de base), pueda ser contactado a los demás números. En este caso, cada número de teléfono (que indicamos en la carga de la base) se genera
como un link dentro de los datos del contacto presentados sobre la pantalla de agente. Al hacer click sobre dicho link, se disparará una llamada hacia el número de teléfono extra
del contacto. En la siguiente figura, se muestra dicho escenario:

.. image:: images/campaigns_prev_agconsole3.png

*Figura 11: Base de datos multinum*

Por lo tanto, el agente puede intentar contactar a todos los números disponibles como "link" en la ficha del contacto, hasta finalmente calificar y pasar a uno nuevo.

Gestión de la entrega de contactos
**********************************
Una vez creada la campaña, los contactos disponibles para cada agente serán entregados siguiendo un orden establecido en el modelo AgenteEnContacto. Ésta funcionalidad permite editar ese orden usando la exportación/importación de archivos en formato .csv.

La idea es que el administrador puede descargarse el orden actual de contactos hacia un archivo .csv, reordenar las filas y luego importar dicho archivo, con lo cual el nuevo orden se impacta en el orden de la asignación de contactos a agentes en la campaña.

A ésta funcionalidad, se puede acceder usando el menú de la campaña preview, como se observa en las siguientes 2 figuras:

.. image:: images/access_to_reorden_contacts.png

*Figura 12: Acceso a la página de ordenamiento de entrega de contactos*

.. image:: images/reorder_contacts_page.png

*Figura 13: Página de reordenamiento de entrega de contactos*

También, es posible marcar como desactivados los contactos que se desee, los cuales no serán entregados a ningún agente.

Ésto es posible definiendo en la misma vista un campo de desactivación para la campaña de entre las columnas de datos de la base de contactos, como se puede ver en la siguiente figura:

.. image:: images/deactivation_field.png

*Figura 14: Campo de desactivación de contactos*

.. important::
  
  El campo a elegir debe tomarse en consideración en el formato del CSV al momento de importar la base de datos de contactos antes de adjuntarla a la campaña. No se permite agregar ni modificar los nombres de las columnas en el CSV a posteriori.

Después de realizar la exportación del orden actual de contactos, se puede editar la columna de desactivación con los valores 0 ó FALSE lo cual, luego de la importación del archivo .csv indicará al sistema que no se deben entregar esos contactos a ningún agente.
Cualquier otro valor distinto a éstos, hace que el sistema asuma que el contacto puede entregarse.
