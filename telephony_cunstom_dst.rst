.. _about_custom_dst:

***********************************
Ejecución de dialplan personalizado
***********************************
Resulta súmamente útil disponer de la posibilidad de poder forzar a que una llamada ejecute un "plan de discado" generado a imagen y semejanza de cualquier
requerimiento puntual del modelo de negocios implicado.

Pero, ¿a qué nos referimos con **"plan de discado"**?

Como bien sabemos, OMniLeads utiliza `Asterisk <https://www.asterisk.org>`_ como pieza fundamental dentro del módulo de "gestión de llamadas telefónicas",
y por lo tanto, cualquier programador con conocimientos de sintaxis de "dialplan" podrá generar sus propias rutinas de tratamiento de llamadas, pudiendo a
su vez invocarlas dentro del flujo de llamadas de:

* Rutas entrantes
* Campañas entrantes
* Destinos de failover
* Rutas salientes
* Etc.

Por lo que se permite entonces, generar un "nodo" invocable dentro de una llamada procesada en OMniLeads, siendo éste "nodo", lógica de programación de
Asterisk, personalizada de acuerdo a cualquier necesidad puntual que esté por afuera del alcance de los módulos típicos del sistema.

Por ejemplo, a la hora de tener que lanzar un IVR que implique algún tratamiento avanzado de las llamadas entrantes (sistemas de autogestión o confirmación
de trámites).

Modo de configuración
*********************
Para crear un destino personalizado, se debe acceder al menú *Telefonía -> Destinos personalizados*.

El módulo "destino personalizado" simplemente involucra un formulario sencillo donde se indica el nombre del nodo de "dialplan personalizado" y la tríada:

* Contexto
* Extensión
* Prioridad

Además, contamos con la necesidad de indicar un destino en caso de fallo.

Todo ésto, se visualiza en la siguiente figura:

.. image:: images/telephony_custom_dst.png

*Figura 1: Formulario de destino personalizado*

Por otro lado, el programador podrá generar su código a nivel archivo de texto "oml_extensions_custom.conf". Éste será cargado en tiempo real y también
tenido en cuenta a la hora de generar los Backup&Restore de la plataforma.

Ejemplo
*******
Vamos a implementar un dialplan como el siguiente:

.. code-block:: bash

  [omnileads_custom]
  exten => s,1,Verbose(*** Ejemplo de destino personalizado ***)
  same => n,Answer()
  same => n,Playback(demo-congrats)
  same => n,Hangup()

Por lo tanto, por un lado generamos el dialplan citado en el archivo "oml_extensions_custom.conf" ubicado en "/opt/omnileads/asterisk/etc/asterisk".

Luego, debemos generar el "nodo" destino personalizado, sobre la interfaz de configuración de OMniLeads:

.. image:: images/telephony_custom_dst_example.png

*Figure 2: Ejemplo de destino personalizado*

Finalmente, podemos invocar a nuestro nodo, desde una opción del IVR, validación horaria o ruta entrante:

.. image:: images/telephony_custom_dst_example_2.png

*Figure 3: Ejemplo de destino personalizado*
