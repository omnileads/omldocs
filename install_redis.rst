.. _about_install_redis:

***************************
Deploy del componente Redis
***************************
Como es sabido, **Redis** implementa un script de *fisrt_boot_installer.tpl*, que puede ser invocado utilizando tecnología de provisioning,
o bien ser ejecutado manualmente con una previa parametrización de las variables implicadas.

Para obtener nuestro script, se debe lanzar el siguiente comando:

.. code-block:: bash

  curl https://gitlab.com/omnileads/omlredis/-/raw/master/deploy/first_boot_installer.tpl?inline=false > first_boot_installer.sh && chmod +x first_boot_installer.sh

Entonces, una vez que contamos con el script, pasamos a trabajar las variables, quitando comentarios y configurando sus valores dentro del propio script.
Vamos a listar y explicar cada una de éstas variables que deben ser ajustadas antes de ejecutar el script.

Variables de instalación
************************
* **oml_infras_stage**: Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise* como valor para la variable. **Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

* **oml_nic**: Aquí debemos indicar la interfaz de red sobre la que se levantará el puerto TCP 6379 (default), que va a procesar las solicitudes de conexión a Redis provenientes de Kamailio, Asterisk, OMLApp y Websockets.

* **oml_redis_release**: Aquí debemos indicar qué versión del componente se desea desplegar. Para ello, se deberá revisar el archivo `.gitmodules <https://gitlab.com/omnileads/ominicontacto/-/blob/master/.gitmodules>`_ de la versión de OMniLeads que se desee desplegar, y tomar el parámetro *branch* del componente, como valor de ésta variable.

* **oml_host_port**: Aquí debemos indicar el puerto sobre el cual se levantará el servicio de Redis.

* **oml_high_load**: En caso de superar los 200 agentes concurrentes, se debe setear ésta variable en *true*, para aplicar una serie de configuraciones de optimización referidas a éste servicio. **Posibles valores**: *true, NULL*.

Con respecto a las variables implicadas para un despliegue en Alta Disponibilidad (HA), las mismas vienen
comentadas por defecto. En caso de desplegar en HA, se deberán descomentar y completar:

* **oml_deploy_ha**: En caso de desplegar un cluster de Redis, se debe indicar **true**.

* **oml_ha_rol**: Se indica si el nodo Redis a desplegar va a ser **main** o **replica**.

* **oml_master_ip**: Se indica la dirección de red del nodo **main** del cluster Redis.

* **oml_master_port**: Se indica el puerto de red del nodo **main** del cluster Redis.

Ejecución de la instalación
***************************
Finalmente, podemos lanzar nuestro script *first-boot-instaler.sh*. Ésto lo podemos hacer como user_data de una instancia cloud, a través de alguna utilidad
de línea de comandos para administrar la infraestructura, o bien directamente copiando el archivo hacia el Linux host destino y lanzando la ejecución del mismo.

Una vez ejecutado el script y finalizado el deploy, debemos comprobar que el componente se encuentra operativo, lanzando el siguiente comando:

.. code-block:: bash

  systemctl status redis

.. image:: images/install_redis_status.png

.. Important::

  ¡Es importante asegurarnos de que Redis abra su puerto (6379, por defecto) sobre una dirección de red privada!
  Es decir, NUNCA debemos instalar éste componente sobre una IP pública.
