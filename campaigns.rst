********************
Campañas telefónicas
********************
Una campaña representa una manera de encapsular (dentro de la plataforma) a una operación de Contact Center conformada por:

- Un grupo de agentes procesando llamadas en alguna dirección (outbound o inbound).
- Una base de contactos asociada a la campaña.
- Un listado de calificaciones disponibles a la hora de clasificar la llamada gestionada por el agente.
- Uno o varios formularios de campaña, a ser lanzados en caso de que el agente asigne una calificación de "gestión" sobre la llamada en curso. El formulario es desplegado por dicha calificación y el agente puede completar el mismo según los datos del contacto de la llamada en curso.

En la figura 1 se representa una campaña y sus componentes.

.. image:: images/campaigns_elements.png
*Figura 1: Los elementos de una campaña*

El hecho de trabajar bajo campañas, permite a los perfiles de usuarios Supervisor/Administrador acotar métricas e informes,
así como también realizar monitoreo en tiempo real o buscar grabaciones, entre otras acciones, utilizando campañas como criterio de filtrado.
Podemos contar con diferentes campañas de diferente naturaleza (predictivas, preview, entrantes o manuales), conviviendo de manera simultánea en OMniLeads y dejando
registros sobre las llamadas transaccionadas por los agentes.

.. image:: images/oml_bpo.png
*Figura 2: Campañas, agentes, supervisores y backoffice*

Calificaciones
**************
Las calificaciones constituyen un listado de "etiquetas" disponibles para ser asignadas a cualquier campaña, de manera tal que los agentes puedan clasificar cada llamada
dentro de una campaña, con cualquiera de éstas calificaciones.

Las calificaciones son generadas por el supervisor o administrador y se pueden relacionar con varios aspectos, por ejemplo:

- El resultado de la llamada hacia un contacto de la campaña (ocupado, no atendió, número equivocado, buzón de mensajes, etc.).
- El resultado asociado a una llamada atendida (interesado, no interesado, venta, encuesta realizada, etc.).

Las calificaciones pueden ser totalmente arbitrarias y para generarlas se debe ingresar al punto de menú *Campañas -> Calificaciones -> Nueva calificación*.

Podemos listar las calificaciones generadas dentro del menú *Campañas -> Calificaciones -> Listado de calificaciones*.

.. image:: images/campaigns_calldispositions.png
*Figura 3: Calificaciones*

Base de contactos
*****************
Las bases de contactos son un requisito para las campañas salientes aunque también pueden ser utilizadas por las campañas entrantes.
En el marco de las campañas salientes, la base de contacto adjunta a la campaña, provee los datos que requiere el discador predictivo o preview;
mientras que, en las campañas entrantes, aporta la información complementaria disponible sobre el número llamante que será desplegada en la pantalla
del agente al momento de recibir una llamada por la campaña.

Éstas bases, son proporcionadas en archivos con formato CSV con los campos separados con coma, y además generadas en la codificación UTF-8 (requisito excluyente).
Debe existir al menos una columna que contenga un teléfono por cada contacto (registro) del archivo, mientras que el resto de las columnas puede contener cualquier dato
(generalmente cada registro cuenta con datos complementarios al teléfono principal). Éstos datos son expuestos en la pantalla de agente a la hora de establecer
una comunicación entre ambos (agente y contacto de la base).

.. image:: images/campaigns_contactdb_1.png
*Figura 4: Archivo CSV de contactos - Vista del editor de texto*

.. image:: images/campaigns_contactdb_2.png
*Figura 5: Archivo CSV de contactos - Vista en libreoffice excel*

Se dispone entonces de una base de contactos (CSV) para proceder con la carga del archivo en el sistema accediendo al menú *Contactos -> Nueva base de datos de contactos*:

.. image:: images/campaigns_upload_contacts.png
*Figura 6: Nueva base de datos de contactos*

Se debe indicar con un check, cuáles columnas son las que almacenan teléfonos, como se indica en la figura 7:

.. image:: images/campaigns_upload_contacts_2.png
*Figura 7: Selección Teléfono*

Finalmente, se salva el archivo y el mismo queda disponible como una base de contactos del sistema instanciable por cualquier tipo de campaña.

Configuración de restricciones sobre campos de contactos
********************************************************
A veces, no es deseable que los agentes puedan ver o editar algún campo de un contacto en la base de datos. Para éstos casos, se puede configurar qué campos restringir ingresando desde la lista de campañas a la opción de "Restringir campos de contacto".

.. image:: images/campaigns_restrict_field_action.png
*Figura 8: Restringir campos de contacto*

Luego, en ésta pantalla se deben seleccionar para bloquear los campos que no deben ser editables y para ocultar los campos que ni siquiera deben poder ser vistos por los agentes. Sólo será posible ocultar los campos que estén seleccionados como bloqueados. El campo principal de teléfono no podrá ser ocultado.

.. image:: images/campaigns_restricted_fields_selection.png
*Figura 9: Seleccionando campos de contacto para bloquear u ocultar*

Cuando un agente quiera editar un contacto, no se le mostrarán los campos ocultos y se le presentarán deshabilitados los campos seleccionados como bloqueados que no estén ocultos. Tener en cuenta que al crear el contacto, el agente podrá ingresar por única vez valores en los campos que sólamente estén bloqueados.

.. image:: images/campaigns_restricted_fields_for_agent.png
*Figura 10: Campos de contacto restringidos y ocultos*

Formularios
***********
Los formularios de campaña constituyen el método por defecto para recolectar información (relevante para la campaña) en la interacción con la persona detrás
de una comunicación establecida.
Son diseñados por el usuario combinando en una vista estática diferentes tipos de campos (texto, fecha, de múltiple selección y área de texto), pudiendo ordenar
los mismos de acuerdo a la necesidad de la campaña.

Para crear formularios se debe acceder al punto de menú *Campañas -> Formularios -> Nuevo Formulario*.

Los formularios pueden contener campos del tipo:

- **Texto**
- **Fecha**
- **Lista**
- **Caja de texto de área**

En la siguiente figura se ejemplifica un campo del tipo "combo" dentro de la creación de un formulario:

.. image:: images/campaigns_newform_1.png
*Figura 11: Nuevo formulario de campaña*

Podemos generar un formulario de ejemplo de encuesta de satisfacción con el aspecto de la siguiente figura:

.. image:: images/campaigns_newform_2.png
*Figura 12: Formulario de encuesta*

.. _about_calificaciones_forms:

Campañas, calificaciones y formularios
**************************************
Para explicar la relación entre éstos componentes, debemos recordar que múltiples formularios pueden ser asignados a una campaña. La idea es que diferentes calificaciones de una campaña
pueden disparar diferentes formularios, permitiendo así a la operación recolectar mediante formularios previamente diseñados, información asociada a la interacción entre el
agente de OMniLeads y la persona en el otro extremo de la comunicación dentro de la campaña.

Resulta importante explicar conceptualmente cómo se utilizan los formularios de campaña en OMniLeads. Antes de empezar, resulta necesario aclarar que,
en el marco de una campaña, a la hora de asignar calificaciones, se van a poder definir calificaciones "Sin acción" y calificaciones de "Gestión".
Éstas últimas, son las que disparan los formularios de campaña.

.. image:: images/campaigns_calldispositions_add.png
*Figura 13: Calificaciones dentro de la campaña*

En el ejemplo de la figura anterior, contamos con 2 calificaciones del tipo "Gestión", por un lado la calificación "Encuesta realizada" que tiene asociado el formulario "Calidad de Servicio"
y por otro la calificación "Encuesta" que dispara el formulario "Encuesta cliente".

Siempre que haya una llamada activa entre un agente y un contacto de la base de la campaña, el agente dispone de los datos complementarios al teléfono del
contacto en su pantalla, junto al combo de selección de calificación para el contacto actual. Si el agente selecciona y guarda una calificación del tipo "gestión",
entonces se dispara en la pantalla de agente el formulario asociado a la calificación dentro de la campaña.

.. image:: images/campaigns_dispositions_engaged.png
*Figura 14: Acciones de gestión y formularios*

Campañas manuales
*****************
Dentro de éste inciso, se ejemplifica paso a paso cómo administrar campañas manuales.

.. toctree::
  :maxdepth: 2

  campaigns_manual.rst

Campañas preview
****************
Dentro de éste inciso, se ejemplifica paso a paso cómo administrar campañas preview.

.. toctree::
  :maxdepth: 2

  campaigns_preview.rst

Campañas con discador predictivo
********************************
Dentro de éste inciso, se ejemplifica paso a paso cómo administrar campañas dialer.

.. toctree::
  :maxdepth: 2

  campaigns_dialer.rst

Campañas entrantes
******************
Al hablar de llamadas entrantes, nos toca desplegar cada funcionalidad aplicable al flujo de llamadas entrantes. Como bien sabemos, una llamada entrante
puede pasar por una serie de "nodos" hasta finalmente conectar con un agente de atención. Por lo tanto, vamos a ampliar el concepto de "campañas entrantes"
a los siguientes ítems de configuración:

.. toctree::
  :maxdepth: 1

  campaigns_inbound.rst
  campaigns_inbound_routes.rst
  campaigns_inbound_routes_frompbx.rst
  campaigns_inbound_timeconditions.rst
  campaigns_inbound_ivr.rst
  campaigns_inbound_customer_id.rst
  telephony_cunstom_dst.rst

Plantillas de campaña
*********************
Suele ser recurrente que los parámetros de una "clase" de campaña (por ejemplo, campañas preview de encuestas) no varíen demasiado, salvo quizás por
el grupo de agentes asignados, la base de contactos a utilizar o el supervisor asignado. Por lo tanto, en lugar de tener que crear campañas muy similares siempre
desde cero, se pueden generar plantillas para luego crear campañas nuevas rápidamente a partir de clonar dichas plantillas.

Ésta funcionalidad la otorgan los *Templates* de campañas de OMniLeads.

A partir de la generación de una plantilla (que se genera de manera similar a una campaña), se pueden crear nuevas campañas simplemente seleccionando el template y
la opción *Crear campaña desde template*. Cada nueva campaña estará disponible con todos los parámetros especificados en su plantilla matriz.

.. image:: images/campaigns_template.png
*Figura 15: Templates*

Interacción con sistemas de gestión externos
********************************************
OMniLeads está diseñado desde una perspectiva en la que se prioriza una integración con el sistema de gestión predilecto de la compañía. Brindando así la posibilidad
de que una empresa mantenga el uso de su sistema de gestión apropiado a su mercado vertical (salud, ventas, atención al cliente, etc.).

Mediante funcionalidades propias y métodos de la API, OMniLeads permite las siguientes interacciones:

* Abrir una vista concreta del CRM en una comunicación entrante o saliente, utilizando parámetros de la comunicación (id del agente, id del contacto, id de la campaña, etc.) como información dinámica para invocar al CRM.
* Permitir realizar una llamada "click to call" desde una vista de contacto en el CRM y accionar así una llamada a través de una campaña y agente de OMniLeads.
* Permitir calificar la gestión de un contacto del CRM y que la calificación se impacte en OMniLeads, de manera tal que exista una correlación entre el sistema CRM y el sistema de Contact Center dentro de cada campaña.
* OMniLeads en las llamadas entrantes puede solicitar el ID del llamante y notificar al CRM para que éste decida sobre qué campaña de OMniLeads encaminar una llamada.

Ampliamos todos éstos conceptos y configuraciones en el siguiente link :ref:`about_crm`.
