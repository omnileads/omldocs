.. _about_inboundcamp:

Creación de nueva campaña
*************************
Para crear una nueva campaña, se debe ir al menú *Campaña -> Campañas entrantes -> Nueva campaña*.

Se debe completar la primera pantalla del wizard, como lo expone la siguiente figura:

.. image:: images/campaigns_in_wizard1.png

*Figura 1: Parámetros de campaña*

- **Nombre:** Nombre de la campaña.
- **Base de datos de contactos:** Campo opcional. Se utiliza para desplegar datos a partir del número de teléfono que realiza la llamada entrante al sistema.
- **Tipo de interacción:** Aquí se selecciona si la campaña va a utilizar un formulario de campaña o va a disparar una solicitud HTTP hacia un sistema CRM externo.
- **URL externa:** En caso de haber seleccionado la invocación a una URL externa en cada llamada, aquí se indica cuál de los CRMs definidos debe invocar la campaña.
- **Objetivo:** Se define como la cantidad de *gestiones positivas* esperadas en la campaña. En la supervisión de la campaña, se muestra en tiempo real el porcentaje de avance de la campaña respecto a éste objetivo definido.
- **Mostrar nombre al recibir llamado:** Aquí se habilita la opción de mostrar el nombre de la campaña por la cual ingresa la llamada.
- **Habilitar video:** Habilitar video si está disponible.
- **Sistema externo:** Aquí se adjudica el sistema de gestión externo que ejecutaría "click to call" sobre la campaña, en caso de así desearlo.
- **ID en sistema externo:** Éste campo debe contener el ID que posee la campaña dentro del sistema de gestión externo, desde el cual llegarán los "click to call" o "solicitudes de calificación" de contactos.
- **Ruta saliente:** Se le asigna una ruta saliente existente a una campaña.
- **CID ruta saliente:** Éste campo debe contener el CID asignado para una ruta saliente existente a una campaña.
- **Speech:** El speech de campaña para ser desplegado en la consola de agente en las llamadas de la campaña.

 .. note::

  El tema de la base de contactos en las campañas entrantes (y también manuales) plantea un escenario flexible, ya que es opcional el asignar una base de contactos a este tipo de campañas.
  En éste caso, la base de contactos es utilizada si deseamos que cada vez que un contacto realice una comunicación entrante, en base a su "callerid" poder recuperar los datos del mismo, si es que se encuentra registrado el teléfono desde donde llama, dentro de la base de contactos asociada a la campaña.

A continuación se prosigue con la creación de la campaña entrante:

.. image:: images/campaigns_in_wizard2.png

*Figura 2: Parámetros de campaña*

- **Cantidad máxima de llamadas:** Es el número de llamadas que se permiten poner en cola de espera. Por encima de ese número, la llamada será enviada a la acción "failover".
- **Grabar llamados:** Se debe seleccionar ésta casilla en caso de requerir que los llamados sean grabados.
- **Prioridad de campaña:** Es un parámetro lineal en la escala del 1 al 10, que implica cuán importante son las llamadas de ésta campaña respecto a otras. Establece prioridades para los agentes que trabajan en varias campañas en simultáneo. Si se deja el valor en "0" (por defecto), se mantiene una equidad con el resto de las campañas.
- **Estrategia de distribución:** Método de distribución de llamadas que usará la campaña sobre los agentes. Para campañas salientes, se recomienda RRMemory.
- **Música de espera:** Indica la playlist que escuchará el cliente cuando su llamada esté en espera.
- **Tiempo de espera en cola:** Es el tiempo (en segundos), que la llamada contactada quedará en cola de espera, aguardando que un agente se libere para conectarle la misma.
- **IVR de escape:** Es un destino de tipo IVR que el usuario puede escoger para "escapar" hacia él en medio del tiempo de espera. Éste parámetro está relacionado con el de anuncio periódico, pues éste último es donde se reproduciría el mensaje que guíe al usuario para realizar el escape hacia dicho destino IVR.
- **Nivel de servicio:** Es un parámetro para medir cuántas de las llamadas fueron contestadas dentro de esa franja de tiempo. El valor se expresa en segundos.
- **Destino en caso de timeout:** Destino hacia el cual se enviarán las llamadas que hayan sido expiradas (superó el tiempo de espera asociado).
- **Anuncio de entrada para el agente:** Audio que se reproduce al agente cuando recibe una llamada entrante.
- **Audio de ingreso:** Permite definir si se reproducirá algún audio sobre la llamada entrante, al ingresar a la cola de espera de nuestra campaña. Para que esté disponible en el selector desplegable, debe cargarse previamente el archivo desde el menú *Telefonía > Audios > Audios personalizados*.
- **Anunciar tiempo promedio de espera:** Permite al usuario especificar el tiempo promedio que deberá esperar el llamante para ser atendido
- **Tiempo de ring:** Es la cantidad de *tiempo de ring* que la campaña utiliza para intentar contactar la llamada entrante hacia un agente. Se debe considerar que éste parámetro tiene sentido siempre que el agente no trabaje dentro de un grupo configurado con la modalidad *Auto-attend inbound calls*.
- **Tiempo de reintento:** Es el tiempo (en segundos) que la llamada quedará en la cola de espera, hasta volver a intentar conectarla con otro agente disponible.
- **Anunciar posición:** Anunciar posición del llamante en la cola.
- **Frecuencia de anuncios de espera/posición:** Es el tiempo (en segundos) de espera o posición para una frecuencia de anuncios.
- **Tiempo de descanso entre llamadas:** Es el tiempo de descanso (en segundos) que cada agente dispone entre cada llamada satisfactoria.
- **Anuncio periódico:** Podemos seleccionar algún audio de nuestro repositorio para reproducir como un anuncio periódico sobre la llamada en espera.
- **Frecuencia del anuncio periódico:** Cantidad de segundos entre cada reproducción de un anuncio periódico.

.. note::
  Los parámetros "Tiempo de Ring" y "Tiempo de reintento", quedan sin efecto para aquellas llamadas que reciban agentes cuyo grupo de agentes esté configurado con el atributo auto-atención de llamadas entrantes o para las campañas que tengan esta opción habilitada en “Configuraciones para agentes”.

En la siguiente etapa de configuración, se deben asignar las calificaciones que la campaña requiera para que los agentes califiquen cada llamada de contacto:

.. image:: images/campaigns_in_wizard3.png

*Figura 3: Calificaciones de campaña*

En el siguiente paso, agregamos supervisores a la campaña:

.. image:: images/campaigns_in_wizard4.png

*Figura 4: Asignación de supervisores*

Finalmente, los agentes pueden ser asignados a la campaña:

.. image:: images/campaigns_prev_ag2.png

*Figura 5: Asignación de agentes*

En el resto del capítulo, se detalla todo acerca del enrutamiento de las llamadas desde los vínculos troncales hacia nustras campañas entrantes.

Ringing vs. atención automática de llamadas entrantes
*****************************************************
El comportamiento del webphone del agente frente a una llamada proveniente de una campaña entrante puede ser:

- **Ringing normal** y con una duración asociada al parámetro "Tiempo de ring" presente en la creación de la campaña entrante. Durante ese tiempo, el teléfono del agente notifica la llamada entrante, aguardando la acción del agente que determine la atención o no de la llamada.
- **Atención automática de la llamada**. Éste comportamiento, implica que cada llamada entrante enviada a un agente sea atendida automáticamente por el teléfono del agente, notificando a éste con un "beep" antes de dejarlo definitivamente en línea con la contraparte de la llamada.

Dicho comportamiento depende de la configuración a nivel "Grupo de agentes" que posea el agente vinculado a la campaña entrante (ver figura 6) o a las
configuraciones para agentes especificadas para la campaña (ver figuras 7 y 8). De tal manera que si el grupo o la campaña tienen activadas la atención
automática de llamadas entrantes, el agente responderá de dicha manera ante cada llamada entrante de cualquier campaña, quedando sin efecto los parámetros
"Tiempo de ring" y "Tiempo de reintento", como se mencionó en la "Nota 2" de ésta sección.

.. image:: images/users_group_config.png

*Figura 6: Configuración de grupo de agentes*

.. image:: images/agents_in_campaign_1.png

*Figura 7: Configuraciones para agentes en campaña*

.. image:: images/agents_in_campaign_2.png

*Figura 8: Configuraciones para agentes en campaña*
