Llamadas hacia otros agentes
****************************
Para ejecutar una llamada hacia otro agente de OMniLeads, debemos acudir al botón "Llamar fuera de campaña" disponible en la parte inferior del webphone.
Al clikear dicho botón, se despliega una ventana que nos facilita la selección de un agente del listado total para luego ejecutar la llamada:

.. image:: images/about_agent_withoutcamp_calls.png

*Figura 1: Llamada fuera de campaña*

Llamadas externas sin campaña asociada
**************************************
A veces, es necesario ejecutar una llamada hacia el exterior (número de abonado o extensión de la central PBX), sin la necesidad de gestionar el contacto.
Ésto se permite a partir de clickear el botón "Llamar fuera de campaña" disponible en la parte inferior del webphone.
La ventana desplegada, cuenta con un campo para introducir el número a marcar:

.. image:: images/about_agent_withoutcamp_calls.png

*Figura 2: Llamada fuera de campaña*

Poner en espera una llamada
***************************
En medio de cualquier tipo de llamada en curso, el agente puede poner en espera al teléfono en el otro extremo
de la comunicación. Esto se logra haciendo click sobre el botón "Hold" del webphone de agente:

.. image:: images/about_agent_callactions_hold_1.png

*Figura 3: Llamada en espera*

Al disparar la acción de "hold", el otro extremo de la llamada queda escuchando la música de espera, mientras que el agente puede volver a retomar la
llamada cuando desee, simplemente clickeando el botón de "unhold", tal como se indica en la siguiente figura:

.. image:: images/about_agent_callactions_hold_2.png

*Figura 4: Saliendo de llamada en espera*

Ésta funcionalidad puede ser utilizada en cualquier tipo de llamada.

Transferencias y conferencias
*****************************
Dentro del abanico de posibilidades de transferencias de llamadas que se pueden realizar en el sistema, tenemos las siguientes:

**Transferencia directa hacia otro agente**

El "agente A" se encuentra en una llamada activa y desea transferir la llamada hacia el "agente B" de manera directa.
En éste caso, se selecciona el botón de transferencia disponible en el webphone, y luego se selecciona "Transferencia ciega" como tipo de transferencia, pudiendo elegir al agente destino:

.. image:: images/about_agent_callactions_ag2ag_bt.png

*Figura 5: Transferencia ciega de "agente A" a "agente B"*

En éste caso, la llamada automáticamente es despachada hacia el "agente B", quedando liberado el webphone del "agente A". Una vez disparada ésta
transferencia, no se puede volver a recuperar la llamada original, ni tampoco el "agente A" puede conocer si la llamada fue atendida o no por el "agente B".

**Transferencia directa hacia teléfono externo**

El "agente A" se encuentra en una llamada activa y desea transferir la llamada hacia un "teléfono" externo a OMniLeads de manera directa. Cuando decimos externo, nos referimos
a una llamada que se genera hacia afuera del sistema, pudiendo ser una extensión de la PBX de la compañía o bien un teléfono externo de la PSTN.

En éste caso, se selecciona el botón de transferencia disponible en el webphone y luego se selecciona "Transferencia ciega" como tipo de transferencia, y se debe introducir el número destino en el recuadro,
como lo indica la siguiente figura:

.. image:: images/about_agent_callactions_ag2out_bt.png

*Figura 6: Transferencia ciega de "agente A" a "número externo"*

En éste caso, la llamada automáticamente es despachada hacia el teléfono destino, quedando liberado el webphone del "agente A". Una vez disparada ésta
transferencia, no se puede volver a recuperar la llamada original, ni tampoco el "agente A" puede conocer si la llamada fue atendida o no por el teléfono
destino de la transferencia.

**Transferencia con consulta hacia otro agente**

El "agente A" se encuentra en una llamada activa y desea transferir la llamada hacia el "agente B" de manera consultativa, es decir logrando que el
teléfono externo quede en espera mientras el "agente A" abre un nuevo canal hacia el "agente B". Si la llamada entre ambos se establece y el "agente B"
desea recibir la transferencia, entonces el "agente A" corta la llamada y automáticamente el teléfono externo queda unido en una llamada con el "agente B".
Ésto, se muestra en la siguiente figura:

.. image:: images/about_agent_callactions_ag2ag_ct.png

*Figura 7: Transferencia consultativa de "agente A" a "agente B"*

En éste escenario también puede ocurrir:

- Que no se logre contactar al "agente B", caso en el cual el "agente A" puede cancelar la transferencia durante el ring hacia el "agente B" con el botón "Cancelar transferencia" del webphone.
- Se logra el contacto con el "agente B" pero éste no puede o no quiere proceder con la transferencia, por lo tanto el "agente B" debe cortar la llamada y atomáticamente vuelve a quedar el "agente A" con el teléfono externo enlazados.

**Conferencia de a 3 entre el número externo, agente A y agente B**

Éste caso, es un escenario posible dentro de una transferencia consultativa, ya que la acción a ejecutar por el agente que impulsa la conferencia "agente A", es en un principio
una transferencia consultativa, sólo que al momento de entablar conversación el "agente A" hacia el "agente B" (mientras la persona externa "numero externo" se encuentra en espera),
el "agente A" debe seleccionar el botón de "Confer" disponible en el webphone de agente, y de ésta manera quedan las 3 partes en un salón de conferencia:

.. image:: images/about_agent_callactions_3way_confer_internal.png

*Figura 8: Conferencia de a 3 entre "agente A", "agente B" y número externo*

**Transferencia con consulta hacia teléfono externo**

El "agente A" se encuentra en una llamada activa y desea transferir la llamada hacia un "teléfono" externo de manera consultativa, es decir logrando que
el "telefono externo A" quede en espera mientras el "agente A" abre un nuevo canal hacia el "teléfono externo B". Si la llamada entre ambos se establece
y el "teléfono externo B" desea recibir la transferencia, entonces el "agente A" corta la llamada y automáticamente el "teléfono externo A" queda unido
en una llamada con el "teléfono externo B". Ésto, se muestra a continuación:

.. image:: images/about_agent_callactions_ag2out_ct.png

*Figura 9: Transferencia consultativa de "agente A" a "teléfono externo"*

En este escenario también puede ocurrir:

- Que no se logre contactar al "teléfono externo B", caso en el cual el "agente A" puede cancelar la transferencia durante el ring hacia el "teléfono externo B" con el botón "Cancelar transferencia" del webphone.
- Se logra el contacto con el "telefono externo B" pero éste no puede o no quiere proceder con la transferencia, por lo tanto el "teléfono externo B" debe cortar la llamada y atomáticamente vuelve a quedar el "agente A" con el "teléfono externo A" enlazados.

**Conferencia de a 3 entre el número externo A, agente y un número externo B**

Bajo éste escenario, el "agente A" puede armar una conferencia de a 3 entre el "número externo A", es decir la persona que inicialmente se encuentra
en llamada con "agente A" y un "numero externo B", que puede ser la extensión de una PBX o un abonado de la PSTN, de manera tal que queden las 3 partes
en una sala de conferencia.

Para llevar a cabo ésta acción, el "agente A" debe iniciar una *transferencia consultativa* hacia el "numero externo B", y una vez en llamada con éste último,
el agente debe seleccionar el botón de "Confer" de su webphone, como se muestra en la siguiente figura:

.. image:: images/about_agent_callactions_3way_confer_out.png

*Figura 10: Conferencia de a 3 entre "agente A", "número externo A" y "número externo B"*

.. image:: images/about_agent_callactions_3way_confer_switch.png

*Figura 11: Botón de confer del webphone*

**Transferencia a otra campaña**

Bajo éste escenario, el "agente A" se encuentra en una llamada activa y desea transferir la llamada hacia una campaña entrante. A la hora de seleccionar
el tipo de transferencia, se debe marcar "Transferencia ciega", ya que la llamada es lanzada sobre la cola de espera de la campaña destino:

.. image:: images/about_agent_callactions_ag2camp.png

*Figura 12: Transferencia de "agente A" a "campaña entrante"*

Como se trata de una transferencia directa, la llamada automáticamente es despachada hacia el teléfono destino, quedando liberado el webphone del "agente A".
Una vez disparada ésta transferencia, no se puede volver a recuperar la llamada original, ni tampoco el "agente A" puede conocer si la llamada fue atendida o no.

Observar grabación de llamada
*****************************
Ésta funcionalidad del webphone de agente, permite a éste poder generar una marca sobre la grabación de la llamada. La idea es que luego, desde el módulo
de búsqueda de grabaciones, se pueda recuperar grabaciones "observadas" por los agentes y allí también desplegar la observación que realizó el agente sobre la grabación de la llamada.

.. image:: images/about_agent_callactions_tag_call.png

*Figura 13: Marca de grabación de llamada*

Como se indica en la figura anterior, luego de seleccionar el botón para marcar la llamada, se depliega un campo de texto para que el agente pueda
describir la situación.

Finalmente, en el módulo de grabación de OMniLeads, se puede recuperar dicha grabación y observar lo que el agente escribió al respecto.

Grabar llamada bajo demanda
***************************
Ésta funcionalidad permite grabar una llamada, a partir del momento de hacer click en el botón que se muestra en la siguiente figura:

.. image:: images/about_agent_callactions_record_call.png

La funcionalidad está habilitada exclusivamente para llamadas pertenecientes a campañas con la opción de realizar grabaciones, desactivada.

La grabación de la llamada puede terminarse a elección del usuario presionando el mismo botón en medio de dicha grabación, y si no se hace ésto,
la llamada se grabará hasta el final de la llamada:

.. image:: images/about_agent_callactions_stop_record_call.png

Agendamiento de llamadas
************************
La funcionalidad de agendamiento de llamadas, permite al sistema volver a procesar un contacto hacia el futuro. La idea es no descartar al mismo, sino
seguirlo gestionando.

**Agendamiento personal**

Cuando el agente requiere volver a llamar a un contacto determinado, puede generar un recordatorio en su agenda personal, para luego al listar dicha
agenda, contar con la entrada que le recuerda el horario y contacto que debe llamar.

El agendamiento de llamadas es una calificación que se encuentra por defecto siempre como calificación de contacto:

.. image:: images/about_agent_callactions_agenda_1.png

*Figura 14: Agenda personal*

Luego de guardar la calificación, se despliega un formulario para seleccionar la fecha, hora y motivo de la agenda personal del contacto:

.. image:: images/about_agent_callactions_agenda_2.png

*Figura 15: Detalles de la agenda personal*

Finalmente, la entrada en la agenda personal del agente, quedará disponible ingresando al menú *Agendas*:

.. image:: images/about_agent_callactions_agenda_3.png

*Figura 16: Detalles de la agenda personal*

**Agendamiento global de llamadas predictivas**

Éste tipo de agendamiento es sólo aplicable a campañas con discador predictivo, ya que tiene como finalidad volver a colocar al contacto dentro de la
lista de números a llamar por el discador. En éste escenario, el discador simplemente vuelve a llamar a dicho número agendado hacia el final de la campaña,
es decir no se puede elegir ni una fecha u horario ni tampoco sobre qué agente va a caer el contacto llamado nuevamente por el discador.

Se trata de una funcionalidad que permite no descartar al contacto, pero sin implicar un seguimiento personal por parte del agente.

Para generar una agenda de éste tipo, se debe calificar al contacto con la calificación "agenda" pero luego seleccionar "global" en el menú de selección del tipo de agenda.
