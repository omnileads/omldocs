.. _about_pbx_integration:

**********************************
Integración entre OMniLeads y PBXs
**********************************
Vamos a abordar éste ejemplo, utilizando Issabel-PBX (un proyecto de software libre bien conocido). Sin embargo, todo lo expuesto aquí puede ser
extrapolado como configuración para cualquier PBX con soporte SIP:

.. image:: images/pbx_integration.png
      :align: center

.. Note::

  Los pasos descriptos en ésta sección, son aplicables tanto al esquema donde OMniLeads se encuentra en un host exclusivo y la PBX en otro, así como también para el caso en que
  OMniLeads se ejecuta en Docker conviviendo dentro del mismo host de la PBX.

Configuración del troncal SIP en la PBX
****************************************
Seleccionamos la creación de un nuevo troncal SIP y completamos la configuración con los siguientes parámetros:

* En caso de tener OMniLeads en un host y la IP-PBX en otro host dentro de la red LAN:

.. code-block:: bash

 type=friend
 host=XXX.XXX.XXX.OML
 port=5161
 disallow=all
 allow=alaw
 qualify=yes
 secret=omnileads
 fromuser=issabel
 defaultuser=issabel
 context=from-internal

* En caso de tener OMniLeads en la nube y la IP-PBX en otro host dentro de la red LAN:

.. code-block:: bash

 type=friend
 host=XXX.XXX.XXX.OML
 port=5162
 disallow=all
 allow=alaw
 qualify=yes
 secret=omnileads
 fromuser=issabel
 defaultuser=issabel
 context=from-internal

* En caso de ejecutar OMniLeads con Docker dentro del sistema operativo base de la IP-PBX:

.. code-block:: bash

 type=friend
 host=XXX.XXX.XXX.PBX
 port=5163
 disallow=all
 allow=alaw
 qualify=yes
 secret=issabelOML
 fromuser=issabel
 defaultuser=issabel
 context=from-internal

Observar que lo único que cambia entre las diferentes posibilidades es el parámetro **port**. Ésto está relacionado con el hecho de que en OML se utiliza
un puerto SIP para cada tipo de escenario: LAN, NAT cloud o Docker.

 .. image:: images/telephony_pjsip_LAN_pbx_issabel_trunk.png
       :align: center

.. image:: images/telephony_pjsip_LAN_pbx_issabel_trunk2.png
      :align: center

Una vez disponible nuestro troncal SIP, pasamos a comprobar accesibilidad utilizando el CLI de Asterisk de la IP-PBX.
Establecemos una sesión bash dentro del host donde se ejecuta Issabel-PBX y lanzamos el siguiente comando:

.. code-block:: bash

  asterisk -rx 'sip show peers'

Si todo va bien, deberíamos observar OK en la línea de salida correspondiente al nuevo troncal SIP, ya sea con el puerto 5161, 5162 o 5163:

.. image:: images/telephony_pjsip_LAN_pbx_issabel_trunk3.png
      :align: center

Configuración del troncal SIP en OMniLeads
******************************************
Una vez generado el troncal SIP del lado de la IP-PBX, se procede con la generación de la contraparte correspondiente a OMniLeads.

* En caso de tener OMniLeads en un host y la IP-PBX en otro host, utilizamos el siguiente bloque de configuración (plantilla PBX LAN):

.. code-block:: bash

 type=wizard
 transport=trunk-transport
 accepts_registrations=no
 sends_auth=yes
 sends_registrations=no
 accepts_auth=yes
 endpoint/rtp_symmetric=no
 endpoint/force_rport=no
 endpoint/rewrite_contact=no
 endpoint/timers=yes
 aor/qualify_frequency=60
 endpoint/allow=alaw,ulaw
 endpoint/dtmf_mode=rfc4733
 endpoint/context=from-pbx
 remote_hosts=XXX.XXX.XXX.PBX:5060
 inbound_auth/username=issabel
 inbound_auth/password=issabelOML
 outbound_auth/username=omnileads
 outbound_auth/password=issabelOML

* En caso de tener OMniLeads en un host cloud y la IP-PBX en otro host, utilizamos el siguiente bloque de configuración (plantilla PBX NAT):

.. code-block:: bash

 type=wizard
 transport=trunk-nat-transport
 accepts_registrations=no
 sends_auth=yes
 sends_registrations=no
 accepts_auth=yes
 endpoint/rtp_symmetric=yes
 endpoint/force_rport=yes
 endpoint/rewrite_contact=no
 endpoint/timers=yes
 aor/qualify_frequency=60
 endpoint/allow=alaw,ulaw
 endpoint/dtmf_mode=rfc4733
 endpoint/context=from-pbx
 remote_hosts=XXX.XXX.XXX.PBX:5060
 inbound_auth/username=issabel
 inbound_auth/password=issabelOML
 outbound_auth/username=omnileads
 outbound_auth/password=issabelOML

* En caso de ejecutar OMniLeads con Docker dentro del sistema operativo base de la IP-PBX, utilizamos el siguiente bloque de configuración (plantilla Docker):

.. code-block:: bash

 type=wizard
 transport=trunk-nat-docker-transport
 accepts_registrations=no
 sends_auth=yes
 sends_registrations=no
 accepts_auth=yes
 endpoint/rtp_symmetric=yes
 endpoint/force_rport=yes
 endpoint/rewrite_contact=yes
 endpoint/timers=yes
 aor/qualify_frequency=60
 endpoint/allow=alaw,ulaw
 endpoint/dtmf_mode=rfc4733
 endpoint/context=from-pbx
 endpoint/rtp_symmetric=yes
 remote_hosts=XXX.XXX.XXX.PBX:5060
 inbound_auth/username=issabel
 inbound_auth/password=issabelOML
 outbound_auth/username=omnileads
 outbound_auth/password=issabelOML

Quedando efectiva nuestra troncal, pasamos a controlar si Issabel está accesible desde OMniLeads, utilizando el CLI de Asterisk de OMniLeads.

.. code-block:: bash

  asterisk -rx 'pjsip show endpoints'

.. Note::

  Si estamos ejecutando OMniLeads sobre Docker, para acceder al contenedor que ejecuta el componente Asterisk de OMniLeads, debemos ejecutar el comando:
  **docker exec -it oml-asterisk-prodenv** , y a partir de allí se invoca el CLI.

La salida del comando debería ser similar a la siguiente figura:

  .. image:: images/telephony_pjsip_LAN_pbx_oml_trunk2.png
        :align: center

En éste punto, existe un troncal SIP entre ambos sistemas telefónicos, quedando pendiente la configuración del enrutamiento de llamadas entre ambos
sistemas.

Finalmente, ponemos énfasis en relacionar parámetros entre la configuración del troncal SIP en Issabel con la de OMniLeads.

Una imagen vale más que 1.000 palabras:

.. image:: images/telephony_pjsip_LAN_pbx_trunk_relationship.png
      :align: center

Cómo enviar llamadas desde la IP-PBX hacia OMniLeads
****************************************************
A continuación, se plantea una forma de conectar los recursos de la IP-PBX (rutas entrantes, IVRs, anuncios, extensiones, etc.) con OMniLeads. Es decir,
que por ejemplo desde una opción del IVR principal de la compañía se pueda derivar a una campaña entrante de OMniLeads, o bien que una extensión pueda
contactar o transferir una llamada hacia una campaña entrante o agente de OMniLeads.

Ésto es completamente viable utilizando las *extensiones custom* de la IP-PBX, en nuestro caso ejemplar, Issabel-PBX:

.. image:: images/pbx_integration_pbx2oml.png
      :align: center

Llamadas hacia rutas entrantes de OMniLeads
*******************************************
Se plantea entonces el ejemplo donde se desea crear una *extensión custom* que al marcarla desde otra extensión o bien invocarla desde algún objeto de
la PBX (IVR, ruta entrante, anuncio, etc.) establezca un canal contra OMniLeads, particularmente apuntando a una *ruta entrante*, la cual puede a su vez
enviar la llamada hacia una campaña entrante.

Por un lado entonces, tendremos una ruta entrante en OMniLeads, apuntando por ejemplo, a una campaña entrante:

.. image:: images/pbx_integration_inr_oml.png
      :align: center

Teniendo en cuenta que el DID elegido fue *098098*, en la IP-PBX hay que generar una *extensión* del tipo *custom*, donde la cadena de *Dial* apunte al
troncal SIP contra OMniLeads y el número enviado sea precisamente *098098*:

.. image:: images/pbx_integration_exten_to_inr.png
      :align: center

En la figura remarcamos 3 elementos:

(1) El número de extensión, no necesariamente debe ser idéntico al número enviado hacia OMniLeads (3). Puede ser un número cualquiera, siempre y cuando la cadena de *Dial* de la *extensión custom* coincida con el DID de la ruta entrante de OML (098098 para nuestro ejemplo).
(2) El *troncal* a donde apuntar la extensión custom. Éste valor debe coincidir con el campo *Trunk Name* en el troncal SIP contra OMniLeads generado en la IP-PBX.
(3) El número a enviar por el troncal tiene que coincidir con el DID de la ruta entrante de OMniLeads.

De ésta manera entonces, cualquier extensión de la IP-PBX podrá marcar o transferir una llamada hacia ésta *extensión custom* y la misma será enviada hacia la ruta entrante
correspondiente en OMniLeads, para finalmente conectar sobre una campaña entrante o el elemento asignado como destino de la ruta entrante en OMniLeads.

Como mención final, está claro que podremos tener en la IP-PBX tantas extensiones custom apuntando a diferentes rutas entrantes de OMniLeads como querramos.

Llamadas hacia agentes de OMniLeads
************************************
Partiendo de la siguiente figura del listado de agentes, tomemos al agente *Adrian Belew*. Observar que su ID es igual a "1" y su número SIP es "1006". Por lo tanto, a la hora de conformar un llamado
hacia el webphone de dicho agente, se debe *discar* un número conformado por el *número SIP* con su *ID de agente*. En nuestro ejemplo, sería **10061** para el agente *Adrian Belew* y **10072** para el agente *Mikael Ackerfeldt*:

.. image:: images/pbx_integration_agents_oml.png
      :align: center

Ahora bien, a la hora de generar la configuración en la PBX para poder enviar llamadas a los agentes, tenemos 2 alternativas:

(1) Utilizar una ruta saliente desde la PBX hacia OMniLeads, tal como indicamos en la siguiente figura:

.. image:: images/pbx_integration_agents_oml_outr.png
      :align: center

En éste caso, cualquier extensión de la PBX podrá generar una llamada hacia un agente marcando la combinación citada en el párrafo anterior.

(2) Generar una *extensión custom* por cada agente de OML, es decir que la cadena de *Dial* de la extensión custom estará conformado ya no por un DID de ruta entrante de OMniLeads como fue en el caso de vincular con rutas entrantes, sino que será una combinación del *ID del agente* y su *número SIP*.

Ésto se indica la siguiente figura:

.. image:: images/pbx_integration_exten_to_agent.png
      :align: center

En la figura remarcamos 3 elementos:

(1) El número de extensión, no necesariamente debe ser idéntico al número enviado hacia OMniLeads (3). Puede ser un número cualquiera, siempre y cuando la cadena de *Dial* de la *extensión custom* coincida con la concatenación del ID de agente y su número SIP (10061 para nuestro ejemplo).
(2) El *troncal* a donde apuntar la extensión custom. Éste valor debe coincidir con el campo *Trunk Name* en el troncal SIP contra OMniLeads generado en la IP-PBX.
(3) El número a enviar por el troncal tiene que coincidir con la concatenación del ID de agente y su número SIP (10061 para nuestro ejemplo).

Se deberá repetir el procedimiento para cada agente que haya que vincular dentro de la IP-PBX.
De ésta manera, el integrador de la PBX podrá utilizar para las extensiones una numeración flexible.

Cualquiera de las 2 alternativas son viables y obtendrán el resultado deseado.

Llamadas desde OMniLeads hacia la PSTN y recursos de la IP-PBX
**************************************************************
Finalmente, vamos a generar el enrutamiento saliente dentro de OMniLeads, que permita a los agentes y discadores lanzar llamadas hacia la PSTN por un lado,
a su vez que permitimos que los agentes puedan marcar o transferir llamadas hacia recursos de la IP-PBX como extensiones, ring groups, colas de llamadas, etc., por el otro.

.. image:: images/pbx_integration_oml2pstn.png
      :align: center

.. image:: images/pbx_integration_oml2pbx.png
      :align: center

Simplemente, se debe añadir una nueva ruta saliente que apunte al troncal hacia la IP-PBX:

.. image:: images/pbx_integration_oml_outr.png
      :align: center

De ésta manera, la integración queda completamente funcional, y ambos sistemas pueden realizar todo tipo de llamadas e interacciones.
