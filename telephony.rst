*************************************
Configuración del módulo de telefonía
*************************************
En ésta sección, se exponen las configuraciones necesarias a realizar para que nuestra aplicación pueda interactuar con la PSTN (Red Telefónica Pública Conmutada),
de manera tal que los agentes puedan tomar llamadas provenientes del exterior así como también marcar llamadas hacia teléfonos externos.

.. important::

  OMniLeads admite sólamente SIP como tecnología de interconexión con otros conmutadores de telefonía. Por lo tanto, el integrador podrá
  configurar troncales SIP de proveedores ITSP, troncales SIP contra sistemas PBX y/o troncales SIP contra Gateways FXO/E1/T1.

Configuración de troncales SIP
******************************
Para acceder a la configuración, debemos ingresar al menú *Telefonía -> Troncales SIP* y allí añadir un nuevo troncal PJSIP.
Se va a desplegar un formulario similar al de la siguiente figura:

.. image:: images/telephony_pjsiptrunk_abm.png
    :align: center

Los campos del formulario son:

- **Trunk Name**: Es el nombre del troncal. Debe ser alfanumérico, sin espacios ni caracteres especiales (por ejemplo, Trunk_provider_A).
- **Number of channels**: Es la cantidad de canales que permite el vínculo.
- **Caller id**: Es el número con el que saldrán las llamadas por el troncal.
- **SIP details**: En éste campo de texto se proporcionan los parámetros SIP, usando sintaxis de `PJSIP configuration Wizard <https://wiki.asterisk.org/wiki/display/AST/PJSIP+Configuration+Wizard>`_ de Asterisk.

A continuación, desplegamos algunas plantillas sugeridas para los tipos de escenarios planteados como casos de uso típicos de OMniLeads:

.. toctree::
  :maxdepth: 1

  telephony_pjsip_templates.rst

Configuración para el enrutamiento de llamadas salientes
********************************************************
OMniLeads permite gestionar el enrutamiento de llamadas salientes sobre múltiples troncales SIP (previamente creados), de manera tal que
utilizando criterios como el *largo* o el *prefijo del número*, se puede determinar por cuál vínculo SIP encaminar la llamada. Además, es posible
mantener una lógica de *failover* entre los diferentes troncales SIP asignados a una ruta saliente.

Para acceder a la vista de configuración de rutas salientes, ingresar al menú *Telefonía -> Rutas salientes*:

.. image:: images/telephony_outr.png
*Figure 2: Outbound route*

- **Nombre**: Es el nombre de la ruta (alfanumérico, sin espacios ni caracteres especiales).
- **Ring time**: Es el tiempo (en segundos) que las llamadas cursadas por ésta ruta, intentarán establecer una conexión con el destino,
  antes de seguir intentando por el próximo troncal, o bien descartarse la llamada.
- **Dial options**: Son las opciones de la aplicación "Dial" utilizadas por Asterisk en bajo nivel.
- **Patrones de discado**: Mediante patrones, se puede representar los *tipos de llamadas* que serán aceptadas por la ruta,
  y así colocadas sobre el troncal SIP para finalmente alcanzar el destino deseado.

  Para comprender cómo se representan los dígitos utilizando *patrones*, se recomienda leer el siguiente enlace:

  https://www.voip-info.org/asterisk-extension-matching/

  Dentro de cada patrón ingresado, hay 3 campos:

  * **Prepend**: Son los dígitos que se mandan por el troncal SIP como adicionales al número discado. Es decir, llegan al troncal posicionados
    delante del número marcado.
  * **Prefijo**: Son los dígitos que pueden venir como “prefijo” de una llamada marcada y los cuales serán removidos en el momento de enviarlos por
    el troncal SIP.
  * **Patrón de discado**: Se busca representar en éste campo, el patrón de dígitos autorizados que la ruta va a procesar para enviarlo a un troncal SIP, y sacar así la llamada hacia el exterior.

- **Secuencia de troncales**: Son los troncales SIP sobre los cuales la ruta saliente va a intentar establecer la llamada discada por OMniLeads.
  Si la llamada falla en un troncal, se sigue intentando con el siguiente.

Configuración del módulo AMD
****************************
El módulo de AMD de Asterisk, puede configurarse en OMniLeads usando la siguiente interfaz:

.. image:: images/amd_conf_page.png
*Figure 2: Amd configuration page*

Para mayor información acerca de éste módulo de Asterisk, remitirse al siguiente enlace:

https://wiki.asterisk.org/wiki/display/AST/Application_AMD

Configuración de esquema de nombres de archivos de grabaciones de llamadas
**************************************************************************
La siguiente sección, permite configurar el formato que tendrán los nombres de los archivos de grabaciones generadas por
llamadas en el sistema.

Al seleccionar algunas de las opciones mostradas en la página, esto impactará directamente en el nombre de los archivos de grabaciones.

.. important::

  Tener en cuenta que el ID de agente como variable, no está disponible para campañas entrantes ni salientes de discador.

.. image:: images/recordings_scheme_page.png
*Figure 2: Recordings scheme page*

Configuración de enrutamiento de llamadas entrantes
***************************************************
El tratamiento de llamadas entrantes se abordará en la sección *Campañas Entrantes* de ésta documentación, ya que para poder operar con dicho
módulo, debemos al menos tener creado algún objeto (IVR, condicional de tiempo, campaña entrante, etc.) hacia a donde enrutar cada DID generado.

:ref:`about_inboundroutes`.
