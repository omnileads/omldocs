.. _about_manualcamp:

Creación de campaña manual
**************************
Para crear una nueva campaña manual, se debe ingresar al menú *Campañas -> Campañas manuales -> Nueva campaña*. El proceso de creación consta de
un wizard de 2 pantallas.

La primera pantalla nos invita a indicar una serie de parámtros de la campaña, como lo indica la siguiente figura:

.. image:: images/campaigns_manual_wizard1.png

*Figura 1: Parámetros de la campaña*

- **Nombre:** Nombre de la campaña.
- **Base de datos de contactos:** Se utiliza para desplegar datos extras al teléfono, a la hora de ejecutar una llamada a un contacto de la campaña.
- **Tipo de interacción:** Indica si la campaña va a operar con formularios de OMniLeads, o bien va a ejecutar una invocación hacia un CRM por cada llamada conectada.
- **URL externa:** En caso de haber seleccionado la invocación a una URL externa en cada llamada, aquí se indica cuál de los CRMs definidos debe invocar la campaña.
- **Grabar llamados:** Habilitar la grabación de todas las llamadas que se cursen por la campaña.
- **Objetivo:** Se define como la cantidad de *gestiones positivas* que se esperan para la campaña. En la supervisión de la campaña se muestra en tiempo real el porcentaje de avance de la campaña respecto al objetivo definido.
- **Sistema externo:** Aquí se adjudica el sistema de gestión externo que ejecutaría "click to call" sobre la campaña, en caso de así desearlo.
- **ID en sistema externo:** Éste campo debe contener el ID que posee la campaña dentro del sistema de gestión externo desde el cual llegarán los "click to call" o "solicitudes de calificación".
- **Ruta saliente:** Se le asigna una ruta saliente existente a una campaña.
- **CID ruta saliente:** Éste campo debe contener el CID asignado para una ruta saliente existente a una campaña.
- **Speech:** El speech de campaña para ser desplegado en la consola de agente en las llamadas de la campaña.

.. note::
  El tema de la base de contactos en las campañas manuales (y también entrantes) plantea un escenario flexible, ya que es opcional el hecho de asignar una base de contactos
  a éste tipo de campañas. En éste caso, la base de contactos es utilizada si deseamos que cada vez que un agente marca un teléfono que corresponde con un contacto
  de la base, se puedan recuperar los datos (columnas extras al teléfono) del mismo. Además, el hecho de trabajar con una base de contactos en una campaña manual
  permite calificar cada contacto llamado.

En la segunda pantalla se deben asignar las calificaciones que se requieran para que los agentes puedan clasificar cada llamada realizada al contacto. Como se puede apreciar
en la siguiente figura, en nuestro ejemplo manejamos 2 calificaciones que disparan 2 formularios diferentes:

.. image:: images/campaigns_calldispositions_add.png

*Figura 2: Calificaciones de campaña*

En los siguientes pasos, se pueden añadir supervisores y agentes a nuestra campaña:

.. image:: images/campaigns_manual_wizard_3.png

*Figura 3: Asignación de agentes*

Interacción de agente con campaña
*********************************
Finalmente, contamos con nuestra nueva campaña manual. Cuando un agente asignado a la misma realice un login a la plataforma y comience a marcar llamadas desde
su webphone, el sistema le permitirá seleccionar la campaña sobre la cual va a asignar cada llamada manual generada desde el webphone, tal como se expone
en la siguiente figura:

.. image:: images/campaigns_manual_agconsole1.png

*Figura 4: Selección de llamada manual a campaña*

Cuando un agente se encuentra online y marca un teléfono correspondiente a un contacto de la base de la campaña, el contacto cuyo teléfono coincide con el teléfono marcado por el agente,
es desplegado como contacto a seleccionar y así despliega sus datos en la pantalla de agente. También se permite generar un nuevo contacto. Entonces, el agente puede, o bien confirmar que la llamada se dispara hacia
el contacto listado, o bien crear un nuevo contacto y marcarlo.
Los datos (extras al teléfono) del contacto son desplegados en la pantalla de agente:

.. image:: images/campaigns_manual_agconsole2.png

*Figura 5: Selección de contacto*

Si se selecciona llamar al contacto listado, entonces los datos del mismo son desplegados en pantalla, como lo expone la siguiente figura:

.. image:: images/campaigns_manual_agconsole3.png

*Figura 6: Datos del contacto*

De ésta manera, el agente puede asignar una calificación sobre el contacto llamado, como se muestra en la siguiente figura:

.. image:: images/campaigns_manual_agconsole4.png

*Figura 7: Calificación de contactos*

Por otro lado, si el teléfono marcado no corresponde a ningún contacto de la base, entonces el sistema permite al agente buscar el contacto en la base o generar un nuevo contacto. En caso de tratarse
de una campaña sin base de contactos, entonces cada llamado que realice un agente implica que se genere el contacto asociado a la llamada marcada, como se aprecia en las siguientes 2 figuras:

.. image:: images/campaigns_manual_agconsole5.png

*Figura 8: Añadir un nuevo contacto a la base de datos de la campaña 1*

.. image:: images/campaigns_manual_agconsole6.png

*Figura 9: Añadir un nuevo contacto a la base de datos de la campaña 2*

Entonces, al momento de marcar a un número que no devuelva un contacto, el agente pasará por una vista en la que primero debe añadir el contacto como un registro de la base de la campaña para luego marcar.
Finalmente, se despliegan el nuevo contacto y la opción de clasificar la llamada con alguna calificación, como se aprecia en la siguiente figura:

.. image:: images/campaigns_manual_agconsole7.png

*Figura 10: Nuevo contacto llamado*

Campaña con base de datos multinum
**********************************
Como sabemos, OMniLeads admite que cada contacto de una base posea "n" números de teléfono de contacto, de manera tal que si el contacto no es encontrado en su número principal
(el primero de nuestro archivo CSV de base), pueda ser contactado a los demás números. En este caso, cada número de teléfono (que indicamos en la carga de la base) se genera
como un link dentro de los datos del contacto presentados sobre la pantalla de agente. Al hacer click sobre dicho enlace, se dispara una llamada hacia el número de teléfono extra
del contacto. En la siguiente figura se muestra dicho escenario:

.. image:: images/campaigns_prev_agconsole3.png

*Figura 11: Base de contactos multinum*

Por lo tanto, el agente puede intentar contactar a todos los números disponibles como "link" en la ficha del contacto, hasta finalmente calificar y pasar a uno nuevo.
