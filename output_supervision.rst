.. _about_supervision:

Supervisión
***********
Éste módulo permite visualizar el estado de las campañas entrantes, campañas salientes (manual, dialer y preview) y agentes.

.. image:: images/output_supervision_options.png

*Figura 1: Posibilidades de supervisión*

Visualización de status de agentes
**********************************
En la sección de agentes, se observan todos los agentes logueados en el sistema y el estado en el que se encuentran (Ready, OnCall, Paused, Dialing, Offline, Unavailable).

.. important::

  * Un agente debe estar asignado al menos a una campaña para que aparezca en éste módulo.
  * Cuando un agente pasa al estado "Offline", desaparece del listado de agentes.
  * Cuando un agente pasa al estado "Unavailable", significa que el agente perdió conexión o cerró el browser sin desloguearse.

.. image:: images/output_supervision_agentes.png

*Figura 2: Vista de supervisión de agentes*

Un supervisor puede tomar acciones sobre cada agente. Para ello son los 4 botones que aparecen al lado del estado. A continuación, se describe la función de cada uno (de izquierda a derecha):

  - **Espiar:** El supervisor escucha la **llamada activa** entre agente y cliente. Se puede hacer click en el botón *Finalizar* para terminar de escuchar, en cualquier momento.
  - **Espiar y susurrar:** El supervisor puede hablar al agente sin que el cliente lo perciba, durante una **llamada activa**. Se puede hacer click en el botón *Finalizar* para terminar de escuchar y susurrar, en cualquier momento.
  - **Pausar agente:** Suponiendo que el agente se fue a break y se olvidó ponerse en pausa, el supervisor puede con este botón inducir una pausa para que no reciba llamadas. Puede despausar oprimiendo nuevamente el mismo botón.
  - **Desloguear agente:** Suponiendo que el agente terminó su sesión y no se deslogueó correctamente del sistema (no presionó su nombre y dió click en salir, en el extremo derecho superior de la consola de agente), el supervisor puede desloguear al agente con este botón. Es importante hacer éste proceso para no tener tiempos de sesión incoherentes en el reporte de agentes.

.. note::

   El supervisor cuenta con un pequeño webphone, por lo que para poder ejecutar éstas acciones, es necesario que aparezca el mensaje de **Supervisor Registrado**.

Visualización de campañas entrantes
***********************************
Ésta vista expone un resumen de todas las campañas entrantes productivas, en términos de los resultados acumulados del día de la operación: Llamadas recibidas, atendidas, abandonadas, abandonadas durante el anuncio de bienvenida, tiempo promedio de espera, expiradas, en espera, tiempo promedio de abandono y gestiones (*) positivas dentro de cada campaña.

.. image:: images/output_supervision_camp_entrantes.png

*Figura 3: Vista de supervisión de campañas entrantes*

Visualización de campañas salientes
***********************************
Al igual que en el punto anterior, las campañas salientes también cuentan con un resumen actualizado en tiempo real, de los resultados de cada campaña: Llamadas discadas, atendidas, no atendidas y las gestiones (*) positivas de cada una.

.. image:: images/output_supervision_camp_salientes.png

*Figura 4: Vista de supervisión de campañas salientes*

Visualización de campañas dialer
********************************
Las campañas dialer también cuentan con un resumen actualizado en tiempo real, por un lado, la disposición de agentes, agentes online, agentes en llamada, agentes en pausa, y por el otro, la disposición de llamadas, discadas, atendidas, no atendidas, contestadores, canales discando, conectadas perdidas, gestiones y llamadas pendientes.

.. image:: images/output_supervision_camp_dialer.png

*Figura 5: Vista de supervisión de campañas dialer*

.. note::

  Se entiende por día, el día a día de operación desde las 00:00 hasta las 23:59. En el siguiente día, las estadísticas de campañas entrantes, salientes y dialer se reinician.

.. note::

  Se entiende por "gestión positiva", una llamada que el agente calificó con una calificación de gestión, calificación capaz de ejecutar un formulario de campaña.
