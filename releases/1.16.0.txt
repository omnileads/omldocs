===================================
Omnileads (OML) 1.16.0 release notes
===================================

*June 28, 2021*

What's new
=========================
- Agent Recording View is now an agent group parameter.
- Speech box is added to the Campaign Configuration and available once call is received.
- Agent Announcement added in Dialer Campaign Configuration. 
- Music-On-Hold can be configurable in Dialer Campagins.
- Improvements in Campaigns Wizards allow multiple supervisors to be added by a click.
- Search Box for Inbound Routes is added to improve management.
- New deploy scheme for an easier and more agile cluster deploy, allows execution as a cloud-init script to materialize infrastructure and components in public cloud ambients and on premise virtualization.
- Asterisk can be configured to run over a Linux host isolated from other components allowing vertical scalation in function of call traffic loads.
- New Recordings storage configuration options. Recordings can be stored in S3-compatible Cloud Object Storage or using remote resources mounted using NFS protocol as well
- New OMniLeads Deploy scheme over Docker Containers.
- Infrastructure as a code, new repository available with OMniLeads Deploy based in Terraform for Digital Ocean (https://gitlab.com/omnileads/terraform-digitalocean)
- Be careful! Software is now divided into new repositories per component (https://gitlab.com/omnileads: Asterisk, Kamailio, Nginx, Redis, Postgresql, Websocket, RTPEngine, Django/Python App), allowing isolated/independent services scheme in a DevOps cycle (code, build, test, release, deploy, upgrades) and facilitating deployments over Public Cloud / On-Premise / Docker containers. Read online documentation in depth! For Upgrades/Installs procedures.

Fixes and improvements
--------------------
- Fixes are applied over Campaign / Databases names when they reach API treatment limit.
- Improvements for agent events in Inbound Supervision Views.
- Improvements in agent labels when added to Live Campaigns.
- Better Caching Algorithm for transfer forms in Agent Console.
- New agenda information is added to the agent console.
- Labeling Improvement added to the Webphone Agent.
- Recordings download url problem fixed.
- Contact duplication when recycling campaigns fixed.
