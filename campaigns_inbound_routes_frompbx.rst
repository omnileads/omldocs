.. _about_inboundroutespbx:

*************************************************************
Derivación de llamadas entrantes desde la PBX hacia OMniLeads
*************************************************************
En ésta sección, se ejemplifica cómo configurar OMniLeads y una PBX basada en Asterisk, para derivar llamadas desde la PBXa hacia OMniLeads.

.. image:: images/campaigns_in_route_frompbx_ivr.png

*Figura 1: Derivación de llamadas desde PBX*

Partimos del hecho de considerar que existe un troncal SIP que vincula OMniLeads con la PBX.

Lo primero que se debe definir es la numeración asignada a la ruta entrante que va a procesar la llamada desde la PBX hacia un destino de OMniLeads, ya que éste número
(DID de la ruta) debe ser marcado por la PBX para enviar llamadas desde cualquier módulo (extensiones, IVRs, rutas entrantes, follow me, etc.) de dicha PBX, hacia el destino
configurado en la ruta de OMniLeads:

.. image:: images/campaigns_in_route_frompbx.png

*Figura 2: Parámetros de ruta entrante*

Tomando como ejemplo la ruta con el DID *123456* utilizado en la figura anterior, la PBX Asteisk deberá generar llamadas por el troncal SIP
hacia el número mencionado, cada vez que algún recurso de la PBX necesite alcanzar el destino *123456* de OMniLeads.

Si nuestra PBX Asterisk dispone de una interfaz web de configuración, entonces simplemente podemos generar una *Nueva extensión custom* y hacer que la misma apunte a
*SIP/trunkomnileads/123456*, donde "trunkomnileads" es el nombre configurado en la PBX para nombrar el troncal SIP con OMniLeads.

La idea de extensión apuntando hacia OMniLeads se ejemplifica en las siguientes figuras 3 y 4:

.. image:: images/campaigns_in_route_frompbx2.png

*Figura 3: Extensión custom para OMniLeads*

Si bien la extensión en la PBX puede tener cualquier numeración (se ejemplificó con *2222*), lo importante es enviar *123456* (en nuestro ejemplo) hacia OMniLeads,
como se resalta en la siguiente figura:

.. image:: images/campaigns_in_route_frompbx3.png

*Figura 4: Extensión custom para OMniLeads*

Una vez disponible la extensión en la PBX, sólo es cuestión de invocarla desde cualquier módulo de la PBX, como por ejemplo un IVR:

.. image:: images/campaigns_in_route_frompbx4.png

*Figura 5: Derivación a OMniLeads desde IVR*

Si bien en la figura anterior se ejemplifica la derivación de llamadas hacia campañas entrantes de OMniLeads desde un IVR de la PBX, podemos concluir
que también las extensiones de la PBX pueden marcar o transferir llamadas hacia OMniLeads, así como también módulos de la PBX como condiciones horarias,
follow me, rutas entrantes, etc. podrán invocar una *extensión custom* de la PBX que derive llamadas hacia OMniLeads.
