.. _about_install_digitalocean:

====================================
Deploy OMniLeads sobre Digital Ocean
====================================

Vamos a plantear una instalacion utilizando el servicio de cluster de PostgreSQL que proporciona el proveedor de infraestructura en la nube. De esta manera
nos ahorarmos el deploy y mantenimiento del motor sobre una instancia linux.

Por otro lado se plantea utilizar el servicio de S3 proporcionado por el proveedor, para el almacenamiento de las grabaciones y demas datos no-relacionales. De esta
manera se accede a un servicio de almacenamiento optimizado para la nube en terminos de escalabilidad, nivel de servicio y costos.

Deploy basico con PGSQL y Bucket S3
***********************************

Se plantea un despliegue simplificado en el cual se va a requerir un Droplet para contener a los componentes: OMLApp (Django/uwsgi), Asterisk, Kamailio,
RTPEngine, Nginx, Redis y Websockets, mientras que los servicios de PostgreSQL, S3 y Load balancer son implementados para adicionar robustez y seguridad a nuestra instancia
de OMniLeads.

.. image:: images/install_digital_ocean_arq.png


Pre-requisitos
**************

A la hora de plantear este formato de instalacion de OMniLeads en Digital Ocean, es necesario disponer de:

* **Una cuenta de DigitalOcean:** Contar con un usuario capaz de generar recursos en la nube del proveedor.

* **Una clave SSH disponible en nuestra cuenta:** Se debe `subir una clave SSH <https://snapshooter.com/blog/using-ssh-keys-for-digitalocean/>`_, para luego poder asignar a cada VM creada y así ingresar vía SSH a cada Linux host de manera segura.

* **Un user_key y secret_key para interactuar con SPACES:** "Spaces" es el nombre del object storage S3 compatible. Para interactuar con el mismo, se debe generar un `id y clave <https://www.digitalocean.com/community/tutorials/how-to-create-a-digitalocean-space-and-api-key>`_.

* **Un dominio propio, cuyos DNSs apunten hacia los de DigitalOcean:** El dominio es utilizado por Terraform a la hora de generar el URL y sus certificados SSL, de manera tal que al invocar dicha URL, resuelva la petición sobre Nginx de la instancia de OMniLeads. Por lo tanto, se debe configurar en su proveedor del dominio, un redireccionamiento hacia los DNSs de DigitalOcean.

  Links de interés:

    * https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars
    * https://digitaloceancode.com/como-configurar-un-nombre-de-dominio-para-tu-servidor
    * https://programacionymas.com/blog/como-enlazar-dominio-hosting-namecheap-digitalocean


Crear cluster de PGSQL
**********************

El proveedor proporciona la posibilidad de lanzar un cluster de DB admite en su configuracion mas basica un simple nodo, pudiendo ademas lanzar uno o dos nodos standby o replica.
Por lo que debemos lanzar la instancia de PGSQL v11. Esto nos debe arrojar los parametros de configuracion: conexion por la red privada (hostname), puerto, usuario
y clave.

.. code::

  username = doadmin
  password = AVNS_dsRe8_gHC3fY1gxFGR7
  host = private-db-postgresql-sfo3-68097-do-user-6023066-0.b.db.ondigitalocean.com
  port = 25060

.. image:: images/install_digital_ocean_pgsql_conecction.png

Durante la creacion de la instancia de postgres, se puede configurar el cloud-firewall asociado de manera tal que solo admitamos conenexiones provenientes de la VPC utilizada
para el despliegue.

.. image:: images/install_digital_ocean_pgsql_db.png

Ademas, debemos crear una base de datos nueva a la cual debemos asignarle un nombre: *omnileads* podria ser por ejemplo.

.. image:: images/install_digital_ocean_pgsql_create_db.png

Crear bucket spaces
*******************

Ahora vamos a crear un bucket para tambien asociar a la instalacion al momento de hacer el deploy de la Aplicacion.

.. image:: images/install_digital_ocean_create_bucket.png

Hay que tener en cuenta que para acceder al bucket, debemos contar con las credenciales para acceder al bucket S3.

Deploy de la aplicacion
***********************

Teniendo entonces disponible el bucket y la instancia de postgres, se procede con el deploy de la aplicacion.

.. important::

    Se debe definir como nombre del droplet el "hostname" requerido. Debemos tener en cuenta que esto esta encadenado con el FQDN/URL
    que utilizaran los usuarios para acceder a la App y tambien con el hecho de generar los certificados con Let's and crypt.

.. image:: images/install_digital_ocean_pgsql_bucket_list.png

Con respecto al archivo de first_boot_installer a configurar, se debe considerar especial atencion en las siguientes variables, ya que son
las inherentes a este formato de despliegue planteado:

.. code::

    export oml_infras_stage=digitalocean
    export oml_callrec_device=s3

    export s3_access_key=DO008GBFGGVUL9TAKYA8
    export s3_secret_key=VqGZvKgo8V/5j1YSd4O61iPgpCWA2Xyk3Vg4oyERS+A
    export s3_bucket_name=tenantx
    export s3_endpoint_url=https://sfo3.digitaloceanspaces.com
    export s3_region=us-east-1

    export oml_pgsql_host=db-postgresql-sfo3-70287-do-user-6023066-0.b.db.ondigitalocean.com
    export oml_pgsql_port=25060
    export oml_pgsql_db=omnileads
    export oml_pgsql_user=doadmin
    export oml_pgsql_password=AVNS_ir8olAQl_ip9Uoj1tc9
    export oml_pgsql_cloud=true

.. note::

    Si desea agregar sus propios certificados SSL: :ref:`about_install_own_ssl`


Agregar un registro DNS
***********************

Para poder ingresar a la instancia de la App usando un hostname/fqddn debemos agregar una nueva entrada sobre nuestro dominio asociado.
Es decir, debo definir una URL que resuelva sobre nuestra instancia de OMniLeads:

.. image:: images/install_digital_ocean_create_record_dns.png


Agregar certificado SSL let's and crypt
***************************************

Para generar un certificado y agregarlo a nuestra instancia Linux de OMniLeads, podemos seguir el siguiente
`paso a paso <https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-centos-7>`_.

Configuracion de Cloud Firewall
*******************************

Para proteger nuestra instancia de OMniLeads (Droplet) podemos definir un cloud-firewall, de manera tal que se exponga lo justo y necesario para poder
utilizar la aplicacion

Las reglas son muy sencillas, simplemente tener en cuenta que para la parte SIP VoIP (UDP 5161 & UDP 20000-30000), es decir cuando la aplicacion se conecta hacia la PSTN, se debera
restringir idealmente por direccion IP. Osea que partimos del hecho de que el extremo del troncal SIP cuenta con una IP publica fija (la IP del carrier o Telco).

.. image:: images/install_digital_ocean_create_firewall.png

Para mas informacion: ref:`about_seguridad`
