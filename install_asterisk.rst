.. _about_install_asterisk:

******************************
Deploy del componente Asterisk
******************************
Como es sabido, **Asterisk** implementa un script de *fisrt_boot_installer.tpl*, que puede ser invocado utilizando tecnología de provisioning,
o bien ser ejecutado manualmente con una previa parametrización de las variables implicadas.

Para obtener nuestro script, se debe lanzar el siguiente comando:

.. code-block:: bash

  curl https://gitlab.com/omnileads/omlacd/-/raw/master/deploy/first_boot_installer.tpl?inline=false > first_boot_installer.sh && chmod +x first_boot_installer.sh

Entonces, una vez que contamos con el script, pasamos a trabajar las variables, quitando comentarios y configurando sus valores dentro del propio script.
Vamos a listar y explicar cada una de éstas variables que deben ser ajustadas antes de ejecutar el script.

Variables de instalación
************************
* **oml_infras_stage**: Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise* como valor para la variable. **Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

* **oml_acd_release**: Aquí debemos indicar qué versión del componente se desea desplegar. Para ello, se deberá revisar el archivo `.gitmodules <https://gitlab.com/omnileads/ominicontacto/-/blob/master/.gitmodules>`_ de la versión de OMniLeads que se desee desplegar, y tomar el parámetro *branch* del componente, como valor de ésta variable.

* **oml_nat_ipaddr**: En el caso que Asterisk necesite salir hacia un destino/proveedor en la nube, con una IP pública particular como IP externa (external_media_address), se puede setear dicha IP en éste campo. Si se deje en NULL, la IP pública será auto-detectada.

* **oml_tz**: Se indica aquí la zona horaria de la instancia.

* **oml_app_host**: La dirección de red del Linux host que aloja el componente OMLApp. Ésto es necesario para realizar la conexión hacia Nginx y así terminar generando la suscripción al Websocket-Python para aprovisionamiento de los archivos de configuración (etc/asterisk).

* **oml_redis_host**: La dirección de red del Linux host que aloja el componente Redis. El dialplan de Asterisk se apoya en AGIs y consultas a Redis a la hora de aplicar las reglas de negocio sobre cada llamada.

* **oml_pgsql_host y oml_pgsql_port**: Dirección de red y puerto para establecer conexiones a PostgreSQL.

* **oml_pgsql_db, oml_pgsql_user y oml_pgsql_password**: Aquí citamos 3 parámetros vinculados entre sí, que son el nombre de la base de datos que va a alojar todas las tablas necesarias, el usuario y el password admitido para acceder a dicha base de datos.

* **oml_pgsql_ssl**: Éste valor sirve para setear ODBC en modo SSL. Por defecto y para PSQL interno, dejarlo en *NULL*. **Posibles valores**: *NULL, true*.

* **oml_pgsql_cloud**: Éste valor se debe setear en *true* si PostgreSQL corre en un cluster cloud. Por defecto y para PSQL interno, dejarlo en *NULL*. **Posibles valores**: *NULL, true*.

* **oml_ami_user y oml_ami_password**: Éstas 2 variables son para definir el usuario y password que utilizará OMLApp para conectarse a la API de Asterisk AMI.

* **oml_callrec_device**: Aquí debemos indicar a la aplicación, la ubicación de las grabaciones de las llamadas.

    Al estar en un escenario donde Asterisk (ACD) corre sobre un Linux host exclusivo, mientras que el componente OMLApp se encuentra en otro host,
    estamos **OBLIGADOS** a que las grabaciones sean almacenadas en un dispositivo de red que pueda ser accedido por OMLApp.

    Por lo tanto, tenemos 2 posibilidades: S3 object storage o NFS.

    Aquí podemos usar el concepto de **bucket** en object storage DB o NFS. Las opciones *s3-do* o *s3-aws*, son para trabajar con
    bucket en la nube de DigitalOcean o AWS, respectivamente. Si bien el integrador que despliegue un cluster de OMniLeads
    podría tranquilamente utilizar cualquier otro object storage que permita montar el bucket en Linux utilizando fuse-s3 (https://github.com/s3fs-fuse/s3fs-fuse),
    debería proceder ejecutando una instalación con la opción *local* y luego ajustar manualmente la configuración necesaria
    para conseguir que las grabaciones se almacenen en dicho object storage.

    Con respecto al uso de NFS para guardar las grabaciones, podemos utilizar cualquier herramienta que proporcione
    almacenamiento de red NFS.

    **Posibles valores**: *s3-do, s3-aws, nfs*.

* **nfs_host**: Ésta variable es necesaria asignar sólamente cuando se utiliza NFS como recurso de almacenamiento de grabaciones.

* **s3_access_key, s3_secret_key, s3url y s3_bucket_name**: Éstas variables sólamente son necesarias cuando se utiliza el object storage DB de DigitalOcean o AWS (a excepción de *s3url* que es sólo para DigitalOcean). Allí se deben indicar las claves para autenticarse en *spaces*, junto al URL y nombre del bucket que va a contener las grabaciones.

Con respecto a las variables implicadas para un despliegue en Alta Disponibilidad (HA), las mismas vienen
comentadas por defecto. En caso de desplegar en HA, se deberán descomentar y completar:

* **oml_deploy_ha**: En caso de desplegar un cluster de Asterisk, se debe indicar **true**.

* **oml_ha_rol**: Se indica si el nodo Asterisk a desplegar va a ser **main** o **backup**.

* **oml_ha_vip**: Se indica la dirección de red de la VIP sobre la cual se levantará el servicio.

* **oml_ha_vip_nic**: Se indica la interfaz de red sobre la cual se levantá la VIP.

* **oml_ha_tenant**: Se indica el nombre del tenant.

* **oml_ha_email**: Se introduce un e-mail para notificación de failovers.

Ejecución de la instalación
***************************
.. note::

  Al instalar Asterisk como componente aislado de OMLApp, debemos asegurarnos de que vamos a utilizar algún tipo de almacenamiento
  de red para las grabaciones, ya que de lo contrario, será inviable el acceso a las grabaciones desde la vista de búsqueda.

  Debemos trabajar con S3 object storage o NFS.

.. image:: images/install_asterisk_callrec.png
*Grabaciones almacenadas en recurso desde Asterisk y accedido desde OMLApp*

Finalmente, podemos lanzar nuestro script *first-boot-instaler.sh*. Ésto lo podemos hacer como user_data de una instancia cloud, a través de alguna utilidad
de línea de comandos para administrar la infraestructura, o bien directamente copiando el archivo hacia el Linux host destino y lanzando la ejecución del mismo.

Una vez ejecutado el script y finalizado el deploy, debemos comprobar que el componente se encuentra operativo, lanzando el siguiente comando:

.. code-block:: bash

  systemctl status asterisk

.. image:: images/install_asterisk_status.png
