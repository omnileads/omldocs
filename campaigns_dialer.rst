.. _about_dialercamp:

Presentación
************
OMniLeads pone a disposición el concepto de *campañas con discado automático de llamadas*, a través de un discador predictivo.

.. important::
  No se provee la funcionalidad de discador automático dentro de las prestaciones del software. Para éste tipo de campañas, está contemplada la integración con
  `WombatDialer <https://www.wombatdialer.com/>`_. La utilización de éste software, está supeditada a la adquisición de la correspondiente licencia con el
  fabricante de dicho software.

Aclarado el tema del componente *engine dialer*, se procede con la explicación de los pasos necesarios a la hora de generar una campaña con discado predictivo.

Creación de campaña dialer
**************************
Ingresar al menú  *Campañas -> Campañas dialer -> Nueva campaña*, en donde se despliegan una secuencia de etapas de configuración.

La primera pantalla luce como la siguiente figura:

.. image:: images/campaigns_dialer_wizard1.png

*Figura 1: Parámetros de campaña*

- **Nombre:** Nombre de la campaña.
- **Mostrar nombre al recibir llamado:** Se puede habilitar para que cada llamada conectada por el dialer al agente, implique la notificación del nombre de la campaña asociada a dicha llamada.
- **Base de datos de contactos:** La base de contactos que el discador utilizará para tomar contactos y generar llamadas.
- **Fecha inicio:** Es la fecha en la que, estando activa la campaña y con agentes conectados, comenzará a discar.
- **Fecha fin:** Es la fecha en la que, estando activa la campaña y con agentes conectados, dejará de discar por más que queden números pendientes de marcar.
- **Sistema externo:** Sistema de gestión externo que se admite para lanzar acciones de "click to call" o "calificación" sobre contactos de la campaña.
- **ID en sistema externo:** Éste campo debe contener el ID que posee la campaña dentro del sistema de gestión externo, desde el cual llegarán los "click to call" o "solicitudes de calificación".
- **Tipo de interacción:** Aquí se selecciona si la campaña va a utilizar un *formulario de campaña* o va a disparar una *solicitud-http* hacia un *sistema CRM externo*.
- **URL externo:** URL (http-request) a disparar cada vez que el discador conecte una llamada hacia un agente.
- **Ruta saliente::** Se le asigna una ruta saliente existente a una campaña.
- **CID ruta saliente:** Éste campo, debe contener el CID asignado para una ruta saliente existente a una campaña.
- **Objetivo:** Se define como la cantidad de gestiones positivas que se esperan para la campaña. En la supervisión de la campaña, se muestra en tiempo real el porcentaje de avance de la campaña respecto al objetivo definido.
- **Speech:** Speech de campaña para ser desplegado en la consola de agente en las llamadas de la campaña.

Una vez completados éstos campos, se debe dar click al botón "Paso siguiente", para continuar con la configuración de nuestra campaña.

En la siguiente pantalla se establecen más parámetros que modelan el comportamiento de la campaña, como lo expone la siguiente figura:

.. image:: images/campaigns_dialer_wizard2.png

*Figura 2: Parámetros de campaña*

- **Cantidad máxima de llamadas:** Cantidad de llamadas que se permiten poner en cola de espera mientras se aguarda por la disponibilidad de un agente. Por encima de ese número, la llamada será enviada a la acción de "failover".
- **Tiempo de descanso entre llamadas:** Es el tiempo de descanso (en segundos) que cada agente dispone entre cada llamada conectada por el discador.
- **Nivel de servicio:** Es un parámetro para medir cuántas de las llamadas fueron conectadas a un agente dentro de esa franja de tiempo (en segundos).
- **Estrategia de distribución:** Método de distribución de llamadas que usará la campaña sobre los agentes. Para campañas salientes, se recomienda RRMemory.
- **Importancia de campaña:** Es un parámetro lineal en la escala del 1 al 10, que implica cuán importante son las llamadas de esta campaña respecto a otras. Establece prioridades para los agentes que trabajan en varias campañas en simultáneo. Si se deja el valor en "0" (por defecto) se mantiene una equidad con el resto de las campañas.
- **Tiempo de espera en cola:** Es el tiempo (en segundos), que la llamada contactada quedará en cola de espera, aguardando que un agente se libere para conectarlo con la misma.
- **Grabar llamados:** Habilita que todas las llamadas de la campaña sean grabadas.
- **Detectar contestadores:** Habilita la detección de contestadores automáticos.
- **Audio para contestadores:** Se puede indicar la reproducción de un audio en caso que se detecte un contestador automático. Para que esté disponible el audio, debe subirse previamente desde el menú *Telefonía -> Audios -> Audios personalizados*, botón "Agregar".
- **Activar predictividad:** El discador ofrece una configuración que posibilita revisar estadísticas de efectividad de la campaña durante el desempeño de la misma. En función de esos resultados, ir variando la cantidad de llamadas generadas por agente disponible de manera tal que se eviten los tiempos muertos entre cada llamadas asignada por el discador a cada agente.
- **Factor de boost inicial:** Indica el valor por el cual se desea multiplicar el comportamiento de la predictividad. Por ejemplo, si el discador detectó que puede realizar 3 llamadas en simultáneo porque es el resultado que le arroja la estadística de comunicaciones exitosas, colocando "2" en el factor de boost inicial, se le pide al discador que duplique ese valor y realizará entonces 6 llamadas a la vez.
- **Dial timeout:** Es el tiempo (en segundos) de timbrado antes de mandar un CANCEL.
- **Anuncio de entrada para el agente:** Se puede seleccionar un audio para reproducir en el momento que ingresa una llamada de la campaña a un agente. Para que esté disponible el audio, debe subirse previamente desde el menú *Telefonía -> Audios -> Audios personalizados*, botón "Agregar".
- **Música de espera:** Se puede seleccionar una música de espera especialmente para la campaña, la cual será reproducida hasta que haya un agente disponible para atender el llamado, o bien hasta que se cumpla el tiempo de espera en cola y la llamada resulte dirigida al destino en caso de timeout. Para que la música de espera esté disponible, debe ser agregada previamente en el menú *Telefonía -> Audios -> Listas de música de espera*, botón "Agregar".
- **Destino en caso de timeout:** Destino hacia el cual se enviarán las llamadas que hayan sido expiradas sin conectar con un agente (el tiempo de espera agotado).

Luego de completar todos los campos, se debe presionar el botón "Paso siguiente".

En la siguiente pantalla, se configuran las opciones de calificación, seleccionando del desplegable una a una las calificaciones que se utilizarán para las llamadas dentro de la campaña.
Además, se deberá indicar si se trata de una calificación de "Gestión" (la que dispara el formulario de campaña) o bien de una calificación "Sin acción" que simplemente clasifica el contacto:

.. image:: images/campaigns_dialer_wizard3.png

*Figura 3: Parámetros de campaña - Calificaciones*

Se guardan los cambios haciendo click en "Paso siguiente" y llegamos a la configuración que determina cuáles días de la semana y dentro de cuáles horarios la campaña efectuará llamados (siempre dentro del rango de fechas establecidas en el primer paso de la creación de la campaña):

.. image:: images/campaigns_dialer_wizard4.png

*Figura 4: Parámetros de campaña - Días y horas*

Se hace click en "Paso siguiente", y en éste caso se trabaja con las "reglas de incidencia", es decir bajo cuáles condiciones se reintentará contactar a números que dieron Ocupado, Contestador, No Contesta, Temporalmente fuera de cobertura, etc.:

.. image:: images/campaigns_dialer_wizard5.png

*Figura 5: Parámetros de campaña - Reglas de incidencia*

Como se puede observar en la figura anterior, los campos a completar permiten determinar cada cuántos segundos debe reintentarse la comunicación y cuántas veces como máximo se debe intentar según cada estado.

Los estados telefónicos que podrán reintentarse marcar automáticamente son:

- Ocupado.
- Contestador automático detectado.
- Destino no contesta.
- Llamada rechazada (Rechazado): Cuando la llamada no pudo ser cursada por problemas inherentes a la red telefónica externa.
- Timeout: Cuando la llamada se contactó, se conectó, pero ningún agente estuvo libre como para gestionar la misma.

Se hace click en "Paso siguiente" para asignar los supervisores de la campaña:

.. image:: images/campaigns_dialer_wizard6.png

*Figura 6: Parámetros de campaña - Supervisores*

Se hace click en "Paso siguiente" para asignar los agentes de la campaña:

.. image:: images/campaigns_dialer_wizard7.png

*Figura 7: Parámetros de campaña - Agentes*

Se hace click en "Paso siguiente" para llegar al último paso de la creación de la campaña:

.. image:: images/campaigns_dialer_wizard8.png

*Figura 8: Parámetros de campaña - Sincronizar*

En éste paso, simplemente se indican 3 opciones:

- **Evitar duplicados:** Seleccionar esta opción para evitar subir al discador registros con el teléfono principal duplicado.
- **Evitar sin teléfono:** Seleccionar esta opción para evitar subir al discador registros de la base de contacto que no posean un teléfono principal.
- **Prefijo discador:** Éste campo sirve para indicar al discador si debe anteponer algún prefijo delante de cada número de la base de contactos a la hora de discar cada llamada de la campaña.

Por último, hacemos click en el botón "Finalizar" para concretar la creación de nuestra campaña.

Activación de campaña
*********************
La campaña recientemente creada, figura en el estado de *Inactiva*, dentro del listado de campañas predictivas, tal como se muestra en la siguiente
figura:

.. image:: images/campaigns_dialer_inactive.png

*Figura 9: Campaña inactiva*

El administrador debe activar la campaña manualmente:

.. image:: images/campaigns_dialer_activate.png

*Figura 10: Activar campaña*

Luego de activar nuestra campaña, automáticamente pasamos a visualizarla en la sección de campañas activas, tal como se muestra en la siguiente
figura:

.. image:: images/campaigns_dialer_ready.png

*Figura 11: Campañas activas*

En el momento en que un agente asignado a nuestra campaña predictiva ingrese a la plataforma, mientras sea dentro del rango de fecha y horario activo de la campaña, entonces
el discador puede comenzar a generar llamadas y entregar éstas hacia los agentes activos en la campaña.

Finalización de campañas dialer
*******************************
Para determinar cuando una campaña predictiva está sin registros por marcar, se debe consultar el estado de la misma haciendo click en el nombre de la campaña,
tal como se muestra en la siguiente figura:

.. image:: images/campaigns_dialer_finish.png

*Figura 12: Llamadas pendientes de campaña*

Cuando el valor "Llamadas pendientes" esté en 0, se debe presionar el botón "Finalizar campañas sin contactos por discar" para finalizar todas las campañas sin contactos por discar,
tal como se muestra en la siguiente figura:

.. image:: images/campaigns_dialer_finish2.png

*Figura 13: Finalizar campañas sin contactos por discar*

O bien se pueden finalizar individualmente:

.. image:: images/campaigns_dialer_finish3.png

*Figura 14: Finalizar individualmente la campaña*

La campaña pasa entonces al listado de *Campañas finalizadas*.

Reciclado y rotación de bases de contactos
******************************************
Cada vez que una campaña predictiva se queda sin registros por marcar en su base de contactos, entonces nuestra campaña se puede re-utilizar mediante
2 posibilidades:

**1) Reciclar la base de contactos**

Ésta opción permite al administrador seleccionar contactos de la base con ciertas calificaciones efectuadas por agentes (sobre llamadas conectadas)
así como también calificaciones efectuadas por el discador (sobre llamadas no contactadas -ocupado, no contesta, voicemail, etc.-), a la hora de formar
un criterio para reciclar la base de contactos de la campaña actual, para que el discador vuelva a llamar a los contactos que caigan dentro de las
calificaciones indicadas en el reciclado.

Para reciclar una campaña finalizada, debemos desplegar el menú "Opciones" de la campaña, y seleccionar "Reciclar", tal como se muestra en la siguiente
figura:

.. image:: images/campaigns_dialer_recycle1.png

*Figura 15: Reciclado de campaña*

Allí se mostrarán las distintas calificaciones y podremos escoger entre 2 modos de reciclado:

 - *Reciclar sobre la misma campaña:* Volver a marcar a los contactos seleccionados desde la misma campaña.
 - *Reciclar sobre una nueva campaña clon:* Volver a marcar a los contactos seleccionados, pero sobre una nueva campaña similar a la original, y cuya base de contactos serán la resultante del reciclado.

Para completar el procedimiento, se deben seleccionar *las calificaciones* que se deasean volver a llamar, y luego pasar por las etapas de configuración de campañas
predictivas en caso de necesitar ajustar cualquier parámetro de configuración de la campaña reciclada:

.. image:: images/campaigns_dialer_recycle2.png

*Figura 16: Reciclado, calificaciones de llamada*

.. image:: images/campaigns_dialer_recycle3.png

*Figura 17: Parametrización de campaña reciclada*

Al ejecutar el reciclado, la campaña se encuentra en estado de "Inactiva", por lo tanto resta activar la misma para que los contactos reciclados comiencen a ser marcados
por el discador:

.. image:: images/campaigns_dialer_recycle4.png

*Figura 18: Activación de campaña reciclada*

**2) Reemplazar la base de contactos**

Una campaña puede sustituir su base por otra nueva. Ésto permite seguir operando con la misma campaña pero renovar la fuente de contactos a llamar.
De ésta manera, se sigue el historial de reportes, grabaciones y demás estadísticas en una misma campaña.

Para llevar a cabo un cambio de base, la campaña debe estar pausada o bajo el estado de "finalizada".
A partir de allí se indica la acción de "cambio de base" sobre la campaña en cuestión:

.. image:: images/campaigns_dialer_changedb.png

*Figura 19: Cambio de base de datos de contactos*

Esto desplegará una pantalla similar a la expuesta en la siguiente figura:

.. image:: images/campaigns_dialer_changedb2.png

*Figura 20: Formulario de cambio de base de datos de contactos*

.. important::

  La estructura de la base de contactos que se puede utilizar como sustituta, debe ser similar a la base que se desea sustituir.

Una vez llevada a cabo la sustitución, es necesario activar nuevamente la campaña.
