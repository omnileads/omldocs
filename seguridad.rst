.. _about_seguridad:

Consideraciones sobre seguridad
*******************************
OMniLeads es una aplicación web, diseñada para operar bajo la protección de al menos un firewall perimetral o cloud firewall en un ambiente
de cloud computing.

Idealmente, se recomienda desplegar OMniLeads junto a un *HTTP Proxy o Cloud Load Balancer* de borde para las peticiones HTTPS y un
Session Border Controller para la gestión de borde VoIP. Ésto robustece ampliamente la seguridad de su desplgiegue.

Considerando el escenario en donde los usuarios van a acceder a la aplicación tanto desde la red local como también desde Internet, se deben
exponer al exterior el siguiente listado de puertos:

* **UDP 5161**: Tráfico SIP proveniente de la PSTN. Se debe validar por IP de origen. Éste puerto es el indicado si la instancia que aloja OML posee una dirección IPV4 pública a nivel tarjeta de red.
* **UDP 5162**: Tráfico SIP proveniente de la PSTN. Se debe validar por IP de origen. Éste puerto es el indicado si la instancia que aloja OML posee una dirección IPV4 pública detrás de NAT.
* **UDP 40000 a 50000**: Tráfico RTP proveniente de la PSTN. Se debe validar por IP de origen. Éstos puertos son los indicados si la instancia que aloja OML posee una dirección IPV4 pública detrás de NAT.
* **UDP 20000 a 30000**: Tráfico WebRTC proveniente de los usuarios. En éste caso, los usuarios se suponen en modalidad home-office, por lo que se deja abierto a Internet.
* **HTTPS 443**: Tráfico web y WebRTC proveniente de los usuarios. En éste caso, los usuarios se suponen en modalidad home-office, por lo que se deja abierto a Internet.

Despliegue del tipo All in One:

.. image:: images/install_oml_nat.png

Despliegue en cluster sobre un esquema de cloud-computing:

.. image:: images/cluster_A.png

.. important::

    En caso de necesitar exponer los puertos VoIP a TODO Internet, se recomienda administrar la seguridad VoIP utilizando un Session Border Controller o un *Asterisk ó Freeswitch*
    configurado como componente SIP de borde, de manera tal que OMniLeads no quede expuesto a todas las direcciones IPs. Cómo mínimo, comenzará a recibir *basura* SIP proveniente de
    múltiples orígenes.
