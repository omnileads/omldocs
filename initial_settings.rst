.. _about_initial_settings:

*********************
Configuración inicial
*********************
Luego de atravesar el proceso de :ref:`about_install`, asumimos que la aplicación se encuentra instalada y disponible en la 
correspondiente URL, por lo que en ésta sección se plantean los pasos esenciales de configuración post-instalación.

.. _about_roles:

Roles y permisos
****************
A éste módulo, se accede a partir del menú *Usuarios y grupos -> Roles y permisos*.

En ésta sección del módulo citado, se puede listar los roles predefinidos del sistema, verificar los permisos específicos de 
cada rol, y finalmente crear nuevos roles personalizados con ciertos privilegios o limitaciones según las necesidades.

.. image:: images/users_and_roles_01.png

A continuación, se listan los roles predefinidos:

* **Cliente Webphone**: Éste rol, si bien viene creado por defecto, sólamente se utiliza cuando se adquiere e instala el addon `Click2Call <https://www.omnileads.net/click2call>`_.
* **Agente**: Los usuarios generados y asignados a éste rol, podrán acceder a la consola de agente, con todos los permisos que ello implica (acceso al webphone para disparar llamadas, posibilidad de ejecutar pausas, acceso a agenda y grabaciones del agente, entre otros).
* **Referente**: Los usuarios generados y asignados a éste rol, podrán sólo listar todas las campañas, consultar reportes generales de agentes y llamadas, acceder a la supervisión de agentes y campañas.
* **Supervisor**: Los usuarios generados y asignados a éste rol, podrán trabajar con todas las campañas a las que estén asignados (crear, modificar, eliminar y listar), acceder a la supervisión y reportes generales de llamadas y agentes. También, podrán crear usuarios y grupos de agentes, pero sólo podrán editar o eliminar agentes que estén asignados a alguna campaña a la que ellos mismos estén asignados. Podrán buscar grabaciones de sus campañas, listar y subir bases de contactos y acceder al módulo de telefonía, donde podrán trabajar con algunas secciones.
* **Gerente**: Los usuarios generados y asignados a éste rol, podrán realizar todas las acciones de un supervisor, y además podrán visualizar el módulo de Auditorías.
* **Administrador**: Los usuarios generados y asignados a éste rol, tienen acceso completo a todo el sistema. Sólamente los administradores pueden editar o borrar usuarios con perfil Administrador.

Para generar un *rol personalizado*, se debe acceder al menú *Usuarios y grupos -> Roles y permisos -> Crear rol*.
Al nuevo rol se le asigna un nombre y luego se marcan los permisos que tendrá. Para no iniciar desde cero, el usuario puede 
partir de una base de permisos de cierto perfil, y luego seguir personalizando (añadiendo o limitando permisos) hasta dejar 
su rol listo.

A continuación, se ejemplifica la creación de un nuevo rol:

* Creación de un nuevo rol: Se crea un nuevo rol y se asigna un nombre al mismo.

.. image:: images/users_and_roles_02.png

Desde el combo de opciones del nuevo rol creado, se puede *imitar permisos de otro rol* ya existente, o bien *aplicar 
permisos de otro rol* ya existente. La diferencia radica en que la primer opción pone los mismos permisos del rol existente, 
mientras que la segunda opción suma los permisos del rol existente, a los permisos que ya han sido seleccionados en el nuevo rol.

.. image:: images/users_and_roles_03.png

* Guardado del nuevo rol: Finalmente, se guarda el nuevo rol creado.

.. image:: images/users_and_roles_04.png

.. _about_users:

Usuarios
********
Vamos a diferenciar entre usuarios del tipo "Agente" y usuarios del tipo "Administrativos". Los usuarios *Agente* son quienes 
gestionan las comunicaciones, y los usuarios *Administrativos* gestionan la aplicación.

.. important::
  Antes de crear un usuario *Agente*, debe existir al menos un *Grupo de agentes*.

Para crear un usuario, se debe acceder al menú *Usuarios y grupos -> Lista de usuarios -> Nuevo usuario*.

  .. image:: images/users_and_roles_05.png

Allí, se despliega un formulario a completar con los datos del nuevo usuario. Dentro de dicho forumlario, desde la opción 
*Rol del usuario*, se determinará si el nuevo usuario será del tipo "Agente" o del tipo "Administrativo", de acuerdo al rol que 
se asocie al usuario creado.

Grupos de agentes
*****************
En ésta sección, se administran los grupos de agentes, grupos que serán invocados en diferentes módulos del sistema, como puede 
ser a la hora de asignar usuarios a campañas, o en la extracción de reportes, así como también en el módulo de supervisión.

**Creación de grupo de agentes**

Para crear un grupo de agentes, se debe acceder al menú *Usuarios y grupos -> Nuevo grupo de agentes*.

.. image:: images/initial_settings_04.png

Los campos allí desplegados, son los siguientes:

- **Nombre:** Es el nombre que se desea asignar al grupo de agentes.
- **Despausar automáticamente:** Para comprender éste parámetro, debemos explicar que en OMniLeads, luego de cada llamada (de cualquier naturaleza) procesada
  por un agente, el mismo es forzado a ingresar en una pausa ACW (After Call Work), en la cual permanece inactivo para las campañas asignadas,
  de manera tal que pueda completar la calificación de la llamada actual y terminar su gestión. Ahora bien, para salir de dicha pausa inducida por
  el sistema, existen 2 posibilidades, y allí es donde entra nuestro parámetro, ya que por un lado si dejamos el valor en «0» el agente debe salir
  explícitamente de la pausa para seguir operando, pero si colocamos un número (por ejemplo, 5 segundos), ésto implica que el agente asignado a éste grupo,
  luego de caer en una pausa inducida ACW, el sistema lo vuelva a dejar online a los X segundos (según lo indicado en "Despausar automáticamente").
  Éste parámetro se puede manejar desde el grupo de agentes, o bien desde una configuración de la campaña, que se mencionará más adelante.
  **Conjunto de pausa:** Aquí se presenta la posibilidad de asignar un grupo de pausas (previamente creado) al grupo de agentes, para que así 
  los agentes que formen parte de dicho grupo, no visualicen todas las pausas, sino únicamente aquellas que forman parte del grupo 
  de pausas asignado.
- **Auto atender entrantes:** Si éste valor está checkeado, entonces las llamadas provenientes de campañas entrantes serán conectadas al agente sin brindar la posibilidad de notificación (ring) y opción de atender por parte del agente.
- **Auto atender dialer:** Si éste valor está checkeado, entonces las llamadas provenientes de campañas con discador predictivo serán conectadas al agente sin brindar la posibilidad de notificación (ring) y opción de atender por parte del agente.
- **Forzar calificación:** Si éste valor está checkeado, entonces todas las llamadas que realice o reciba el agente, deberán ser calificadas antes de volver a estar operativo el agente. Al seleccionar ésta opción, se deshabilita la posibilidad de despausar automáticamente al agente, ya que pasa a ser obligatoria la calificación.
- **Forzar despausa:** Si la opción de "Forzar calificación" está habilitada, entonces se ofrece ésta segunda opción para forzar la despausa, una vez calificada la llamada. Si éste valor no está checkeado, el agente tendrá que retomar operación de manera explícita luego de calificar la llamada.
- **Llamada fuera de campaña:** Si éste valor está checkeado, permitirá al agente realizar llamadas que no encuadren dentro de ninguna campaña, teniendo en cuenta que las mismas, no dejarán registro de reportería, grabaciones, etc.
- **Permitir el acceso a las grabaciones:** Si éste valor está checkeado, el agente tendrá acceso a la búsqueda, descarga y escucha de sus propias grabaciones.
- **Permitir el acceso al dashboard:** Si éste valor está checkeado, el agente tendrá acceso al dashboard sobre su gestión.
- **Permitir la activación de On-Hold:** Si éste valor está checkeado, se permitirá al agente poner una llamada en espera.
- **Ver temporizadores en consola:** Si éste valor está checkeado, el agente podrá ver los tiempos de operación y pausa que figuran en la esquina superior izquierda de la consola de agente.
- **Limitar agendas personales:** Si éste valor está checkeado, se puede introducir la cantidad máxima de agendas personales que puede manejar un agente.
- **Limitar agendas personales en días:** Si éste valor está checkeado, se puede introducir la cantidad máxima de días que se permitirá al agente usar, a la hora de agendar una llamada. Es decir, el agente podrá agendar llamadas dentro de la ventana de N días configurado a partir de éste parámetro.
- **Acceso a los contactos como agente:** Si éste valor está checkeado, el agente tendrá acceso a la lista de contactos, desde la consola de agente.
- **Acceso a las agendas como agente:** Si éste valor está checkeado, el agente tendrá acceso a la agenda, desde la consola de agente.
- **Acceso a las calificaciones como agente:** Si éste valor está checkeado, el agente tendrá acceso al listado de calificaciones gestionadas, desde la consola de agente.
- **Acceso a las campañas preview como agente:** Si éste valor está checkeado, el agente tendrá acceso al listado de campañas preview a las cuales se encuentra asignado, desde la consola de agente.

Adicionar paquetes de audios en otros idiomas
*********************************************
Los audios genéricos que los agentes o teléfonos externos escucharán vienen por defecto en inglés, siendo configurables en las rutas entrantes
o rutas salientes, de manera tal que si el canal telefónico se encuentra con alguna indicación a través de un audio genérico dentro del flujo de
una llamada, éste podrá ser reproducido de acuerdo al idioma indicado.

Si la instancia precisa utilizar otros idiomas, se pueden instalar los mismos a través de la sección *Telefonía -> Audios -> Paquetes de Audio de Asterisk*,
en donde se podrán adicionar nuevos idiomas.

.. image:: images/telephony_i18n_audio.png

Al seleccionar el idioma deseado y presionar la opción "Guardar", se descargará e instalará dicho paquete de idioma.

Música de espera
****************
Dentro del módulo *Telefonía -> Audios -> Listas de Música de espera*, se permite gestionar *listas de reproducción* con archivos en el formato *wav 16 bits*.
Las listas aquí generadas, podrán ser utilizadas en las *campañas entrantes* a la hora de poner a los llamantes en cola de espera.
Cabe destacar, que no podrán eliminarse playlists que estén en uso por alguna campaña o que tengan archivos asignados.

.. image:: images/telephony_playlist_create.png

Una vez creada una nueva lista, deberán agregarse las músicas deseadas, a través de archivos en formato **.wav** a cargarse desde su computadora.
Sólo estarán disponibles para su uso en campañas entrantes las playlists que tengan al menos una música cargada.

.. image:: images/telephony_playlist_edit.png

Pausas
******
Los agentes pueden entrar en una pausa cada vez que deseen quedar desafectados para atender nuevas comunicaciones, de manera 
tal que se evita que una campaña entrante o con discador predictivo, le entregue una nueva llamada. Además, los estados de 
pausa son útiles para registrar productividad y medir los tiempos de sesión del agente.

Las pausas las pueden generar los usuarios con dicho permiso, y pueden ser del tipo "Recreativas" o "Productivas".

**Creación de pausas**

Para generar una nueva pausa, se debe acceder al menú *Pausas -> Listado de pausas -> Nuevo*:

.. image:: images/initial_settings_08.png

Desde el formulario que se despliega, se asigna un nombre, se selecciona el tipo de pausa, y se guardan los cambios.

.. image:: images/initial_settings_09.png

A la hora de presentar los reportes de sesión de agente, las pausas totalizadas se dividen en pausas recreativas y pausas 
productivas, lo cual permite medir la productividad de nuestros agentes de una manera más exacta.

**Creación de conjuntos de pausas**

Los grupos de pausas permiten, como su nombre lo indica, agrupar determinadas pausas creadas previamente, para ser asignadas 
a grupos de agentes. De ésta manera, los agentes que formen parte de un determinado grupo de agentes, con un determinado 
grupo de pausas, podrán utilizar sólamente las pausas de dicho grupo desde su consola de agente, y no tendrán acceso a todas 
las pausas que se hayan dado de alta en el sistema.

Para generar un conjunto de pausas, se debe acceder al menú *Pausas -> Conjuntos de pausas -> Nuevo*:

.. image:: images/initial_settings_17.png

Desde el formulario que se despliega, se asigna un nombre, se agregan pausas creadas previamente al nuevo grupo, y se guardan 
los cambios.

.. image:: images/initial_settings_18.png

Primer login de agente
**********************
.. important::

 Tener en cuenta que para obtener un login exitoso, debemos contar con un **MICRÓFONO disponible** en la estación de trabajo
 desde la cual se ha realizado el login de agente. Si ésto no se cumple, entonces el login será defectuoso.

Una vez que accedemos con nuestro agente, si todo va bien, se desplegará un popup que solicita el permiso para tomar control
del micrófono:

.. image:: images/initial_settings_10.png

Al habilitar el permiso, debemos escuchar un audio que el sistema reproduce, indicando el login exitoso:

.. image:: images/initial_settings_11.png

.. _about_omnileads_register:

Registro de la instancia
************************
Éste paso no es obligatorio, ya que el sistema puede funcionar perfectamente sin realizar un registro. Sin embargo, SÍ es
necesario tener la instancia registrada a la hora de adquirir un addon o suscribir la plataforma al soporte de fabricante.

Finalmente, para aquellos integradores certificados (que han aprobado el programa de certificación oficial de OMniLeads),
a partir de registrar la instancia, podrán firmar la instalación con el código de certificación, logrando así dejar una
constancia de que la plataforma ha sido desplegada y configurada por un *administrador IT* certificado por el fabricante.

 .. image:: images/initial_settings_13.png

Se deben completar los campos allí solicitados, y luego se recibirá un e-mail con el código de la instancia:

.. image:: images/initial_settings_15.png

Luego, cada vez que ingresemos a la sección de registro a partir del menú *Ayuda -> Registrarse*, se obtendrá una salida que
informa el hecho de haber registrado ya la instancia:

.. image:: images/initial_settings_14.png

El registro de la instancia pide como valores obligatorios el nombre del usuario o empresa, la dirección de e-mail y la
contraseña, siendo opcional el campo de teléfono.

Una vez que se ha registrado la instancia satisfactoriamente, se le enviará un e-mail a la dirección ingresada con el valor de
la clave asignada a la instancia.
En caso de que quiera que se le reenvíe el e-mail con la llave de la instancia una vez registrado, puede usar el botón
"Reenviar llave".

Es importante tener en cuenta que si quiere registrar varias instancias con una misma dirección de e-mail, debe ingresar
también la misma contraseña.
En otro caso, use una dirección de e-mail diferente.

Addons comerciales disponibles
******************************
El sistema brinda información sobre los addons comerciales disponibles a través del menú *Ayuda -> Market place*:

.. image:: images/initial_settings_16.png

Desde aquí, haciendo click en los nombres de los addons, se puede acceder a sus sitios y adquirirlos para instalarlos en su
instancia actual.
