.. _about_install:

************************
Instalación de OMniLeads
************************
Antes de avanzar con la instalación, se recomienda leer y entender la sección :ref:`about_oml_arq`.


Deploy de OMniLeads sobre Ubuntu 22.04, Rocky Linux 8, Debian 11, Alma Linux 8
******************************************************************************

A partir de la version 1.26.0 OMniLeads dispone de la posibilidad de desplegar sobre contenedores. Esto permite que la App pueda ser desplegada 
en producción sobre cualquier Linux que implemente Docker o Podman.

Además este tipo de deploy implementa actualizaciones de los componentes de OMniLeads, respecto al entregable similar sobre CentOS 7. 

- Django / Python
- PostgreSQL
- Redis
- RTPengine
- Asterisk
- Kamailio

En ésta sección se cubre el despliegue de OMniLeads utilizando contenedores: :ref:`about_install_containers`.


Deploy de OMniLeads sobre CentOS-7
**********************************

.. note:: 
  
  Las versiones 1.X de OMniLeads se distribuyen para CentOS-7 hasta el 31 de setiembre de 2023. 

Esta opción permite levantar una instancia de OMniLeads sobre una única instancia de CentOS-7. 
.. image:: images/install_arq_aio.png

.. note::

  La versión de Centos oficialmente soportada es: `CentOS7-Minimal <http://isoredirect.centos.org/centos/7/isos/x86_64/>`

En ésta sección se cubre el despliegue de OMniLeads CentOS-7: :ref:`about_install_centos`.


Deploy de OMniLeads OnPremise Alta Disponibilidad
*************************************************
OMniLeads puede ser desplegado bajo un esquema de alta disponibilidad con redundancia en todos sus componentes.

.. image:: images/install_arq_cluster_ha.png

En ésta sección se cubre el despliegue en cluster de Alta Disponibilidad: :ref:`about_install_linux_ha`.

Deploy de OMniLeads sobre DigitalOcean
**************************************
En ésta seccion se cubre la instalación de la aplicación aprovechando las ventajas de la nube de DigitalOcean.

* :ref:`about_install_digitalocean`

Deploy de OMniLeads sobre Vultr
*******************************
En ésta seccion se cubre la instalación de la aplicación aprovechando las ventajas de la nube de DigitalOcean.

* :ref:`about_install_digitalocean`

Deploy de OMniLeads sobre Linode
********************************
En ésta seccion se cubre la instalación de la aplicación aprovechando las ventajas de la nube de DigitalOcean.

* :ref:`about_install_digitalocean`


Deploy de OMniLeads basado en Terraform
****************************************
A partir de la versión 1.16, es posible utilizar Terraform para desplegar OMniLeads sobre algunas nubes concretas que se irán incrementando con el
pasar del tiempo y el aporte de la comunidad.

Para quienes no estén familiarizados con Terraform y estén interesados en conocer, dejamos el enlace a su `documentación <https://www.terraform.io/>`_. Terraform nos permite
codificar un despliegue. En términos de Terraform, podemos definir un despliegue de OMniLeads, como el siguiente conjunto de acciones:

 * Configuración de networking de la infraestructura.
 * Creación de máquinas virtuales, clusters de DB, balanceadores de carga.
 * Deploy de OMniLeads y sus componentes sobre las máquinas virtuales.
 * Aplicación de la configuración de seguridad.
 * Generación de certificados SSL y URL de acceso para los usuarios.

 .. image:: images/install_terraform_tenant_arq.png

Por lo tanto, usted puede simplemente configurar un archivo de variables y luego lanzar un comando (terraform apply), y en cuestión de minutos
contar con un despliegue de OMniLeads "as a Service", listo para que nuestros usuarios puedan comenzar a operar desde cualquier punto geográfico.

.. note::

  El deploy de los componentes Jitsi Meet, WombatDialer y MySQL, es configurable de acuerdo a las necesidades de la operación sobre campañas predictivas
  y/o campañas de video atención.

El enfoque de desplegar en nube de infraestructura, nos permite automatizar completamente la generación de un suscriptor al servicio de Contact Center
en la nube.

.. image:: images/install_terraform_tenants_2 .png

A continuación, el listado de nubes públicas soportadas:

* :ref:`about_install_terraform_digitalocean`
* :ref:`about_install_terraform_aws`
