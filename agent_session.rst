Login
*****
Para acceder a la aplicación, se ingresa la URL de su instancia de OMniLeads en su browser.

Si la instancia de OMniLeads no dispone de certificados comerciales, al ingresar al sistema, el usuario
se topa con la advertencia de seguridad correspondiente al ingreso a un sitio *HTTPS* con un certificado auto-firmado.

En ese caso, simplemente debe aceptar la advertencia y continuar, como lo indican las siguientes figuras:

.. image:: images/about_agent_login_1.png

*Figura 1: Certificado no confiable*

.. image:: images/about_agent_login_2.png

*Figura 2: Certificado no confiable*

Al avanzar sobre el sitio, se despliega la pantalla de login, en donde el agente deberá ingresar su usuario y contraseña
para acceder a la aplicación:

.. image:: images/about_agent_login_4.png

*Figura 3: Pantalla de login*

Luego de un login exitoso, se despliega la interfaz de agente. Lo primero que debe suceder es una advertencia
del browser que notifica que OMniLeads desea acceder al micrófono del ordenador. Por supuesto, el usuario debe
permitir ésta acción, ya que precisa del micrófono para utilizar la aplicación a la hora de gestionar llamadas.
Ésto, se muestra a continuación:

.. image:: images/about_agent_login_5.png

*Figura 4: Permiso de acceso al micrófono*

Al permitir el acceso al micrófono, entonces se procede con el resto del login. Ésto implica la reproducción
de un "audio" sobre el auricular del agente anunciando el login exitoso.

Además, el agente debe visualizar el webphone desplegado y en estado "Registrado" como lo indica la siguiente figura:

.. image:: images/about_agent_login_3.png

*Figura 5: Agente registrado al sistema*

La consola de agente
********************
La consola de agente es el componente sobre el cual el agente de OMniLeads puede realizar toda la gestión.
En la siguiente figura, se puede observar su aspecto y además se diseccionó en sub-componentes que son explicados
a continuación:

.. image:: images/about_agent_console.png

*Figura 6: Consola de agente de OMniLeads*

**Dashboard de agente**

Al ingresar el agente al sitio, va a visualizar en el espacio de contenido una vista en forma de dashboard que muestra las siguientes estadísticas de agente del día actual:

- La lista de las últimas 10 llamadas recibidas/realizadas.
- El número de llamadas conectadas y un gráfico de pastel, mostrando la relación entre las llamadas entrantes y salientes.
- El tiempo de pausa recreativa que ha tenido el agente y un gráfico de pastel que muestra la relación entre el tiempo de sesión total del agente y el tiempo total de pausa.
- El número de las calificaciones de gestión que ha realizado el agente y un gráfico de pastel que muestra la relación entre las calificaciones de gestión realizadas y, de ellas, las marcadas como observadas por una auditoría.

Éste dashboard se actualiza periódicamente y permite al agente tener información personalizada de su desempeño en el día actual.

Por último, es importante destacar que la lista de llamadas que se visualizan en el dashboard muestra información de su calificación en caso de que se haya calificado la llamada e incluso del valor de la gestión, además de que se permite modificar dichos valores e incluso realizar una llamada al contacto a través del número marcado que se muestra en el log.

.. image:: images/agent_dashboard.png

*Figura 7: Dashboard*

**1) Gestión de contactos**

En ésta sección, encontramos las funcionalidades que permiten navegar entre los contactos de la base de las campañas,
visualizar los contactos agendados para ser llamados en el futuro, listar el histórico de todas las calificaciones
que fue realizando el agente, y además acceder a las campañas del tipo preview para solicitar contactos para gestionar.

Todos éstos aspectos, serán ampliados en las próximas secciones.

**2) Barra de estado**

En esta barra tenemos información sobre la sesión.

.. image:: images/about_agent_console_statusbar.png

*Figura 8: Barra de estado*

En la barra de estado hay 2 cronómetros que van contabilizando la suma del tiempo de agente en pausa y en estado
de Ready. Su color cambia de acuerdo a si el agente está en estado "Ready" (gris), "En llamada" (verde) o "En pausa" (amarillo),
y además el estado también aparece como leyenda. Finalmente, aparecen los botones de pausa y des-pausa, para que el agente
pueda irse a pausa y salir de la misma.

**3) Área de información dinámica**

En ésta sección de la pantalla, aparece toda la información que el agente va solicitando mientras navega en la gestión
de contactos, así como también los datos de los contactos en el momento en que el agente procesa una nueva llamada.

**4) Webphone**

El webphone de OMniLeads es el componente principal dentro del módulo de telefonía. A continuación,
vamos a repasar los botones del mismo:

.. image:: images/about_agent_console_webphone.png

*Figura 9: Webphone*

Vamos a dividir al webphone en 6 partes:

- (1) Estado del webphone. Debe siempre figurar como *Registered*. En caso contrario, debe comunicarse con el administrador.
- (2) Dialpad para marcar llamados manuales o enviar DTMFs sobre una llamada conectada.
- (3) Éstos botones permiten disparar acciones de llamar, cortar, re-llamar al último número por un lado, siendo el cuarto botón un método para "marcar" la grabación de la llamada con un comentario.
- (4) Ésta serie de botones permiten al agente efectuar acciones de realizar una transferencia de llamada, efectuar una conferencia con una tercera parte y poner la llamada en espera.
- (5) Éste botón permite modificar la campaña sobre la cual se procesa cada llamada manual. También, se abordará más adelante en la sección de "campañas manuales".
- (6) Éste botón permite efectuar una llamada hacia otro agente, así como también lanzar una llamada saliente a un número externo SIN asociarse a ninguna campaña.

La mayoría de éstas funcionalidades son ampliadas en las secciones pertinentes de ésta documentación.

**5) Dock**

El dock expone cada herramienta de comunicación. En la versión actual, SÓLAMENTE está funcional el webphone, ya que
OMniLeads actualmente soporta solamente llamadas telefónicas. En versiones futuras, se comenzarán a activar otros componentes.

.. _about_agent_session_pause:

Pausas
******
El agente puede ingresar en modo pausa para que ninguna llamada de campañas entrantes o predictivas pueda ingresarle.
Como bien se explica en la sección de "Configuración inicial", existen diferentes tipos de pausas que el administrador
puede generar y mantener en el sistema. Por lo tanto, el agente al ingresar en el estado de pausa, debe indicar
a qué tipo de pausa.

Para ingresar en una pausa, se debe hacer click en el botón "Pausa" dentro de la barra de estado del agente:

.. image:: images/about_agent_login_7.png

*Figura 10: Pausa*

Se despliega entonces el menú de selección del tipo de pausa:

.. image:: images/about_agent_login_8.png

*Figura 11: Tipo de pausa*

Finalmente, el agente entra en pausa. Debe notarse cómo cambia hacia el color "amarillo" la barra de estado,
y además en la misma barra se puede leer el tipo de pausa actual. Finalmente, se puede observar que
el cronómetro de tiempo de pausa comienza a correr, mientras que el de tiempo de operación se frena:

.. image:: images/about_agent_login_9.png

*Figura 12: En pausa*

Logout
******
Para efectuar un logout, se debe acceder al vértice superior derecho, tal como lo expone la siguiente figura:

.. image:: images/about_agent_login_6.png

*Figura 13: Logout de agente*
