.. _about_install_kamailio:

******************************
Deploy del componente Kamailio
******************************
Como es sabido, **Kamailio** implementa un script de *fisrt_boot_installer.tpl*, que puede ser invocado utilizando tecnología de provisioning,
o bien ser ejecutado manualmente con una previa parametrización de las variables implicadas.

Para obtener nuestro script, se debe lanzar el siguiente comando:

.. code-block:: bash

  curl https://gitlab.com/omnileads/omlkamailio/-/raw/master/deploy/first_boot_installer.tpl?inline=false > first_boot_installer.sh && chmod +x first_boot_installer.sh

Entonces, una vez que contamos con el script, pasamos a trabajar las variables, quitando comentarios y configurando sus valores dentro del propio script.
Vamos a listar y explicar cada una de éstas variables que deben ser ajustadas antes de ejecutar el script.

Variables de instalación
************************
* **oml_infras_stage**: Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise* como valor para la variable. **Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

* **oml_nic**: Aquí debemos indicar la interfaz de red sobre la que se levantará el servicio de Kamailio.

* **oml_kamailio_release**: Aquí debemos indicar qué versión del componente se desea desplegar. Para ello, se deberá revisar el archivo `.gitmodules <https://gitlab.com/omnileads/ominicontacto/-/blob/master/.gitmodules>`_ de la versión de OMniLeads que se desee desplegar, y tomar el parámetro *branch* del componente, como valor de ésta variable.

* **oml_redis_host, oml_acd_host y oml_rtpengine_host**: Se mencionan éstas 3 variables de forma agrupada, ya que tienen que ver con el hecho de declarar la dirección de red de cada uno de éstos componentes con los cuales Kamailio necesita interactuar en su labor.

* **oml_kamailio_shm_size y oml_kamailio_pkg_size**: Se indican los valores de memoria en MB que utilizará Kamailio (shared memory y private memory, respectivamente).

Ejecución de la instalación
***************************
Finalmente, podemos lanzar nuestro script *first-boot-instaler.sh*. Ésto lo podemos hacer como user_data de una instancia cloud, a través de alguna utilidad
de línea de comandos para administrar la infraestructura, o bien directamente copiando el archivo hacia el Linux host destino y lanzando la ejecución del mismo.

Una vez ejecutado el script y finalizado el deploy, debemos comprobar que el componente se encuentra operativo, lanzando el siguiente comando:

.. code-block:: bash

  systemctl status kamailio

.. image:: images/install_kamailio_status.png
