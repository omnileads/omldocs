��          t               �   S   �      !     9  )   M  k   w     �  0   �  #   -  !   Q  N   s  }  �  &   @     g     |     �  X   �     �  ,        A  #   _  ?   �   Cualquiera de las columnas de la base de datos de contactos asociada a la campaña. El "id" de la campaña. El "id" del agente. El "path" de la grabación de la llamada. En cada llamada al CRM se pueden enviar datos de la llamada, del contacto o valores fijos como por ejemplo: Entre otros parámetros. Identificación de entidades del Sistema Externo Interacciones con Sistemas Externos Interacción con un Sitio externo Un valor fijo que identifique a que cliente corresponde la campaña en el CRM. Project-Id-Version: OMniLeads 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-07-17 14:37-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.1
 Any of the campaign's database columns The campaign's "id". The agent's "id". The call record "path". Each CRM interaction can send call information, contact information or fixedvalues like: And other parameters Entity identification in the External System External Systems Interactions Interaction with an External System A fixed value that identifies the client in the CRM's campaign. 