��          �               ,     -  #   I     m     ~     �     �  z   �  X   2  ?   �  1   �  0   �  /   .     ^     o  E   �  [   �  }  $     �  '   �     �     �            \   &  :   �  3   �  )   �  (     )   E     o     |  7   �  @   �   Acciones sobre las llamadas Agendas, calificaciones y contactos Campañas dialer Campañas entrantes Campañas manuales Campañas preview Cuando un agente se encuentra en una llamada, cuenta con diferentes funcionalidades que optimizan la gestión de llamadas. El agente cuenta con diferentes menús para el manejo de los contactos de las campañas. El agente y la operación en campañas con discador predictivo. El agente y la operación en campañas entrantes. El agente y la operación en campañas manuales. El agente y la operación en campañas preview. Manual de agente Sesión del agente Todo lo inherente a la sesión del agente describimos en este inciso. Vamos a dividir el manual de gestión del agente en los diferentes tópicos citados debajo. Project-Id-Version: OMniLeads 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-12-28 10:47-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.1
 Call Actions Agendas, Call Dispositions and Contacts Outbound Dialer Campaigns Inbound Campaigns Manual Campaigns Preview Campaigns When Agent is on call, it counts on with multiple features that improve the call management. Agent has different menues to work with campaign contacts. Agent and Operation in Predictive Dialer Campaigns. Agent and Operation in Inbound Campaigns. Agent and Operation in Manual Campaigns. Agent and Operation in Preview Campaigns. Agent Manual Agent Session All related to agent session is decribed in this piece. We will split Manual Agent in the following topics stated below. 