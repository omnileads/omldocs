��          �               �      �             $   /     T     \  
   a     l  �   �  �   1  	   �     �  2   �  }       �     �     �     �     �     �  	   �     �  �     w   �     *     3     C   Campañas entrantes Contexto Destinos de failover Ejecución de dialplan personalizado Ejemplo Etc. Extensión Modo de configuración Por ejemplo, a la hora de tener que lanzar un IVR que implique algún tratamiento avanzado de las llamadas entrantes (sistemas de autogestión o confirmación de trámites). Por lo tanto, por un lado generamos el dialplan citado en el archivo "oml_extensions_custom.conf" ubicado en "/opt/omnileads/asterisk/etc/asterisk". Prioridad Rutas salientes Vamos a implementar un dialplan como el siguiente: Project-Id-Version: OMniLeads 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-12-28 10:47-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.1
 Inbound campaigns Context Failover destination Custom asterisk dialplan Example Etc. Extension Custom destination settings For example, a developer can write a dialplan to integrate different business processes and numerous data sources to your IVR using API integrations with CRM or ERP. Then, we will write the code on *oml_extensions_custom.conf* the file is on "/opt/omnileads/asterisk/etc/asterisk" dir. Priority Outbound routes We will implement the dialplan 