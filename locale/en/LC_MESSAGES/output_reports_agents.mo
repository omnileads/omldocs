��          |               �   %   �   *        .      L     m    �  �   �  "  �  �   �     b  �   v  }    '   �  "   �     �     �     	  �      �   �  �   �	  q   t
     �
  ]   �
   *Figura 1: Filtro de fecha y agentes* *Figura 2: Resumen de actividad de agente* *Figura 3: Detalle de pausas* *Figura 4: Llamadas y campañas* *Figura 5: Tipos de llamadas* A partir de nuestro resumen, se comienza a desglosar la información, como por ejemplo el tiempo acumulado en pausa. Entonces, la segunda tabla que presenta la vista es un reporte detallado de las pausas y el tiempo de cada una, en las que cada agente estuvo detenido: Al acceder al menú *Reportes -> Agentes*, se despliega una vista que nos permite seleccionar el agente o grupo de agentes por un lado, y la fecha o rango de fechas por el otro, lo cual nos permite filtrar la salida del reporte de actividad de agentes. El próximo parámetro que se desglosa, tiene que ver con la cantidad de llamadas procesadas por cada agente en términos de campañas. Es decir, se presenta un detalle por agente de cuántas llamadas y qué tiempo le dedicó a cada una de las campañas sobre las que procesó sus llamadas. Finalmente, se presenta una tabla en la que se detalla la cantidad de llamadas de cada tipo (manual, preview, dialer y entrantes), en las que estuvo vinculado el agente: Métricas de agente Una vez ejecutada la búsqueda, se despliega como primera información una tabla con el resumen de toda la actividad realizada por los agentes: Project-Id-Version: OMniLeads 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-12-28 10:47-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.1
 *Figure 1: Date/time and Agent Filters* *Figure 2: Agent Activity Summary* *Figure 3: Pauses Detail* *Figure 4: Calls and Campaigns* *Figure 5: Call Types* From our summary we can break down plenty of details as for example the accumulated time in pause. There you will have a detailed report of the pauses and the time each agent took. When accessing *Reports -> Agents* menu, a filter engine allows to select the agent or group of agents, and the date or range of dates. After clicking in Search button, agent activity report is displayed. The next parameter has to do with the amount of calls processed by each agent in terms of campaigns. That is to say, it presents a detail by agent of how many calls and how much time agent spent, per campaign. Finally, a last table is presented detailing the amount of calls per type (manual, preview, dialer and incoming). Agent Reports The first information that can be seen is a table with the summary of all the agent activity. 