��          �               �   $   �   '   "  R   J  (   �  )   �     �  '        3     M  
   Y     d     m     t  }  y     �       M   3     �  !   �     �     �     �       
             "     )   API de sesión de Agente en Asterisk Cierre de sesión de agente en Asterisk Endpoints para controlar la sesion del agente en Asterisk para el uso del Webphone Id de la pausa de la que sale el Agente. Id de la pausa en la que entra el Agente. Ingreso en pausa de agente Inicio de sesión de agente en Asterisk Salida de pausa de agente description field name pause_id string type Project-Id-Version: OMniLeads 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-06-29 10:25-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.1
 Asterisk's Agent Sessions API Asterisk agent session logout Endpoints to control the Asterisk's Agent Session to be used by the WebPhone. ID of the pause the agent ends. ID of the pause the agent starts. Agent Pause start Asterisk agent session login Agent Pause End description field name pause_id string type 