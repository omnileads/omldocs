��          �               �     �  )   �      �      �          4  ]   S  �   �  �   P  �   �  �  �  �   �      l  "   �     �     �     �     �     
  "     Y   >     �  }  �     2
     I
     f
     �
     �
     �
  a   �
  o     d   �  �   �  "  �  �   �     Y     g     �     �     �     �     �     �  P   �     L   *Figura 1: Tipos de llamadas* *Figura 2: Gráfico de tipos de llamadas* *Figura 3: Llamadas y campañas* *Figura 4: Llamadas predictivas* *Figura 5: Llamadas preview* *Figura 6: Llamadas entrantes* Además, se detalla la información en términos de si las llamadas fueron o no "conectadas". De ésta manera, se puede deducir rápidamente la efectividad de cada campaña a partir de la exposición de la información tanto tabular como gráficamente. El primer informe que se nos presenta, tiene que ver con esa totalidad de llamadas transaccionadas por la plataforma clasificadas en: El reporte se puede correr como en todos los casos, sobre una fecha o rango de fechas. El mismo expone información sobre todas las llamadas transaccionadas por la plataforma dentro de la fecha acotada. En las siguientes 3 secciones, se presenta de manera tabular y gráfica una descomposición de todas las llamadas procesadas por OMniLeads en términos de su naturaleza (entrantes, manuales, preview y predictivas) y a su vez dentro de cada una de estas clasificaciones se descompone en las campañas puntuales de cada tipo de llamada. Seleccionando alguna de las campañas activas, podremos tener la información desglosada por día, en el caso de que estemos buscando un rango de más de un día. La siguiente información, se presenta como una tabla en la que se descompone granularmente a todas las llamadas transaccionadas por la plataforma en términos de las campañas por donde fueron procesadas las mismas. Llamadas de campañas entrantes. Llamadas de campañas predictivas. Llamadas de campañas preview. Llamadas entrantes Llamadas manuales. Llamadas predictivas Llamadas preview Llamadas transferidas a campañas. Luego, aparecen un par de gráficos que representan las estadísticas arriba mencionadas. Reporte general de llamadas Project-Id-Version: OMniLeads 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-12-28 10:47-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.1
 *Figure 1: Call Types* *Figure 2: Types Call Graph* *Figure 3: Calls and Campaigns* Predictive Calls Preview Calls Inbound Calls The information is also detailed in terms of whether the calls were connected or not "connected". This way you can quickly deduce the effectiveness of each campaign analyzing tabular and graphical information. The first report has to do with the total amount of calls transacted by the platform, classified in: The report can be run as in all cases, based on a date or date range. It displays information about all calls managed by the platform according to the filter selected. In the following three sections, it is presented in a tabular and graphic way a breakdown of all calls processed by OMniLeads in terms of its nature (incoming, manual, preview and predictive). Then, for each type of campaign, all call information can be seen according to each type of call. The following information is presented as a table in which all calls transacted by the platform can be divided in terms of the campaigns where they were processed. Inbound Calls Predictive Outbound Calls Preview Outbound Calls Inbound Calls Manual Outbound Calls Predictive Calls Preview Calls Calls transferred to campaigns. Then there are a couple of graphs that represent the statistics above mentioned. Calls General Report 