.. _about_recordings:

Acceso al menú
**************
Para acceder al buscador de grabaciones, debemos llegar al menú *Buscar grabación*. A continuación, se depliega la vista del módulo, donde se listan las
grabaciones del día de la fecha y un conjunto de parámetros que nos permiten filtrar y recuperar grabaciones de acuerdo a criterios como los siguientes:

- **Fecha:** Permite acotar la búsqueda a una fecha o rango de fechas en particular.
- **Tipo de llamada:** Se puede filtrar por tipo de llamada (manual, entrante, preview o de discador predictivo).
- **Teléfono del contacto:** Éste campo permite acotar la búsqueda a llamadas en las que un teléfono puntual esté involucrado.
- **Call ID:** Se puede buscar una llamada si contamos con el CallerID de la misma, haciendo una búsqueda rápida y efectiva.
- **Agente:** Se puede acotar la búsqueda a llamadas en las que un agente puntual esté involucrado en la grabación.
- **Campaña:** Filtro para listar llamadas asociadas a una cierta campaña.
- **ID de contacto externo:** Podemos buscar todas las llamadas de un contacto en particular, con el campo id_externo definido en la base de contactos al momento de crear la misma.
- **Duración mínima:** Éste campo sirve para acotar la búsqueda a grabaciones con una cierta duración mínima.
- **Marcada:** Éste check sirve para recuperar grabaciones de llamadas que hayan sido observadas por el agente mediante el botón pertinente de su consola.
- **Calificada como gestión:** Como bien sabemos, en OMniLeads existen calificaciones ordinarias y calificaciones "de gestión", que son las que disparan los formularios de campaña. En éste caso, este check permite recuperar llamadas calificadas con dicho tipo de calificación. Por ejemplo: ventas, encuesta, etc.).
- **Grabaciones por página:** Definimos cuántas grabaciones queremos listar por página.
- **Generar zip de grabaciones:** Una vez listadas las grabaciones, podemos seleccionar todas las que la página permita y descargarlas en formato zip.

.. image:: images/output_recordings_1.png
.. image:: images/output_recordings_2.png

*Figura 1: Grabaciones*
