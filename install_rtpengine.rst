.. _about_install_rtpengine:

********************************
Deploy del componente RTPEngine
********************************
Como es sabido, **RTPEngine** implementa un script de *fisrt_boot_installer.tpl*, que puede ser invocado utilizando tecnología de provisioning,
o bien ser descargado y luego ejecutado sobre el Linux host en cuestión.

Para obtener nuestro script, se debe lanzar el siguiente comando:

.. code-block:: bash

  curl https://gitlab.com/omnileads/omlrtpengine/-/raw/master/deploy/first_boot_installer.tpl?inline=false > first_boot_installer.sh && chmod +x first_boot_installer.sh

Entonces, una vez que contamos con el script, pasamos a trabajar las variables, quitando comentarios y configurando sus valores dentro del propio script.
Vamos a listar y explicar cada una de éstas variables que deben ser ajustadas antes de ejecutar el script.

Variables de instalación
************************
* **oml_infras_stage**: Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise* como valor para la variable. **Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

* **oml_nic**: Aquí debemos indicar la interfaz de red sobre la que se levantará el puerto TCP 22222, que va a procesar las solicitudes provenientes desde Kamailio para establecer un SDP en la sesión WebRTC.

* **oml_public_nic**: Éste parámetro es opcional, y tiene que ver con el escenario en el cual se cuenta con 2 placas de red, una para LAN y otra WAN.

* **oml_rtpengine_release**: Aquí debemos indicar qué versión del componente se desea desplegar. Para ello, se deberá revisar el archivo `.gitmodules <https://gitlab.com/omnileads/ominicontacto/-/blob/master/.gitmodules>`_ de la versión de OMniLeads que se desee desplegar, y tomar el parámetro *branch* del componente, como valor de ésta variable.

* **oml_type_deploy**: Aquí indicamos bajo qué escenario estará operando nuestro RTPEngine. Para entender ésta variable, debemos repasar el funcionamiento de éste componente, ya que RTPEngine es quien concede los parámetros necesarios (dirección de red y puerto, entre otros) para acordar el streaming de audio con el usuario (agente o supervisor). Dependiendo del escenario dispuesto, los **posibles valores** son: *LAN, CLOUD, HYBRID_1_NIC, HYBRID_2_NIC*.

  * **LAN**: Los agentes acceden a OMniLeads SÓLAMENTE desde una red LAN.
  * **CLOUD**: Los agentes acceden a OMniLeads SÓLAMENTE desde Internet.
  * **HYBRID_1_NIC**: Los agentes acceden tanto desde una LAN como desde INTERNET, mientras que el Linux host que aloja el componente posee una sola NIC.
  * **HYBRID_2_NIC**: Los agentes acceden tanto desde una LAN como desde INTERNET, mientras que el Linux host que aloja el componente posee 2 NICs (una para LAN y otra para WAN).

Ejecución de la instalación
***************************
Finalmente, podemos lanzar nuestro script *first-boot-instaler.sh*. Ésto lo podemos hacer como user_data de una instancia cloud, a través de alguna utilidad
de línea de comandos para administrar la infraestructura, o bien directamente copiando el archivo hacia el Linux host destino y lanzando la ejecución del mismo.

Una vez ejecutado el script y finalizado el deploy, debemos comprobar que el componente se encuentra operativo, lanzando el siguiente comando:

.. code-block:: bash

  systemctl status rtpengine

.. image:: images/install_rtpengine_status.png

Como se remarca en la figura, es importante revisar que la dirección IP allí expuesta, coincida con vuestro escenario. Es decir, si estamos
en un ambiente de usuarios que acceden desde LAN, tendrá que utilizar una IPv4 privada, y si los usuarios acceden desde Internet, deberá ser una
IPv4 pública.
