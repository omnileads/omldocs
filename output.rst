***********************************
Métricas, grabaciones y supervisión
***********************************

Grabaciones
***********
Todas las campañas que operen con la opción de grabación de llamadas habilitada, generan sus archivos con grabaciones para posterior escucha o descarga.
El módulo de *Grabaciones* de OMniLeads, permite la búsqueda de grabaciones utilizando los filtros como criterio de búsqueda.

.. toctree::
  :maxdepth: 1

  output_recordings.rst

Output de las campañas entrantes
********************************
En ésta sección, se expone y analiza todo lo inherente al "output" generado por las campañas de llamadas entrantes.

.. toctree::
  :maxdepth: 1

  output_inbound_camp.rst

Output de las campañas salientes
********************************
En ésta sección, se expone y analiza todo lo inherente al "output" generado por las campañas de llamadas salientes.

.. toctree::
  :maxdepth: 1

  output_outbound_camp.rst

Reporte general de llamadas
***************************
Ésta sección, repasa cada informe generado por el reporte general de llamadas. Menú: *Reportes -> Llamadas*.

.. toctree::
  :maxdepth: 1

  output_reports_general.rst

Reportes general de agentes
***************************
Ésta sección, repasa cada informe generado por el reporte general de actividad de agente. Menú: *Reportes -> Agentes*.

.. toctree::
  :maxdepth: 1

  output_reports_agents.rst

Supervisión realtime
********************
Ésta sección, repasa el módulo de supervisión de OMniLeads.

.. toctree::
  :maxdepth: 1
  
  output_supervision.rst
