.. _about_timeconditions:

*********************************************
Enrutamiento condicionado por rango de tiempo
*********************************************
En ésta sección, se trabaja con el concepto de *Enrutamiento de llamadas entrantes condicionado por rango de tiempo*, para configurar el flujo de llamadas
entrantes hacia diferentes destinos internos, a partir de comparar la fecha/hora en que llega una llamada y un patrón (de fechas y horas) relacionado,
de manera tal que se pueda planificar de antemano si una llamada debe ir hacia un destino u otro de acuerdo al resultado de dicha comparación.

Por ejemplo, una llamada podría ir hacia una campaña entrante *dentro del rango de fecha y horario de atención al cliente* y hacia un IVR que reproduzca
un anuncio acerca de los horarios de atención, cuando la llamada ingrese fuera de ese rango definido.

.. image:: images/campaigns_in_tc_diagrama.png

*Figura 1: Condición de tiempo*

Existen 2 módulos que trabajan en conjunto y que permiten implementar éste tipo de configuraciones, los cuales se detallan a continuación.

Grupos horarios
***************
Éste módulo permite agrupar patrones de fechas y horas como un objeto, para luego poder ser invocados por los objetos del tipo condicionales de tiempo.

Para definir o editar grupos de horarios, se debe acceder al menú *Telefonía -> Grupos horarios*. Para añadir un nuevo grupo se debe presionar
el botón "Agregar grupo horario".

La pantalla de grupos horarios se expone en la siguiente figura:

.. image:: images/campaigns_in_tc1.png

*Figure 2: Grupos horarios*

Una vez generados los *grupos horarios*, podemos invocarlos desde el módulo complementario *Validaciones horarias*.

Validaciones horarias
*********************
Éste módulo permite comparar la fecha y hora en el momento de procesar una llamada, con un grupo horario asignado como patrón de comparación.
Luego, en base a la coincidencia o no con alguna franja de fecha/hora del grupo, la llamada se envía hacia el destino positivo o negativo de la comparación.

Un *nodo* condicional de ésta clase puede ser invocado por otros nodos:

- Rutas entrantes
- Opción de IVR
- Failover de una campaña entrante
- Otro condicional de tiempo

Para generar un elemento condicional de tiempo, se debe acceder al menú *Telefonía -> Validaciones horarias*.

La pantalla de configuración es similar a la siguiente figura:

.. image:: images/campaigns_in_tc2.png

*Figure 3: Validaciones horarias*

Finalmente, tenemos disponible éste elemento de enrutamiento para ser utilizado por ejemplo, como destino de una ruta entrante.
