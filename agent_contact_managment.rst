Listado de contactos de la campaña
**********************************
El agente puede ingresar a los contactos de cada campaña a la que se encuentra asignado. Ésto, se hace ingresando al
punto menú *Contactos -> Lista de contactos".

Allí, se despliega una vista que permite seleccionar la campaña a la cual ingresar para visualizar sus contactos:

.. image:: images/about_agent_contact_list_1.png

*Figura 1: Lista de contactos de campaña*

Al listar todos los contactos de la campaña, el agente puede recorrer cada uno de ellos, o bien realizar una búsqueda
por ID de contacto, teléfono, nombre, apellido, etc.:

.. image:: images/about_agent_contact_list_2.png

*Figura 2: Búsqueda de contactos*

Por ejemplo, podemos buscar un número de teléfono en el listado de contactos, como se ejemplifica en la siguiente figura:

.. image:: images/about_agent_contact_list_3.png

*Figura 3: Búsqueda de contacto por número*

La herramienta permite poder editar cualquiera de estos contactos:

.. image:: images/about_agent_contact_list_4.png

*Figura 4: Búsqueda de contactos*

Agendas pendientes
******************
El agente puede acceder a su agenda de llamadas pendientes de realizar. En dicha sección, se listan todas
las entradas que el agente realizó durante la gestión:

.. image:: images/about_agent_agenda_1.png

*Figura 5: Agenda*

El agente cuenta con cada contacto agendado y su descripción. A partir de hacer click sobre el número de teléfono,
automáticamente se dispara la llamada hacia el teléfono del contacto.

Histórico de calificaciones del agente
**************************************
En éste menú, el agente podrá listar a todos los contactos que ha calificado a nivel histórico. Por lo tanto,
el agente puede llevar un control hacia atrás de cada contacto gestionado:

.. image:: images/about_agent_dispositions_history_1.png

*Figura 6: Lista de calificaciones de llamadas*

Se puede filtrar por fecha la búsqueda y además el agente podrá ingresar al contacto seleccionado para
repasar sus datos o modificar la calificación asignada previamente:

.. image:: images/about_agent_dispositions_history_2.png

*Figura 7: Edición de calificaciones de llamadas*

Como se puede observar, el agente puede modificar los datos de la calificación o del formulario, en caso de tratarse
de un contacto calificado con una calificación "de gestión".

Búsqueda de grabaciones del agente
**********************************
En este menú, el agente podrá buscar las grabaciones de las llamadas en las que haya participado:

.. image:: images/about_agent_search_record_1.png

*Figura 8: Lista de grabaciones de llamadas*

Se puede filtrar por fecha, tipo de llamada, teléfono del cliente, CallID, campaña, si fue marcada, si está calificada como gestión y por su duración.
