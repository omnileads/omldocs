.. _about_general_reports:

Reporte general de llamadas
***************************
El reporte se puede correr como en todos los casos, sobre una fecha o rango de fechas. El mismo expone información
sobre todas las llamadas transaccionadas por la plataforma dentro de la fecha acotada.

El primer informe que se nos presenta, tiene que ver con esa totalidad de llamadas transaccionadas por la plataforma
clasificadas en:

- Llamadas manuales.
- Llamadas de campañas preview.
- Llamadas de campañas predictivas.
- Llamadas de campañas entrantes.
- Llamadas transferidas a campañas.

.. image:: images/output_gral_calls_1.png

*Figura 1: Tipos de llamadas*

Además, se detalla la información en términos de si las llamadas fueron o no "conectadas".

Luego, aparecen un par de gráficos que representan las estadísticas arriba mencionadas.

.. image:: images/output_gral_calls_2.png

*Figura 2: Gráfico de tipos de llamadas*

La siguiente información, se presenta como una tabla en la que se descompone granularmente a todas las llamadas transaccionadas
por la plataforma en términos de las campañas por donde fueron procesadas las mismas.

.. image:: images/output_gral_calls_3.png

*Figura 3: Llamadas y campañas*

En las siguientes 3 secciones, se presenta de manera tabular y gráfica una descomposición de todas las llamadas procesadas
por OMniLeads en términos de su naturaleza (entrantes, manuales, preview y predictivas) y a su vez dentro de cada una
de estas clasificaciones se descompone en las campañas puntuales de cada tipo de llamada. Seleccionando alguna de las campañas activas,
podremos tener la información desglosada por día, en el caso de que estemos buscando un rango de más de un día.

De ésta manera, se puede deducir rápidamente la efectividad de cada campaña a partir de la exposición de la información
tanto tabular como gráficamente.

- Llamadas predictivas

.. image:: images/output_gral_calls_4.png

*Figura 4: Llamadas predictivas*

- Llamadas preview

.. image:: images/output_gral_calls_6.png

*Figura 5: Llamadas preview*

- Llamadas entrantes

.. image:: images/output_gral_calls_5.png

*Figura 6: Llamadas entrantes*
