Llamadas manuales desde listado de contactos
********************************************
Cuando un agente trabaja dentro de una campaña manual, puede generar los llamados a partir de listar los contactos
de la campaña, ésto es ingresando a *Contactos -> Lista de contactos* y allí se puede seleccionar
la campaña manual sobre la cual buscar el contacto a marcar. Ésto, se muestra en la siguiente figura:

.. image:: images/about_agent_manual_camp_1.png

*Figura 1: Lista de contactos*

Si se hace click en "Mostrar contactos", aparecen los mismos listados, como lo indica la siguiente figura:

.. image:: images/about_agent_manual_camp_2.png

*Figura 2: Lista de contactos*

Entonces, el agente puede generar una llamada hacia uno de los teléfonos listados, a partir de realizar un click
sobre el ícono del teléfono. A partir de ese momento, se presenta la información del contacto en la pantalla de agente
y seguidamente se comienza a marcar su teléfono:

.. image:: images/about_agent_manual_camp_3.png

*Figura 3: Llamada a contacto*

Si la comunicación ha finalizado o bien el teléfono no pudo ser contactado, entonces el agente puede intentar marcar
a otro de los números del contacto (si es que el contacto tiene más de un teléfono cargado). Si este es el caso,
entonces el agente puede hacer click sobre cualquiera de los teléfonos extras y automáticamente se buscará contactar
al nuevo teléfono.

.. image:: images/about_agent_manual_camp_4.png

*Figura 4: Rellamar a contacto*

Todas las llamadas del contacto en pantalla pueden ser calificadas, siempre y cuando la calificación no sea de gestión (despliegue de un formulario,
donde se cierra dicha gestión).

Finalmente, el agente debe calificar al contacto a través del combo de calificaciones. Éste listado de calificaciones
fue generado por el administrador para cada campaña:

.. image:: images/about_agent_manual_camp_5.png

*Figura 5: Calificación de llamada*

Llamadas manuales marcando desde el webphone
********************************************
El agente puede marcar llamadas directamente sobre el webphone. Es común a veces distribuir las llamadas a realizar
por los agentes, a través de una planilla de cálculo o buscando los datos en un CRM externo. Ésto se muestra en la siguiente figura:

.. image:: images/about_agent_manual_camp_6.png

*Figura 6: Llamada manual desde webphone*

Presionando *enter* o disparando el llamado desde el botón *dial*, si aún no tiene una campaña pre-seleccionada, el sistema
pregunta por qué campaña procesar la nueva llamada:

.. image:: images/about_agent_manual_camp_7.png

*Figura 7: Selección de campaña*

OMnileads busca primero si el teléfono marcado existe como asignado sobre algún contacto del sistema, y si el mismo existe, se despliegan los contactos que posean
dicho teléfono. Entonces, el agente puede indicar marcar a dicho contacto, caso en el cual la llamada se lanza y los datos
del contacto son desplegados sobre la consola de agente:

.. image:: images/about_agent_manual_camp_8.png

*Figura 8: Selección de contacto*

.. image:: images/about_agent_manual_camp_9.png

*Figura 9: Llamada a contacto*

Llamadas a números sin contacto existente
*****************************************
También puede suceder que el teléfono marcado desde el webphone no coincida con ningún contacto, tal como se muestra en la siguiente figura:

.. image:: images/about_agent_manual_camp_10.png

*Figura 10: Número marcado sin contacto existente*

En éste caso, el agente puede o bien directamente marcar el teléfono y luego cargar el contacto:

.. image:: images/about_agent_manual_camp_11.png

*Figura 11: Llamada a contacto sin identificar*

O bien puede cargar el nuevo contacto a la campaña antes de contactar el teléfono:

.. image:: images/about_agent_manual_camp_12.png

*Figura 12: Cargar contacto y llamar*
