.. _about_install_containers:

***********
Deploy Tool
***********

Se trata de una herramienta de gestión de instancias de OMniLeads que tiene como finalidad dotar al administrador IT encargado de los despliegues de la App
de una herramienta centralizada desde donde se pueden ejecutar tareas de Instalacion, Actualizaciones, Rollbacks, Backups & Restores. A partir de un archivo 
de parámetros correspondiente a cada instancia (o tenant) que debe ser mantenido por el ingeniero sre. 

.......

Deploy Tool permite desplegar sobre 


VMs onpremise

.....

VMs onpremise HA

.....

Cloud providers

Deploy Tool permite desplegar en cualquier Nube pudiendo ser 100% orientada a instancias Linux o bien utilizando servicios de Bucket y PostgreSQL como servicios 
administrados por el proveedor Cloud. 

......


Deploy basic
************

.......

Deploy HA 2 Hypervisors
***********************

.......

Deploy sobre Cloud
******************

.......




Deploy de OMniLeads basado en Terraform
****************************************
A partir de la versión 1.16, es posible utilizar Terraform para desplegar OMniLeads sobre algunas nubes concretas que se irán incrementando con el
pasar del tiempo y el aporte de la comunidad.

Para quienes no estén familiarizados con Terraform y estén interesados en conocer, dejamos el enlace a su `documentación <https://www.terraform.io/>`_. Terraform nos permite
codificar un despliegue. En términos de Terraform, podemos definir un despliegue de OMniLeads, como el siguiente conjunto de acciones:

 * Configuración de networking de la infraestructura.
 * Creación de máquinas virtuales, clusters de DB, balanceadores de carga.
 * Deploy de OMniLeads y sus componentes sobre las máquinas virtuales.
 * Aplicación de la configuración de seguridad.
 * Generación de certificados SSL y URL de acceso para los usuarios.

 .. image:: images/install_terraform_tenant_arq.png

Por lo tanto, usted puede simplemente configurar un archivo de variables y luego lanzar un comando (terraform apply), y en cuestión de minutos
contar con un despliegue de OMniLeads "as a Service", listo para que nuestros usuarios puedan comenzar a operar desde cualquier punto geográfico.

.. note::

  El deploy de los componentes Jitsi Meet, WombatDialer y MySQL, es configurable de acuerdo a las necesidades de la operación sobre campañas predictivas
  y/o campañas de video atención.

El enfoque de desplegar en nube de infraestructura, nos permite automatizar completamente la generación de un suscriptor al servicio de Contact Center
en la nube.

.. image:: images/install_terraform_tenants_2 .png

A continuación, el listado de nubes públicas soportadas:

* :ref:`about_install_terraform_digitalocean`
* :ref:`about_install_terraform_aws`
