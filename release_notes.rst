Release Notes 1.27.0
*********************

*05-04-2023*

Nuevas Funcionalidades
======================

* OML-2361: Se añade la posibilidad de generar Metadata para posterior data-analysis sobre grabaciones de llamadas
* OML-2352: Se optimizan los reciclados de bases de datos
* OML-2338: Optimzación sobre el manejo de "tiempo de Hold" cuando no existe evento de cierre explicito
* OML-2353: Restricción sobre caracteres de Nombre de Base de Contactos
* OML-2247: Se añade la posibilidad de obtener el detalle sobre tipo de pausa Productiva/recreativa en consola de agente
* OML-2349: Se añade información de Transferencias efectuadas al addon CX-Survey en Reportes de Agentes
* OML-2385: Optimización en Vista de Agregado de Contactos a Base de Campaña
* OML-2386: Se optimizan consultas SQL sobre el Módulo de Grabaciones cuando el filtro es por Calificación

Bug fixes & Optimizaciones
==========================

* OML-2359: Fix en eliminación de asignaciones de contactos sobre campañas preview ya finalizadas
* OML-2351: Fix en Grupos Horarios y Destinos Personalizados al regenerar configuración telefónica
* OML-2358: Fix en Paginación de Lista de Previews de Agente cuando hay mas de 10 campañas asignadas
* OML-2354: Fix en evento de forzar calificación cuando Auto-Despausa está configurado.
* OML-2350: Fix en opción de Hangup sobre eventos de timeout de espera en cola
* OML-2355: Fix en longitud de Checkbox sobre opción Grabar en Wizard de Campaña.
* OML-2292: Fix sobre URLs de archivos de grabación cuando los mismos apuntan a un Bucket S3 (object-storage)
* OML-2383: Fix en Liberar y Reservar Contactos Preview cuando las bases de contactos se comparten.