.. _about_ivr:

********************************
IVR - Interactive Voice Response
********************************
Los IVRs hacen posible que la persona que realiza una llamada entrante, pueda seleccionar un destino apropiado en base a informar los mismos a través de una grabación y aguardando la interacción a través de los tonos de teclado DTMF.
Con ésta herramienta, un administrador puede enrutar llamadas entrantes hacia un IVR y configurar el mismo para que diferentes DTMFs se conmuten hacia diferentes campañas entrantes, IVRs, condicionales de fecha y hora, etc.

.. image:: images/campaigns_in_ivr_diagrama.png

*Figura 1: IVR*

Para generar un nuevo IVR, se necesita como mínimo un audio a reproducir (disponible en la biblioteca de audios) y además un destino por defecto hacia donde enviar las llamadas que pasen por dicho IVR.

Para añadir un IVR, se debe acceder al menú *Telefonía -> IVR*, y seleccionar "Agregar IVR". Se desplegará una pantalla similar a la siguiente figura:

.. image:: images/campaigns_in_ivr.png

*Figure 2: Parámetros de IVR*

El formulario se puede dividir en 3 secciones, donde la primera sección contiene los siguientes campos:

- **Nombre:** Es el nombre del objeto, con el cual se referencia en el listado de IVRs.
- **Descripción:** Campo opcional dedicado a un comentario aclaratorio sobre el objeto.
- **Archivo interno:** Se selecciona ésta opción en caso de querer seleccionar como audio principal del IVR, un archivo previamente subido por el módulo de audios de OML.
- **Archivo externo:** Se selecciona ésta opción en caso de querer seleccionar como audio principal del IVR, un archivo y subirlo en el mismo instante al sistema.

Luego siguen las secciones para configurar acciones de timeout y opciones inválidas:

- **Time out:** Es la cantidad de segundos que se aguarda a que el "llamante" introduzca un DTMF a partir de la finalización de la reproducción del audio del IVR.
- **Time out retries:** Es la cantidad de intentos que el IVR ofrece al "llamante", a la hora de dar falla por "timeout". Es decir, se permite una cierta cantidad de intentos fallidos por timeout, para luego ejecutar la acción referenciada en "Tipo de destino para time out".
- **Time out audio:** Cada vez que se da un timeout por no ingreso de DTMF, se puede reproducir un audio subido previamente al módulo de audio de OML, que indique el error.
- **Time out ext audio:** Cada vez que se da un timeout por no ingreso de DTMF, se puede reproducir un audio que se puede seleccionar y subir en el momento, que indique el error.
- **Tipo de destino time out:** En caso de cumplirse la cantidad de "retries" de timeout, la llamada es enviada por el IVR hacia el tipo de destino por defecto para los timeouts de IVR. En éste campo se indica dicha clase de destino.
- **Destino time out:** Finalmente, se selecciona puntualmente el objeto dentro de la familia de tipo de destino.
- **Invalid retries:** Es la cantidad de intentos que el IVR ofrece al "llamante" a la hora de dar falla de destino ingresado inválido. Es decir, se permite una cierta cantidad de intentos fallidos por opción inválida, para luego ejecutar la acción referenciada en "Tipo de destino para destino inválido".
- **Invalid audio:** Cada vez que se da un ingreso de opción inválida, se puede reproducir un audio subido previamente al módulo de audio de OML, que indique el error.
- **Invalid destination ext audio:** Cada vez que se da un ingreso de opción inválida, se puede reproducir un audio que se puede seleccionar y subir en el momento, que indique el error.
- **Tipo de destino para destino inválido:** En caso de cumplirse la cantidad de "retries" de opción incorrecta, la llamada es enviada por el IVR hacia el tipo de destino por defecto para los ingresos de opciones incorrectas del IVR. En éste campo se indica dicha clase de destino.
- **Destino inválido:** Finalmente, se selecciona puntualmente el objeto dentro de la familia de tipo de destino.

Finalmente, la tercera sección despliega tantas filas como opciones de DMTF implique el IVR.

Por cada fila, se puede asignar un DTMF a un destino de conmutación de la llamada.

Como bien se conoce, la idea es seleccionar un "tipo de destino" y un "destino particular dentro del tipo", para cada DTMF del IVR que se brinda como opción.

Finalmente, al seleccionar el botón guardar, se dispondrá del IVR.

 .. note::
  Es posible anidar un IVR dentro de otro.
