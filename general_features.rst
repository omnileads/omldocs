.. _about_general_features:

.. _about_webrtc:

**************************************
Características generales de OMniLeads
**************************************

WebRTC - Tecnología subyacente de OMniLeads
*******************************************
Antes de citar los casos de uso, ponemos énfasis en repasar los beneficios de la tecnología WebRTC, núcleo de OMniLeads.

WebRTC dota a un navegador web de la posibilidad de mantener comunicaciones real-time de voz, video, chat y compartición de pantalla.
OMniLeads se nutre de ésta tecnología para nuclear las comunicaciones y la interfaz de gestión web, evitando el uso de aplicaciones de escritorio
"softphones", lo cual otorga una inmediatez en términos de "click and work" en el alta usuarios, ya que a partir de un login web, dichos usuarios están en línea
procesando comunicaciones.

.. image:: images/what_is_webrtc.png
        :align: center

Algunas de las principales ventajas de WebRTC son las siguientes:

- Se minimizan los puntos de fallo en las estaciones de trabajo.
- Se minimizan las tareas del helpdesk y por ende, la demanda del personal de soporte/sistemas.
- Se trabaja con los codecs de audio Opus y video VP8, ambos concebidos para una máxima performance en entornos de Internet (codecs internet nativos).
- A nivel seguridad, todas las comunicaciones viajan cifradas de manera obligatoria, en términos de señalización y media.

.. _about_omlfeatures:

Características y funcionalidades de OMniLeads
**********************************************
- Gestión de campañas entrantes y salientes.
- Consola de agente WebRTC (no se requiere instalar ninguna aplicación ni plugin, 100% web browser).
- Opus® como codec por defecto para todas las comunicaciones de agente.
- Consola de supervisión WebRTC, con detalle de estados de agentes y campañas.
- Reportes de productividad de agentes y campañas.
- Búsqueda de grabaciones con filtros de fecha, campaña, agente, calificaciones, llamadas "observadas", etc.
- Reciclado de campañas por calificación de agente y/o status telefónico.
- Cambio de base de contactos sobre la misma campaña.
- Detección de contestadores con reproducción de mensaje de audio.
- Integración con CRM/ERP a través de la API RestFull.
- Listo para virtualizar! OMniLeads fue concebido como una tecnología orientada a los entornos de virtualización.
- Listo para dockerizar! OMniLeads está disponible en términos de imágenes Docker, lo cual permite ser ejecutado en cualquier OS, así como también orquestando clusters Docker HA y/o despliegues en proveedores de nube (AWS, GCloud, etc.).
- Listo para escalar! La escalabilidad es posible debido al hecho de utilizar tecnologías subyacentes muy potentes como PostgreSQL, Nginx, RTPEngine, que a su vez pueden correr en hosts independientes (escalabilidad horizontal) con una mínima configuración.
- Addons complementarios que dotan a la plataforma de funcionalidades extras y/o para segmentos verticales.
- Orientación 100% a Contact Center. No se trata de un software de PBX con agregados de reportería y/o supervisión. La aplicación fue concebida desde cero, y como una plataforma orientada y optimizada para funcionalidades de Contact Center.

.. _about_usecase_pbx:

OMniLeads como Contact Center integrado a una PBX basada en SIP
***************************************************************
OMniLeads resulta ideal para las compañías que demandan funcionalidades típicas de Contact Center, que el sistema PBX no llega a cubrir por su propia naturaleza.
Por lo tanto, OMniLeads surge como una alternativa para complementar dicha central PBX, desde una instancia independiente (bare-metal host, virtual machine o infraestructura de cloud) integrada a la PBX,
permitiendo el fluir de las comunicaciones entre ambos componentes, de manera fiable, segura y transparente.

Se plantea expandir el paradigma tradicional de adquisición de un stack de software de reportería/supervisión instalado sobre la PBX, para en lugar de ello, desplegar una completa aplicación
de Contact Center independiente (utiliza su propio Asterisk), que permite a su vez una integración sencilla con el software de PBX, de manera tal que podamos derivar una opción del IVR de la PBX
hacia una campaña entrante de OMniLeads, o bien realizar una transferencia desde una extensión de la PBX hacia OMniLeads, o viceversa.

Las ventajas que se hacen notables, son:

- Evitar el coste económico que involucran las licencias de software de las típicas herramientas complementarias del mercado, que dotan a la PBX de algunas funcionalidades de *reporting y supervisión* de colas.
- Evitar el coste en términos de performance del core de telefonía *PBX* sacrificada para correr complejos reportes y herramientas de monitoreo, que implica ejecutar un «módulo de call center» sobre el sistema PBX.

En operaciones donde hay una gran demanda de extracción de reportes, o bien donde se necesita escalar en términos de agentes, es sumamente sencillo desplegar OMniLeads *out of the box*, ya sea en una VM, VPS o server dedicado,
sin perder la integración con la PBX.

.. image:: images/oml_and_pbx.png
        :align: center

.. _about_usecase_bpo:

OMniLeads en una compañía de servicios de Customer Contact
**********************************************************
Bajo éste escenario, OMniLeads puede trabajar como núcleo de comunicaciones de un Contact Center con agentes que van entre las decenas y centenas.
Así, OMniLeads puede manejar múltiples troncales SIP a la vez, con sus pertinentes enrutamientos entrantes y salientes de comunicaciones.

En éstos contextos, la escalabilidad es un requisito básico, ya que las operaciones son muy dinámicas y pueden demandar picos de usuarios conectados
trabajando en simultáneo. La escalabilidad se garantiza a partir de concebir nuestra solución de manera tal que pueda ser fácilmente desplegada
en modalidad de cluster de alta disponibilidad.

A su vez, la API RestFull permite generar fácilmente CRMs o web workflows para cada campaña, de manera tal de ajustarse a los requisitos del cliente que terceriza la cartera.

.. image:: images/oml_bpo.png
        :align: center

.. _about_usecase_cloud:

OMniLeads para carriers o proveedores de cloud PBX
**************************************************
Si la necesidad es implementar un servicio de CCaaS (Contact Center as a Service), OMniLeads resulta ideal a partir de la ventaja otorgada por WebRTC y Docker como
tecnología de base.

Podemos citar ventajas como las siguientes:

* **WebRTC** elimina la necesidad de instalar aplicaciones softphone para escritorio, ya que la voz y el video fluye a través del browser de los agentes y supervisores. Ésto elimina un punto de falla y mantenimiento sobre las estaciones de trabajo.
* Los **codecs** implementados para audio y video son Opus y VP8, ambos diseñados para funcionar en Internet, los cuales se adaptan dinámicamente al ancho de banda disponible, lo que evita los incómodos entrecortes de llamadas de la VoIP convencional.
* **Seguridad**: El intercambio de información entre las estaciones de trabajo y la instancia de OML en cloud, se encuentra encriptado bajo los estándares HTTPS, sRTP y dTLS.
* `Kamailio <https://www.kamailio.org/>`_ es parte del core de comunicaciones de OMniLeads. Se trata de un Proxy-SIP de avanzadas prestaciones y crucial para brindar seguridad a servidores de VoIP y video de acceso público en Internet.
* Docker permite desplegar OMniLeads fácilmente abstrayendo la infraestructura subyacente, y permitiendo sin problemas correr en VPS, AWS, GCloud, etc.

.. image:: images/what_is_webrtc_oml.png
        :align: center
