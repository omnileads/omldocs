.. _about_install_websockets:

********************************
Deploy del componente Websockets
********************************
Como es sabido, **Websockets** implementa un script de *fisrt_boot_installer.tpl*, que puede ser invocado utilizando tecnología de provisioning,
o bien ser ejecutado manualmente con una previa parametrización de las variables implicadas.

Para obtener nuestro script, se debe lanzar el siguiente comando:

.. code-block:: bash

  curl https://gitlab.com/omnileads/omnileads-websockets/-/raw/master/deploy/first_boot_installer.tpl?inline=false > first_boot_installer.sh && chmod +x first_boot_installer.sh

Entonces, una vez que contamos con el script, pasamos a trabajar las variables, quitando comentarios y configurando sus valores dentro del propio script.
Vamos a listar y explicar cada una de éstas variables que deben ser ajustadas antes de ejecutar el script.

Variables de instalación
************************
* **oml_infras_stage**: Se refiere al proveedor de infraestructura implicado. La idea es aprovechar las APIs de los proveedores cloud para determinar cuestiones como parámetros de la red. Si vamos a instalar OnPremise o en alguna nube que no está dentro del listado, asignar *onpremise* como valor para la variable. **Posibles valores**: *onpremise, aws, digitalocean, vultr, linode*.

* **oml_nic**: Aquí debemos indicar la interfaz de red sobre la que se levanta el puerto TCP 8000 (default).

* **oml_ws_release**: Aquí debemos indicar qué versión del componente se desea desplegar. Para ello, se deberá revisar el archivo `.gitmodules <https://gitlab.com/omnileads/ominicontacto/-/blob/master/.gitmodules>`_ de la versión de OMniLeads que se desee desplegar, y tomar el parámetro *branch* del componente, como valor de ésta variable.

* **oml_ws_port**: Aquí se indica el puerto TCP sobre el que se publicará el servicio de websocket.

* **oml_redis_host**: Aquí se debe indicar la dirección del host Redis. Cuando Redis es desplegado en un nodo simple, el valor del parámetro debe ser: **redis://redis_host:redis_port**. En cambio, cuando Redis es desplegado como cluster, el parámetro debe ser: **redis+sentinel://master/sentinel_host_01,sentinel_host_02,sentinel_host_03**, donde el valor *master* es constante, mientras que sentinel_host implica la dirección de red de cada nodo sentinel.

* **oml_redis_port**: Aquí se debe indicar el puerto sobre el que recibe las conexiones Redis.

Con respecto a las variables implicadas para un despliegue en Alta Disponibilidad (HA), las mismas vienen
comentadas por defecto. En caso de desplegar en HA, se deberán descomentar y completar:

* **oml_redis_cluster**: En caso de tratarse de un despliegue HA, se debe apuntar el componente Websockets al cluster de Redis. Por ende, se debe indicar ésta variable en **true**.

* **oml_redis_sentinel_host_01**: Nodo de réplica 1.

* **oml_redis_sentinel_host_02**: Nodo de réplica 2.

* **oml_redis_sentinel_host_03**: Nodo de réplica 3.

Ejecución de la instalación
***************************
Finalmente, podemos lanzar nuestro script *first-boot-instaler.sh*. Ésto lo podemos hacer como user_data de una instancia cloud, a través de alguna utilidad
de línea de comandos para administrar la infraestructura, o bien directamente copiando el archivo hacia el Linux host destino y lanzando la ejecución del mismo.

Una vez ejecutado el script y finalizado el deploy, debemos comprobar que el componente se encuentra operativo, lanzando el siguiente comando:

.. code-block:: bash

  systemctl status container-oml-websockets-server.service

.. image:: images/install_websockets_status.png

.. Important::

  ¡Es importante asegurarnos de que Websockets abra su puerto (8000, por defecto) sobre una dirección de red privada! Es decir, NUNCA debemos instalar éste componente sobre una IP pública.
