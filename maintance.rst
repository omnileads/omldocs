******************************
Gestiones del administrador IT
******************************

.. _about_maintance_envvars:

Variables de entorno
*********************

A lo largo de esta sección vamos a estar comentando procedimientos que implican volver a las contraseñas utilizadas en el deploy a través del archivo *inventory*.
Como es sabido, dicho archivo es editado a la hora de realizar la instalación, pero posteriormente vuelve a su estado original quedando todas las variables y sus valores
disponibles como *variables de entorno* del sistema operativo.

Para consultar dichas variables podemos ejecutar un *cat* sobre el archivo */etc/profile.d/omnileads_envars.sh*.

.. code-block:: bash

 cat /etc/profile.d/omnileads_envars.sh

 AMI_USER=omnileadsami
 AMI_PASSWORD=5_MeO_DMT
 ASTERISK_IP=192.168.95.163
 ASTERISK_HOSTNAME=localhost.localdomain
 ASTERISK_LOCATION=/opt/omnileads/asterisk
 CALIFICACION_REAGENDA=Agenda
 DJANGO_PASS=098098ZZZ
 DJANGO_SETTINGS_MODULE=ominicontacto.settings.production
 EPHEMERAL_USER_TTL=28800
 EXTERNAL_PORT=443
 INSTALL_PREFIX=/opt/omnileads/
 KAMAILIO_IP=192.168.95.163
 KAMAILIO_HOSTNAME=localhost.localdomain
 KAMAILIO_LOCATION=/opt/omnileads/kamailio
 MONITORFORMAT=mp3
 NGINX_HOSTNAME=localhost.localdomain
 OMNILEADS_HOSTNAME=localhost.localdomain
 PGHOST=localhost.localdomain
 PGDATABASE=omnileads
 PGUSER=omnileads
 PGPASSWORD=my_very_strong_pass
 PYTHONPATH=$INSTALL_PREFIX
 REDIS_HOSTNAME=localhost
 SESSION_COOKIE_AGE=3600
 TZ=America/Argentina/Cordoba
 WOMBAT_HOSTNAME=localhost.localdomain
 WOMBAT_USER=demoadmin
 WOMBAT_PASSWORD=demo

 export AMI_USER AMI_PASSWORD ASTERISK_IP ASTERISK_HOSTNAME ASTERISK_LOCATION CALIFICACION_REAGENDA DJANGO_SETTINGS_MODULE DJANGO_PASS EPHEMERAL_USER_TTL EXTERNAL_PORT INSTALL_PREFIX KAMAILIO_IP KAMAILIO_HOSTNAME KAMAILIO_LOCATION MONITORFORMAT NGINX_HOSTNAME OMNILEADS_HOSTNAME PGHOST PGDATABASE PGUSER PGPASSWORD PYTHONPATH REDIS_HOSTNAME SESSION_COOKIE_AGE TZ WOMBAT_HOSTNAME WOMBAT_USER WOMBAT_PASSWORD

De esta manera el administrador podrá disponer de todos estos parámeros operativos cuando desee.

.. Important::

  No editar este archivo bajo ninguna condición

Configuración del módulo de *Discador predictivo*
*************************************************

.. note::

  Antes que nada se notifica que si la instancia de OML desplegada en los pasos anteriores, NO contemplan el uso de campañas con discado saliente predictivo, este paso puede ser omitido.

OMniLeads necesita de una herramienta de terceros para implementar las campañas con discador predictivo. Esta herramienta se basa en licencias de software comerciales que deben
ser gestionadas con el fabricante.

De todas maneras el sistema puede ser utilizado con un canal de pruebas que otorga como demo. Por lo tanto podemos configurar el componente y correr pruebas de concepto
antes de adquirir licencias para una operación real.

Si se desean correr campañas predictivas, se debe generar la siguiente configuración básica de Wombat Dialer .
Para generar esta configuración debemos seguir una serie de pasos que comienzan con el acceso a la URL correspondiente.

http://omnileads.yourdomain:8080/wombat ó http://XXX.XXX.XXX.OML:8080/wombat

.. Note::

  En caso de estar corriendo OMniLeads en **Docker** la URL es:
  http://XXX.XXX.XXX.OML:442/wombat

  Donde XXX.XXX.XXX.OML es la dirección IP del docker engine host

Creación de base de datos
##########################

Al ingresar por primera vez, se debe proceder con la creación de la base de datos MariaDB que utiliza Wombat Dialer.
Hacer click en botón remarcado en la figura 1.

.. image:: images/maintance_wd_2.png

*Figure 1: DB create*

Luego es el momento de ingresar la clave del usuario root de MySQL y hacer click en botón remarcado en la figura 2.

.. Note::

  A partir de la versión 1.6.0 OMniLeads no setea password de usuario root de MySQL, dejar este campo vacio. A no ser que el Administrador haya generado una clave de MySQL de root.

Procedemos entonces con la creación de la base de datos MySQL que utilizará de ahora en más el componente Wombat Dialer.

.. image:: images/maintance_wd_mariadb_create.png

*Figure 2: MySQL root password*

Primer login
#############

Una vez creada la base de datos MariaDB que utiliza Wombat Dialer, se procede con el primer login.

.. image:: images/maintance_wd_mariadb_post_create.png

*Figure 3: Login post db create*


A continuación se debe realizar un login en la interfaz de administración de Wombat Dialer para avanzar con la configuración
de parámetros necesarios para la interacción con OML.

Al ingresar se despliega una pantalla como la siguiente, donde debemos acceder con el usuario y passwords que se generaron en la instalación.

.. image:: images/maintance_wd_1.png

*Figure 4: Access to WD*

Cambio de credenciales web
###########################

Por defecto Wombat Dialer viene con las credenciales web *demoadmin* como user y *demo* como pass. Estas credeciales se pueden cambiar, para ello:

* Primeramente, las credenciales deben ser definidas en el archivo de inventario, son las variables **dialer_user** y **dialer_password**. Ver :ref:`about_install_inventory_vars`.
* Ingresar a Wombat Dialer via web, e ingresar a Administracion -> Editar usuarios.

.. image:: images/maintance_wd_changepass_1.png

*Figure 5: WD change credentials*

* Ahí se puede ver al usuario demoadmin, hacer click en el icono de lápiz, cambiar el usuario en *Login* por el **mismo usuario que ingresó en la variable dialer_user**, ingresar la **misma contraseña ingresada en dialer_password**.
* Finalmente dar click en el botón Guardar.

.. image:: images/maintance_wd_changepass_2.png

*Figure 6: WD change credentials 2*

* Una vez finalizado, recargar la página e ingresar con las nuevas credenciales.

Credenciales AMI
#################

.. note::

  A partir del release-1.8.0 tener esto en cuenta

Wombat Dialer utilizará unas credenciales para AMI aparte de las que usa OMniLeads, por ello se crea un usuario **wombatami** dentro del archivo `oml_manager.conf`. La contraseña de este usuario de AMI se cambia según lo que ingresó el usuario en el parametro **ami_password** del archivo de inventario. Por defecto viene asi:

.. code::

  [wombatami]
  secret =  5_MeO_DMT
  deny = 0.0.0.0/0.0.0.0
  permit = 127.0.0.1/255.255.255.255
  read = all
  write = all

Parámetros básicos
###################

Una vez adentro del sistema, se procede con la configuración de dos parámetros básicos necesarios para dejar lista la integración con OMniLeads.
Para ello debemos acceder al menú de "Configuración básica" como se indica en la figura 7.

.. image:: images/maintance_wd_config1.png

*Figure 7: WD basic config*

En este menú se debe generar en primer lugar se debe generar una nueva instancia de conexión dentro de la sección "Asterisk Servers"
como se expone en la figura 8.

.. image:: images/maintance_wd_config2.png

*Figure 8: WD basic config - AMI Asterisk*

En el siguiente punto, se configura un Troncal utilizando un "Nombre del troncal" arbitrario, pero con la cadena de llamado marcada
en la figura 9. **Local/${num}@from-oml/n**

.. image:: images/maintance_wd_config3.png

*Figure 9: WD basic config - Asterisk Trunk*

Por último, recuerde dar "play" al servicio de dialer, tal como lo indica la siguiente figura 10.

.. image:: images/maintance_wd_config4.png

*Figure 10: WD activate*

Finalmente la plataforma se encuentra habilitada para gestionar llamadas predictivas. La instalación por defecto cuenta con una licencia de Wombat Dialer demo de un canal.

Backup/restore de base de datos
###############################

Se puede realizar el backup/restore de la base de datos MariaDB del discador, ejcutando los siguientes comandos en la máquina donde esta corriendo Wombat/MariaDB.

Para hacer el backup:

.. code::

  # mysqldump -B wombat -u root -p > $ubicacion_archivo_dump/dump.sql
  Enter password:

Donde `$ubicacion_archivo_dump` es la ruta donde se va a ubicar el archivo dump

Para hacer el restore, en un nuevo servidor Mariadb:

.. code::

  mysql -u root -p qstats < dump.sql

Hay que tener el archivo de backup en este nuevo server para hacer el restore

Cambiar certificados SSL
************************

Si desea cambiar los certificados SSL con los que instaló la plataforma necesita tener el par llave/certificado en formato **.pem**. Renombre los archivos, tienen que llamarse ***cert.pem** y **key.pem**. Luego Ubique estos archivos en las siguientes carpetas:

.. code::

  /opt/omnileads/nginx_certs/
  /opt/omnileads/kamailio/etc/certs

Luego de ubicar los archivos, reiniciar los siguientes servicios:

.. code::

  service nginx restart
  service kamailio restart

Resetear contraseña web de admin
*********************************

Si ha olvidado su contraseña de usuario admin podrá resetear la misma a sus valor por defecto (admin/admin), para ello ingrese por SSH al OMniLeads y ejecute el siguiente comando:

.. code::

  /opt/omnileads/bin/manage.sh cambiar_admin_password

Backup & Restore
****************
OMniLeads dispone de un bash script para llevar a cabo las tareas de backup/restore.


* **Backup**

  Para realizar un backup:

  Debemos acceder por ssh al host donde tenemos corriendo OMniLeads. Una vez dentro del host se ejecutan los siguiente comandos.

  .. code-block:: bash

    su omnileads -
    cd /opt/omnileads/bin
    ./backup-restore.sh --backup

  La ejecución del script arroja una salida similar a la de la figura:

  .. image:: images/maintance_backup_1.png

  Como se puede observar, la salida del comando nos indica cómo realizar el restore de dicho backup.
  Dentro del path **/opt/omnileads/backup**, se generan los archivos ".tar.gz" que contienen los backups ejecutados.

  Si no se quiere realizar un backup de la base de datos, se debe invocar al script con la opción *--no-database*

  .. code-block:: bash

    su omnileads -
    cd /opt/omnileads/bin
   ./backup-restore.sh --backup --no-database


* **Restore**

  Para realizar el *restore* en un nuevo host, se debe dejar disponible el archivo generado por el *backup* dentro del path **/opt/omnileads/backup**.

  .. important::

    En caso de hacer el restore en una nueva instancia, es necesario que dicha máquina cuente con OMniLeads corriendo en la misma versión respecto
    a la que fue generadora de nuestro archivo de backup.

  Para llevar a cabo un restore, se debe ejecutar:

  .. code-block:: bash

     su omnileads
     cd /opt/omnileads/bin/
     ./backup-restore.sh --restore=nombre_del_archivo_de_backup.tar.gz

  No hace falta agregar el path completo de ubicación del backup, un restore exitoso arroja una salida similar a la figura:

  .. image:: images/maintance_backup_2.png

 .. note::

    * Si el backup no contiene backup de base de datos el restore no va a modificar la base de datos
    * Una copia del archivo omnileads_envars.sh va a quedar en `/opt/omnileads/backup/omnileads_envars.sh.backup  `
    * Si el backup contiene addons instalados, el restore va a ejecutar la reinstalación de dichos addons

Actualizaciones
***************

OMniLeads genera releases continuos, lo cual implica tener que actualizar el sistema periodicamente.

.. important::

  **Upgrade anterior a release-1.3.1 (incluyendolo)**

  * Es **IMPRESCINDIBLE** contar con las contraseñas de *postgresql*, *mysql* y *django admin* que se usaron durante la instalación. Tendrá que asignarlas nuevamente en el archivo *inventory*. Si no se utilizan las mismas contraseñas que se usaron, el upgrade cambiará las contraseñas por aquellas que se encuentren en el inventory
  * Si no utiliza la misma contraseña de MySQL que se tenia previamente, el upgrade fallará.

  **Upgrade después de release-1.3.1**

  * Si no se quieren cambiar alguna variable basta con definir el tipo de instalación.
  * Si se quieren cambiar alguna variable, ingresarla y la actualización se encargará de ello.

  **Upgrade desde de release-1.15.0 (o cualquier versión anterior) hacia release-1.16.0**

  * En dicha entrega se extraen del repositorio principal, los componentens: rtpengine, asterisk, redis, websocket, kamailio, postgres y nginx.
  Los mismos desde entonces residen en repositorios aislados y dedicados, incluidos como submodulos GIT del repositorio principal de la App.
  Por lo tanto el procedimiento de actualización debe implicar la inicialización de submodulos como se indica debajo.

A continuación se exponen los pasos a seguir para llevar a cabo una nueva actualización de la plataforma. Esta tarea también se realiza con el script "deploy.sh".
Las actualizaciones se anuncian por los canales de comunicaciones oficiales del proyecto.
Dependiendo el método de instalación que se haya seleccionado:

Actualizaciones en instalaciones Self-Hosted
############################################

Para proceder bajo dicho escenario:

* Acceder como *root* a la maquina con OMniLeads instalado
* Posicionarse sobre el directorio raiz *ominicontacto* y ejecutar:

.. code-block:: bash

 git fetch
 git checkout release-V.V.V
 git submodule init
 git submodule update --remote

.. note::

 Recordar que la tecla *Tab* al presionar más de una vez, autocompleta el comando desplegando todos los releases.

* A continuación debemos posicionarnos sobre el directorio

.. code-block:: bash

 cd install/onpremise/deploy/ansible

* Ahora se procede con la edición del archivo de inventario, donde solamente se deben ajustar las variables:

.. code-block:: bash

  ##########################################################################################
  # If you are installing a prodenv (PE) AIO y bare-metal, change the IP and hostname here #
  ##########################################################################################
  [prodenv-aio]
  localhost ansible_connection=local ansible_user=root #(this line is for self-hosted installation)
  #10.10.1

  extern_ip=auto

* Finalmente se debe ejecutar el script deploy.sh con el parámetro -u (update). Esta ejecución tomará unos minutos e implica el aplicar todas las actualizaciones que aplica el nuevo release.

::

 ./deploy.sh -u --iface=$NETWORK_INTERFACE

* Si todo acontece correctamente, al finalizar la ejecución de la tarea veremos una pantalla como muestra la figura 13.

.. image:: images/maintance_updates_ok.png

*Figure 13: updates OK*

Actualizaciones en utilizando método Deloyer-Nodes
##################################################

* Se debe acceder al repositorio clonado dentro del deployer, para desde allí correr la actualización sobre el host que aloja la App.

::

 cd PATH_repo_OML
 cd ominicontacto/deploy/ansible
 git fetch
 git checkout release-V.V.V
 git submodule init
 git submodule update --remote

Recordar que la tecla *Tab* al presionar más de una vez, autocompleta el comando desplegando todos los releases.
Una vez seleccionado el release:

* Descomentar en el archivo de inventario la línea correspondiente al tipo de instalación self-hosted y arquitectura desplegada.

::

  ##########################################################################################
  # If you are installing a prodenv (PE) AIO y bare-metal, change the IP and hostname here #
  ##########################################################################################
  [prodenv-aio]
  #localhost ansible_connection=local ansible_user=root #(this line is for self-hosted installation)
  10.10.10.100 ansible_ssh_port=22 ansible_user=root #(this line is for node-host installation)

  extern_ip=auto

* Finalmente se debe ejecutar el script deploy.sh con el parámetro -u (update). Esta ejecución tomará unos minutos e implica el aplicar todas las actualizaciones que aplica el nuevo release.

::

	sudo ./deploy.sh -u

* Finalmente, la plataforma queda actualizada a la última versión estable "master"

.. image:: images/maintance_updates_ok.png

*Figure 14: updates from ansible remote OK*

.. note::

  Las instalaciones AIO dejarán de ser soportadas en un futuro para Debian y Ubuntu, por lo que se recomienda usar CentOS

Cambios de los parámetros de red (Hostname y/o Dirección IP) y cambios de contraseñas de servicios
***************************************************************************************************

* Para llevar a cabo éstas tareas, debemos ejecutar nuevamente el script "deploy.sh".
* **Si se quiere cambiar IP** Se debe ingresar con el usuario root al sistema, cambiar la dirección IP a nivel sistema operativo y/o hostname y asegurarnos de que el host tomó los cambios. Se recomienda un *reboot* del sistema.
* **Si se quieren cambiar constraseñas** cambiar la contraseña que se desee, remitirse a :ref:`about_install_inventory_vars` para revisar las variables de contraseñas.

Llevar a cabo esta tarea conlleva ejecutar el script deploy.sh asi:

.. code:: bash

  ./deploy.sh -u

.. important::

  Asegurarse de correr el script en el mismo release en el cual se encuentra instalado el sistema, de lo contrario realizará actualización del software.

Desbloqueo de usuarios
***********************

OMniLeads cuenta con un sistema de bloqueo de usuarios, cuando alguno ingresa la contraseña erronea tres veces. Esta es una medida de seguridad implementada para evitar ataques de fuerza bruta en la consola de Login de la plataforma.
El usuario administrador tiene la posibilidad de desbloquar algún usuario que haya sido bloqueado por ingresar su contraseña errónea sin querer.

Para desbloquearlo se ingresa a la siguiente URL: https://omnileads-hostname/admin, esta URL despliega la llamada **Consola de Administración de Django**.

.. image:: images/django_admin.png

*Figure 15: Django admin console*


Allí, ingresar las credenciales del usuario admin. Luego hacer click en el botón **Defender**

.. image:: images/defender.png

*Figure 16: Defender in django admin*

Esto abre la administración de **Django Defender** (https://github.com/kencochrane/django-defender) que es el plugin de Django usado para manejar esto. Hacer click en **Blocked Users**

.. image:: images/blocked_users.png

*Figure 17: Blocked users view*

Se observará el usuario bloqueado. Basta con hacer click en **Unblock** para desbloquearlo.

.. image:: images/unblock.png

*Figure 18: Unblock user view*

Ya el usuario podrá loguearse sin problema.

<<<<<<< HEAD
Recover & Takeover nodo PostgreSQL HA
**************************************
=======
.. _about_recovery_pgsql_ha:

Recovery & Takeover nodo PostgreSQL HA
***************************************
>>>>>>> develop

Para realizar la recuperacion de un nodo principal que por algun tipo de falla quedo fuera de serivcio y por lo tanto el nodo de backup pasa a ser principal.
Para volver a unir nuestro nodo al cluster, se deben ejecutar los siguientes comandos:

.. code::

<<<<<<< HEAD
    main_pgsql: systemctl stop postgresql-11
    main_pgsql: su postgres -
    main_pgsql: cd ~
    main_pgsql: repmgr -h replica_pgsql -U repmgr -d repmgr -f repmgr.conf standby clone --dry-run
    main_pgsql: repmgr -h replica_pgsql -U repmgr -d repmgr -f repmgr.conf standby clone -F
    main_pgsql: systemctl start postgresql-11
    main_pgsql: repmgr -f repmgr.conf standby register -F

=======
    main_pgsql: su postgres -
    main_pgsql: cd ~
    main_pgsql: repmgr -h ip_backup_node -U repmgr -d repmgr -f repmgr.conf standby clone --dry-run
    main_pgsql: repmgr -h ip_backup_node -U repmgr -d repmgr -f repmgr.conf standby clone -F
    main_pgsql: systemctl start postgresql-11
    main_pgsql: systemctl start repmgr11
    main_pgsql: repmgr -f repmgr.conf standby register -F



>>>>>>> develop
Ahora bien, para dejar nuestro cluster en el estado inicial, es decir en la coniguracion de main - backup explicidata en la instalacion, se deben
ejecutar la siguiente lista de comandos:

.. code::

    replica_pgsql: systemctl stop postgresql-11
<<<<<<< HEAD
    replica_pgsql: repmgr -h main_pgsql -U repmgr -d repmgr -f repmgr.conf standby clone --dry-run
    replica_pgsql: repmgr -h main_pgsql -U repmgr -d repmgr -f repmgr.conf standby clone -F
=======
    replica_pgsql: su postgres -
    replica_pgsql: cd ~
    replica_pgsql: repmgr -h ip_main_node -U repmgr -d repmgr -f repmgr.conf standby clone --dry-run
    replica_pgsql: repmgr -h ip_main_node -U repmgr -d repmgr -f repmgr.conf standby clone -F
>>>>>>> develop
    replica_pgsql: systemctl start postgresql-11
    replica_pgsql: repmgr -f repmgr.conf standby register -F


Desinstalación de OMniLeads
****************************

Si por alguna razón quiere desinstalar OMniLeads de su máquina o VM se cuenta con un script para ello. Ya viene incorporado en el proceso de instalación, basta con ejecutarlo:

.. code::

  oml-uninstall

Este script:

* Desinstala los servicios esenciales de omnileads: asterisk, kamailio, rtpengine, mariadb, postgresql, wombat dialer, redis, nginx y omniapp.
* Borra la carpeta /opt/omnileads (incluyendo grabaciones)
* Elimina las bases de datos

.. note::

  El script no desinstala la paquetería de dependencias usadas para la instalación de los servicios.

.. important::

  Tener cuidado al ejecutarlo, una vez ejecutado no hay forma de recuperar el sistema.

  .. _about_install_docker_build:
